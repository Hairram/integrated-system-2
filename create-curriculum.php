<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$course_id=$_POST['course_id']; 
$var2 = explode("/", $course_id);
$course_id=$var2[0];
$curr_name=$_POST['curr_name'];
$curr_description=$_POST['curr_description']; 
$sql="INSERT INTO  tbl_curriculum(curr_name,curr_description,course_id) VALUES(:curr_name,:curr_description,:course_id)";
$query = $dbh->prepare($sql);
$query->bindParam(':curr_name',$curr_name,PDO::PARAM_STR);
$query->bindParam(':curr_description',$curr_description,PDO::PARAM_STR);
$query->bindParam(':course_id',$course_id,PDO::PARAM_STR);

$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$_SESSION['cr8_curr_msg']="Curriculum Created successfully";
header("Location: view-curr-list.php");
}
else 
{
$error="Something went wrong. Please try again";
}

}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
                                               


        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Create Curriculum</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage Curriculum</li>
                                        <li class="active">Create Curriculum</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5><b>Create New Curriculum</b></h5>
                                                </div>
                                          
                                        </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">

                                            <div class="form-group">
                                               <label for="default" class="col-sm-2 control-label">Curriculum Name</label>
                                                    <div class="col-sm-5">
                                                        <input type="text" name="curr_name" class="form-control" id="default" placeholder="e.g. Bachelor of Science in Information Technology" required="required">
                                                    </div>
                                                </div>

                                            <div class="form-group">
                                               <label for="default" class="col-sm-2 control-label">Curriculum Description</label>
                                                    <div class="col-sm-5">
                                                        <input type="text" name="curr_description" class="form-control" id="default" placeholder="" required="required">
                                                    </div>
                                                </div>

                                                  <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Course</label>
                                                        <div class="col-sm-5">
                                                            <input list="course_name" name="course_id" id="default" class="form-control" placeholder="Course" required>
                                                        <datalist id="course_name">
<?php $sql = "SELECT * from tbl_courses order by course_id";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->course_id . '/' .$result->course_code); ?>"></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                         </div>           
                                                    
                                             </tbody>

                                             

                                            <div class="form-group"> 
                                             <div class="col-sm-offset-2 col-sm-2"> 
                                                <button type="submit" name="submit" class="btn btn-primary">Create</button>
                                             </div>
                                         </div>
                                         
                                             </table>
                                            </form>
                                                 
                                                 </div>
                                                   
                                </div>
                    </div>

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>

        
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
<?PHP } ?>
