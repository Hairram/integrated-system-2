<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');

$errormsg='';
$action = "add";
$miscellaneous_type='';
$miscellaneous_amount='';
$miscellaneous_id='';
if(isset($_POST['submit']))
{
$miscellaneous_type = mysqli_real_escape_string($conn,$_POST['miscellaneous_type']);
$miscellaneous_amount = mysqli_real_escape_string($conn,$_POST['miscellaneous_amount']);

 if($_POST['action']=="add")
 {
$sql = $conn->query("INSERT INTO tbl_miscellaneous (miscellaneous_type,miscellaneous_amount) VALUES ('$miscellaneous_type','$miscellaneous_amount')") ;
echo '<script type="text/javascript">window.location="miscellaneous.php?act=1";</script>';

}else
  if($_POST['action']=="update")
 {
 $miscellaneous_id = mysqli_real_escape_string($conn,$_POST['miscellaneous_id']); 
   $sql = $conn->query("UPDATE  tbl_miscellaneous  SET  miscellaneous_type  = '$miscellaneous_type', miscellaneous_amount = '$miscellaneous_amount'   WHERE  miscellaneous_id  = '$miscellaneous_id'");
   echo '<script type="text/javascript">window.location="miscellaneous.php?act=2";</script>';
 }
}
if(isset($_GET['action']) && $_GET['action']=="delete"){

$conn->query("DELETE FROM tbl_miscellaneous  WHERE miscellaneous_id='".$_GET['miscellaneous_id']."'");
header("location: miscellaneous.php?act=3");

}




$action = "add";
if(isset($_GET['action']) && $_GET['action']=="edit" ){
$miscellaneous_id = isset($_GET['miscellaneous_id'])?mysqli_real_escape_string($conn,$_GET['miscellaneous_id']):'';

$sqlEdit = $conn->query("SELECT * FROM tbl_miscellaneous WHERE miscellaneous_id='".$miscellaneous_id."'");
if($sqlEdit->num_rows)
{
$rowsEdit = $sqlEdit->fetch_assoc();
extract($rowsEdit);
$action = "update";
}else
{
$_GET['action']="";
}
}

if(isset($_REQUEST['act']) && @$_REQUEST['act']=="1")
{
$errormsg = "<div class='alert alert-success'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong>  Add successfully</div>";
}else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="2")
{
$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> <strong>Success!</strong>  Edit successfully</div>";
}
else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="3")
{
$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong>  Delete successfully</div>";
}


if(isset($_POST['search']))
{
  $valueToSearch = $_POST['valueToSearch'];
$query = "SELECT  * FROM tbl_miscellaneous WHERE concat(miscellaneous_type,miscellaneous_amount) LIKE '%".$valueToSearch."%'";
$search_result = filterTable($query); 
}else {

  $query = "SELECT * FROM tbl_miscellaneous";
$search_result = filterTable($query);

   
}
function filterTable($query)
{

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "occ_sis";

// Create connection
$connect = mysqli_connect($servername, $username, $password, $dbname);
  $filter_Result = mysqli_query($connect,$query);
  return $filter_Result;
}

        ?>

<!DOCTYPE html>
<html lang="en">
    <head>
         <link rel="stylesheet" href="css/miscellaneous.css" >
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
       
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar-billing.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar-billing.php');?>

 <?php

echo $errormsg;
?>

 
<div class="container">
  <div class="box1">
    
      Create Fees <hr style="padding-bottom: 0px;">
      <span>Type*</span>
       <form action="miscellaneous.php" method="post" id="signupForm1">
     <div class="inputBox">
     <input type="text" name="miscellaneous_type" id="miscellaneous_type" value="<?php echo $miscellaneous_type?>">
   </div>
     <span>Amount*</span>
     <div class="inputBox">
     <input type="text" name="miscellaneous_amount" id="miscellaneous_amount" value="<?php echo $miscellaneous_amount?>">
   </div>

    <div class="button" style="float: right">
      <input type="hidden" name="miscellaneous_id" value="<?php echo $miscellaneous_id;?>">
      <input type="hidden" name="action" value="<?php echo $action;?>">
      <button type="submit" name="submit" class="btn btn-primary" style="border-radius: 20px;">Save</button>
      </div>
  </div>


  <div class="box2">
    List of Fees <hr style="padding-bottom: 0px;">
    <div class="wrap">
   <div class="search">
      <input type="text" class="searchTerm" name="valueToSearch" placeholder="Search. . . ">
   </div>
    <div class="button" style="float: right; ">
      <button type="submit" name="search" value="Filter" class="btn btn-primary" style="border-radius: 16px; width: 80px; height: 32px; font-size: 12px;"><span class="fa fa-search"> </span>Search</button>
    </div>
   <div class="table">
   <table>
    <thead>
  <tr>                                    
    <th>Type of Fees</th>
    <th>Amount</th>
    <th>Action</th>
   </tr>
   </thead>
   <tbody>
   
     <?php
     $sql = "select * from tbl_miscellaneous";
                    
                   while($r = mysqli_fetch_array($search_result))
                  {
            echo '<tr>
                                            
                                            
                                            <td>'.$r['miscellaneous_type'].'</td>
                                            <td>'.$r['miscellaneous_amount'].'</td>

                                            <td>
                      
                      
                                            <a href="miscellaneous.php?action=edit&miscellaneous_id='.$r['miscellaneous_id'].'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span></a>
                 
                      
                      <a onclick="return confirm(\'Are you sure you want to delete this record\');" href="miscellaneous.php?action=delete&miscellaneous_id='.$r['miscellaneous_id'].'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-remove"></span></a>

                                             </td>
                                        </tr>';
                    $i++;

                  }

     ?>


                                          <!-- <td><input type="checkbox" name="miss[]" value="'.$r['id'].'" onclick="add_miscallaneous_fee('.$fee.',this)"> -->
  
      </tbody>
  </table>
</div>
</div>
</div>
</form>
</div>


                          



        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/waypoint/waypoints.min.js"></script>
        <script src="js/counterUp/jquery.counterup.min.js"></script>
        <script src="js/amcharts/amcharts.js"></script>
        <script src="js/amcharts/serial.js"></script>
        <script src="js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="js/amcharts/themes/light.js"></script>
        <script src="js/toastr/toastr.min.js"></script>
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script src="js/production-chart.js"></script>
        <script src="js/traffic-chart.js"></script>
        <script src="js/task-list.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
        <script>

            $(function(){

                // Counter for dashboard stats
                $('.counter').counterUp({
                    delay: 10,
                    time: 1000
                });
                /*
                // Welcome notification
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "3000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr["success"]( "Welcome "+<?php echo(json_encode($instructorlastname .", ". $instructorfirstname));?>);

            });*/}
        </script>


    </body>

    <div class="foot"><footer>

</footer> </div>

<style> .foot{text-align: center; */}</style>
</html>

