<?php
$connect = mysqli_connect("localhost", "root", "", "occ_sis");
$output = '';
if(isset($_POST["import"]))
{
    // $extension = end(explode(".", $_FILES["excel"]["name"])); // For getting Extension of selected file
    $value = explode(".", $_FILES["excel"]["name"]);
    $extension = strtolower(array_pop($value));
    $allowed_extension = array("csv"); //allowed extension
    if(in_array($extension, $allowed_extension)) //check selected file extension is present in allowed extension array
    {
    $file = $_FILES["excel"]["tmp_name"]; // getting temporary source of excel file
    include("../StudentInformationSystem /Classes/PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code

    $objPHPExcel = PHPExcel_IOFactory::load($file); // create object of PHPExcel library by using load() method and in load method define path of selected file

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
    {
    $highestRow = $worksheet->getHighestRow();
    for($row=2; $row<=$highestRow; $row++)
    {
    $a = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(0, $row)->getValue());
    $b = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(1, $row)->getValue());
    $c = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(2, $row)->getValue());
    $d = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
    $e = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(4, $row)->getValue());
    $f = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(5, $row)->getValue());
    $g = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(6, $row)->getValue());
    $h = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(7, $row)->getValue());
        $query = "INSERT INTO tbl_subjects(subject_code,course_id,descriptive_title,units,lec_hrs,lab_hrs,hrs_week,pre_requisite) VALUES ('$a', '$b' , '$c' , '$d' , '$e', '$f' ,'$g','$h')";
        mysqli_query($connect, $query);
    }
    }
    $output .= "<label class='text-success'>Imported Successfully!</label>";
	$_SESSION['cr8_sub_msg']="Subject Imported successfully";

	header("Location: manage-subjects.php");

    }
    else
    {
    $output = '<label class="text-danger">Invalid File</label>'; //if non excel file then
    }
}
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$subject_code		 = $_POST['subject_code']; 
$descriptive_title	 = $_POST['descriptive_title']; 
$units             	 = $_POST['units'];
$lec_hrs         	 = $_POST['lec_hrs'];
$lab_hrs		 	 = $_POST['lab_hrs'];
$hrs_week		 	 = $_POST['hrs_week'];
$pre_requisite	 	 = $_POST['pre_requisite'];
$subject_id		 	 = $_POST['subject_id']; 
$course_id   	 	 = $_POST['course_id']; 
$semester   	 	 = $_POST['semester']; 
$year_level   	 	 = $_POST['year_level']; 
$var        	 	 = explode("/", $subject_id);
$subject_id=$var[0];
$sql="INSERT INTO  tbl_subjects(subject_code,course_id,semester,year_level , descriptive_title,units,lec_hrs,lab_hrs,hrs_week,pre_requisite) VALUES(:subject_code,:course_id,:semester,:year_level,:descriptive_title,:units,:lec_hrs,:lab_hrs,:hrs_week,:pre_requisite)";
$query = $dbh->prepare($sql);
$query->bindParam(':subject_code',$subject_code,PDO::PARAM_STR);
$query->bindParam(':course_id',$course_id,PDO::PARAM_STR);
$query->bindParam(':semester',$semester,PDO::PARAM_STR);
$query->bindParam(':year_level',$year_level,PDO::PARAM_STR);
$query->bindParam(':descriptive_title',$descriptive_title,PDO::PARAM_STR);
$query->bindParam(':units',$units,PDO::PARAM_STR);
$query->bindParam(':lec_hrs',$lec_hrs,PDO::PARAM_STR);
$query->bindParam(':lab_hrs',$lab_hrs,PDO::PARAM_STR);
$query->bindParam(':hrs_week',$hrs_week,PDO::PARAM_STR);
$query->bindParam(':pre_requisite',$pre_requisite,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$_SESSION['cr8_sub_msg']="Subject Created successfully";
header("Location: manage-subjects.php");
}
else 
{
$error="Something went wrong. Please try again";
}

}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Create/Upload Subjects</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li class="active">Manage Subjects</li>
                                        <li>Create/Upload Subjects</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <!-- <h5>Create Subject</h5> -->
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
        <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
    </div>
    <?php } ?>
            <form class="form-horizontal" method="post">
			<div class="form-group">
                <label for="default" class="col-sm-2 control-label">Course</label>
                    <div class="col-sm-5">
                    <select class="form-control" name="course_id" required>
					<option value=""></option>
					<?php 
					$sql = "SELECT * from tbl_courses";
					$query = $dbh->prepare($sql);
					$query->execute();
					$results=$query->fetchAll(PDO::FETCH_OBJ);
					if($query->rowCount() > 0)
					{
					foreach($results as $result)
					{   ?>
					<option value="<?php echo htmlentities($result->course_id); ?>"><?php echo htmlentities($result->course_name); ?></option>
					<?php }} ?>
					</select>
                </div>
            </div>
			<div class="form-group">
                <label for="default" class="col-sm-2 control-label">Semester</label>
                    <div class="col-sm-5">
                    <select name="semester" class="form-control" id="default" required="required">
                        <option selected="selected" value="First">First</option>
                         <option value="Second">Second</option>
                          <option value="Summer">Summer</option>
                    </select>
                </div>
            </div>
			<div class="form-group">
                <label for="default" class="col-sm-2 control-label">Year Level</label>
                    <div class="col-sm-5">
                    <select name="year_level" class="form-control" id="default" required="required">
							<option value="">-- Select Year Level -- </option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
                    </select>
                </div>
            </div>
			
            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Subject Code</label>
                    <div class="col-sm-5">
                        <input type="text" name="subject_code" class="form-control" id="default" placeholder="Subject Code" required="required">
                </div>
            </div>

            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Descriptive Title</label>
                    <div class="col-sm-5">
                        <input type="text" name="descriptive_title" class="form-control" id="default" placeholder="Subject Name" required="required">
                </div>
            </div>

            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Units</label>
                    <div class="col-sm-5">
                        <input type="text" name="units" class="form-control" id="default" placeholder="Credit Units" required="required">
                </div>
            </div>

            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Lec Hours</label>
                    <div class="col-sm-5">
                        <input type="text" name="lec_hrs" class="form-control" id="default" placeholder="" required="required">
                </div>
            </div>

            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Lab Hours</label>
                    <div class="col-sm-5">
                        <input type="text" name="lab_hrs" class="form-control" id="default" placeholder="" required="required">
                </div>
            </div>

            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Hrs/Week</label>
                    <div class="col-sm-5">
                        <input type="text" name="hrs_week" class="form-control" id="default" placeholder="" required="required">
                </div>
            </div>


            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Pre-requisites</label>
                    <div class="col-sm-5">
                        <input list="pre_requisite" name="pre_requisite" id="default" class="form-control" placeholder="Subjects">
                            <datalist id="pre_requisite">
							<?php 
							$sql = "SELECT * from tbl_subjects order by descriptive_title";
							$query = $dbh->prepare($sql);
							$query->execute();
							$results=$query->fetchAll(PDO::FETCH_OBJ);
							if($query->rowCount() > 0)
							{
							foreach($results as $result)
							{   ?>
							<option value="<?php echo htmlentities($result->descriptive_title); ?>"><?php echo htmlentities($result->descriptive_title); ?></option>
							<?php }} ?>
							</datalist>
                </div>
            </div>


                                                    
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-2">
            <button type="submit" name="submit" class="btn btn-primary">Create</button>
        </div>
    </form>

    <div class="col-sm-4 text-right">
        <u><i class="text-danger" class="margin-input-bot">.csv file (Subject Code, Descriptive Title, Units,Lec Hours, Lab Hours, Hrs/Week, Pre-requisite)</i></u>
    </div>

        <form method="post" enctype="multipart/form-data">
            <div class="col-sm-2 text-right">
                <input type="file" accept=".csv" name="excel" class="margin-input-bot" />
        </div>

        <div class="col-sm-2 text-center">
            <input type="submit" name="import" class="btn btn-info" value="Import" />
        </div>
        </form>

        <div class="col-sm-12 text-right">
            <i class="text-success" class="margin-input-bot"><?php echo $output; ?></i>
        </div>
    </div>
            
            <div class="container-fluid">       
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-5">
                            <div class="col-md-3">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <!-- <h5><a class="btn btn-success" href="upload-subject-csv.php">Upload Subjects (CSV file)</h5></a> -->
                                        </div>
                                    </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-12 -->
                        </div>
                    </div>

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
