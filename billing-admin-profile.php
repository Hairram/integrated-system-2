<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {
    header("Location: index.php");
    }
    else{
        $billing_id = $_SESSION['billing_id'];
        $billinglastname = $_SESSION['lastname'];
         $billingrfirstname = $_SESSION['firstname'];
        ?>
<!DOCTYPE html>
<html lang="en">
    <head>
         <link rel="stylesheet" href="css/billing-admin-profile.css" >
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
       
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar-billing.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar-billing.php');?>

           

<div class="container">
  <div class="box1">
     <img src="images/no-image-icon.png" alt="Avatar" class="avatar"><h5 style="text-align: center; margin-top: 10px; margin-bottom: 5px">Angelo M. Pinque</h5> <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p><b>Staff ID</b></p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p><b>Roll</b></p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p><b>Designation</b></p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p><b>EPF No</b></p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p><b>Assigned</b></p>

     
  </div>

  <div class="box2">
   <b>Personal Profile</b> <hr style="padding: 0px; margin: 10px">
   <p>Phone Number</p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p>Email</p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p>Gender</p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p>Date of Birth</p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p>Contract Type</p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 10px">
    <b>Address</b> <hr style="padding: 0px; margin: 10px">
     <p>Current Address</p>
     <hr style="margin: 0px; padding: 0px; padding-bottom: 0px">
     <p>Permanent Address</p>
     
     
</div>



</div>

                          


        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/waypoint/waypoints.min.js"></script>
        <script src="js/counterUp/jquery.counterup.min.js"></script>
        <script src="js/amcharts/amcharts.js"></script>
        <script src="js/amcharts/serial.js"></script>
        <script src="js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="js/amcharts/themes/light.js"></script>
        <script src="js/toastr/toastr.min.js"></script>
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script src="js/production-chart.js"></script>
        <script src="js/traffic-chart.js"></script>
        <script src="js/task-list.js"></script>
        <script>
            $(function(){

                // Counter for dashboard stats
                $('.counter').counterUp({
                    delay: 10,
                    time: 1000
                });
                /*
                // Welcome notification
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "3000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr["success"]( "Welcome "+<?php echo(json_encode($instructorlastname .", ". $instructorfirstname));?>);

            });*/
        </script>
    </body>

    <div class="foot"><footer>

</footer> </div>

<style> .foot{text-align: center; */}</style>
</html>
<?php } ?>
