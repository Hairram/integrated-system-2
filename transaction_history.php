<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');
if(strlen($_SESSION['alogin'])==""){
    header("Location: index.php");
} else {
	$billing_id = $_SESSION['billing_id'];
    $billinglastname = $_SESSION['lastname'];
    $billingrfirstname = $_SESSION['firstname'];
	$student_id	 = $_GET['student_id'];
	$sql = "SELECT * FROM tbl_student WHERE student_id='$student_id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		$student_id=$row["student_id"];
		$firstname=$row["firstname"];
		$middlename=$row["middlename"];
		$lastname=$row["lastname"];
		$course_id=$row["course_id"];
		$semester=$row["semester"];
		$schoolyear=$row["school_year"];
		$yearlevel=$row["year_level"];
	}
	$sql = "SELECT * FROM tbl_courses WHERE course_id='$course_id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		$cyear=$row["course_code"];
	}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
       <link rel="stylesheet" href="css/payment.js" >
         <link rel="stylesheet" href="css/transaction_history.css" >
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>

    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar-billing.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar-billing.php');?>
<div class="container">
  <div class="box2">
     <div class="row" style="margin-top: 13px;">
 
      <div class="col-sm-14">
      <div class="col-sm-6 mb-1"> <strong>Full Name:</strong> <span><?php echo $lastname .','.$firstname;?></span> </div>
      <div class="col-sm-6 mb-1" style="text-align: right"> <strong>Student ID: </strong> <span><?php echo $_GET['student_id'];?></span> </div>
    </div>
      <br/>
      <br/>

      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span><?php echo $semester;?> Semester</span>
          </div>
          <div class="col-sm-3" style="text-align: right;"> <strong>School Year: </strong><span><?php echo $schoolyear;?></span>
          </div>
          <div class="col-sm-3" style="text-align: right;"> <strong>Course & Year: </strong><span><?php echo $cyear .'-'.$yearlevel;?></span>
          </div>
          <div class="col-sm-3" style="text-align: right;"> <strong>Student Type: </strong><span>Old</span>
          </div>
        </div>
      </div>
    </div>

     <h4>Transaction History</h4>

   <div class="panel panel-default" style="margin-top: 7px">

                 <div class="table-sorting table-responsive" id="subjectresult">

                    <table class="table table-striped table-bordered table-hover" id="tSortable22">
                                    <thead>
                                        <tr>
                                            <th>OR No.</th>
                                            <th>Payable Amount</th>  
                                            <th>Amount Paid</th>
                                            <th>Date of Paid</th>
                                            <th>Balance</th>
                                            <th>Remarks</th>

                                           
                                        </tr>
                                    </thead>
									<?php 	
											$id  	= $_GET['student_id'];
											$sql 	= "select * from tbl_payment_transaction where student_id='$id' ";
											$q 		= $conn->query($sql);
											$i		= 1;
											while($r= $q->fetch_assoc())
											{
									?>
										<tr>
											<td class="text-center"><?php echo $r['or_number'];?></td>
											<td class="text-center"><?php echo $_GET['payable'];?></td>
											<td class="text-center"><?php echo $r['paid_amount'];?></td>
											<td class="text-center"><?php echo $r['submit_date'];?></td>
											<td class="text-center"><?php echo $_GET['balance'];?></td>
											<td class="text-center"><?php echo $r['transaction_remark'];?></td>
											<td class="text-center"><a href="receipt.php?student_id=<?php echo $student_id;?>&or=<?php echo $r['or_number'];?>" target="_blank"><button class="btn btn-sm  btn-info"> Receipt </button></a></td>
										</tr>
									<?php } ?>
                                </table>
                            </div>
                    </div>

  </div>
</div>


        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/waypoint/waypoints.min.js"></script>
        <script src="js/counterUp/jquery.counterup.min.js"></script>
        <script src="js/amcharts/amcharts.js"></script>
        <script src="js/amcharts/serial.js"></script>
        <script src="js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="js/amcharts/themes/light.js"></script>
        <script src="js/toastr/toastr.min.js"></script>
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script src="js/production-chart.js"></script>
        <script src="js/traffic-chart.js"></script>
        <script src="js/task-list.js"></script>
        <script>
            $(function(){

                // Counter for dashboard stats
                $('.counter').counterUp({
                    delay: 10,
                    time: 1000
                });
                /*
                // Welcome notification
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "3000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }*/
                /*toastr["success"]( "Welcome "<?php echo(json_encode($instructorlastname .", ". $instructorfirstname));?>);

            });*/
        </script>
    </body>

    <div class="foot"><footer>

</footer> </div>

<style> .foot{text-align: center; */}</style>
</html>
<?php } ?>
