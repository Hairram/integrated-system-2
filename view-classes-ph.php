 
<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{

?>  
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
<?php include('includes/leftbar-ph.php');?>  

  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>View Classes</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> List of Sections</li>
                                        <li class="active"> View Classes</li>
            							
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    

<p style="position: relative; left: 16px"><b>Section Name :</b> BSIT - 1A</p>
 <p style="position: absolute; left: 76%;"><b>Course:</b> BSIT</p>
<p style="position: relative; left: 16px"><b>Shool Year :</b> 2020-2021</p></p>

 <p style="position: absolute; left: 76%;"><b>No. of Students:</b> 3</p>
<p style="position: relative; left: 16px"><b>Semester:</b> First</p>


        
</b>

  <div class="panel-body p-20">                                   
     <table border="1" class="table table-hover table-bordered" style="width:100% !important;">
            <tbody>


<p style="position: relative; left: 20px"><b><h2>Classes</h2></p>
     <tr>
                                                            <th class="text-center">No.</th>
                                                            <th class="text-center">Subject Code</th>
                                                            <th class="text-center">Descriptive Title</th> 
                                                               <th class="text-center">Units</th>
                                                            <th class="text-center">Time</th>
                                                            <th class="text-center">Day</th>
                                                            <th class="text-center">Room</th>
                                                            <th class="text-center">Instructor</th>
                                                            



                                                    

                                                        </tr>

                                                        <tr>
                                                            <td>1</td>
                                                            <td>GenEd1</td>
                                                            <td>Understanding the self</td>
                                                             <td>3</td>
                                                            <td>2:00PM-5:00PM </td>
                                                            <td>Wed</td>
                                                            <td>210</td>
                                                            <td>Leilani Ranara</td>
          

                                                        </tr>

                                                        <tr>
                                                             <td>2</td>
                                                            <td>GenEd2</td>
                                                            <td>Purposive Communication</td>
                                                              <td>3</td>
                                                            <td>2:00PM-5:00PM </td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>
                                                           

                                                        </tr>

                                                        <tr>
                                                             <td>3</td>
                                                            <td>GenEd3</td>
                                                            <td>Mathematics in the Modern World</td>
                                                              <td>3</td>
                                                            <td>2:00PM-5:00PM</td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>
                                                           

                                                        </tr>

                                                        <tr>
                                                             <td>4</td>
                                                            <td>VED</td>
                                                            <td>Good Manners and Right Conduct</td>
                                                              <td>3</td>
                                                            <td>2:00PM-5:00PM</td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>


                                                        </tr>

                                                        <tr>
                                                             <td>5</td>
                                                            <td>PF1</td>
                                                            <td>Movement Competency and Training</td>
                                                              <td>3</td>
                                                            <td>2:00PM-5:00PM</td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>
                                                           
            </td>

                                                        </tr>

                                                        <tr>
                                                             <td>6</td>
                                                            <td>NSTP 1</td>
                                                            <td>CWTS/ROTC/LTS</td>
                                                              <td>3</td>
                                                            <td>2:00PM-5:00PM</td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>
                                                           

                                                        </tr>

                                                        <tr>
                                                             <td>7</td>
                                                            <td>IT101</td>
                                                            <td>Introduction to Computing</td>
                                                              <td>3</td>
                                                            <td>2:00PM-5:00PM</td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>
                                                           

                                                        </tr>

                                                        <tr>
                                                             <td>8</td>
                                                            <td>IT102</td>
                                                            <td>IT Fundamentals</td>
                                                              <td>3</td>
                                                            <td>2:00PM-5:00PM</td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>
                                                           
            

                                                        </tr>

                                                        <tr>
                                                             <td>9</td>
                                                            <td>IT103</td>
                                                            <td>Programming 1</td>
                                                              <td>3</td>    
                                                            <td>2:00PM-5:00PM</td>
                                                            <td>Wed</td>
                                                             <td>210</td>
                                                            <td>Leilani Ranara</td>
                                                           

                                                        </tr>

                                            <!--             <tr>
                                                <th colspan="2" scope="row" class="text-right" colspan="8">Total Hrs/Units</th>           
                                                            <td>26</td>
                                                            <td>26</td>
                                                            <td>4</td>
                                                            <td>30</td>
                                                            <td> </td>
                                                            
                                                             </tr> -->

        </tbody>

        </table>
            


<!-- <?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                            <div class="panel-body p-20">

                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Section ID</th>
                                                            <th>Section</th>
                                                            <th>School Year</th>
                                                            <th>Semester</th>
                                                            <th>Students</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody>


<?php 
/*$getInstructor = "IFNULL((Select lastname,firstname from tbl_faculty_info i where i.faculty_id=c.faculty_id limit 1), 'N/A') as Instructor";*/
/*$getSection = "IFNULL((SELECT CONCAT(co.course_code,'-',i.year_level,' ',i.section_name) from tbl_section i, tbl_courses co where i.course_id=co.course_id limit 1), 'N/A') as Section";*/

/*$getSubject = "IFNULL((Select descriptive_title from tbl_subjects i where i.id=c.subject_id limit 1), 'N/A') as SubjectName";
*/
$getCourse = "IFNULL((SELECT CONCAT (co.course_code,'-', c.year_level,' ',c.section_name) from tbl_courses co where c.course_id=co.course_id limit 1), 'N/A') as Course";
$countStudents = "IFNULL((Select Count(i.student_id) from tbl_class_details i where i.class_id=c.class_id), 'N/A') as TotalStudents";
$sql = "SELECT c.*, ".$getCourse.", ".$countStudents." from tbl_section c";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?> 
<tr>
    <td><?php echo htmlentities($result->section_id);?></td>
    <td><?php echo htmlentities($result->Course);?></td> 
    <td><?php echo htmlentities($result->school_year);?></td>
    <td><?php echo htmlentities($result->semester);?></td>
    <td><?php echo htmlentities($result->TotalStudents);?></td>

<td>
    <a href="edit-section.php?edit=<?php echo htmlentities($result->section_id);?>"><input type="button" name="edit" value="Edit Section"> </a>
    <a href="create-class-try.php?view=<?php echo htmlentities($result->section_id);?>"><input type="button" name="view" value="Add/Create Class"> </a>    
    <a href="view-grades.php?view=<?php echo htmlentities($result->student_id);?>"><input type="button" name="view" value="View Details"> </a>

</td>
</tr>
<?php $cnt=$cnt+1;}} ?>
                                                       
                                                    
                                                    </tbody>
                                                </table> -->

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
<?php } ?>

