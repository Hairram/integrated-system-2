<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['Update']))
{
$sid				=	intval($_GET['subjectid']);
$subject_code		=	$_POST['subject_code'];
$descriptive_title	=	$_POST['descriptive_title'];
$units				=	$_POST['units']; 
$pre_requisite		=	$_POST['pre_requisite']; 
$course_id			=	$_POST['course_id']; 
$semester			=	$_POST['semester']; 
$year_level			=	$_POST['year_level']; 
$sql				=	"update  tbl_subjects set subject_code=:subject_code,course_id=:course_id,semester=:semester,year_level=:year_level,descriptive_title=:descriptive_title,units=:units,pre_requisite=:pre_requisite where subject_id=:sid";
$query				= $dbh->prepare($sql);
$query->bindParam(':subject_code',$subject_code,PDO::PARAM_STR);
$query->bindParam(':course_id',$course_id,PDO::PARAM_STR);
$query->bindParam(':semester',$semester,PDO::PARAM_STR);
$query->bindParam(':year_level',$year_level,PDO::PARAM_STR);
$query->bindParam(':descriptive_title',$descriptive_title,PDO::PARAM_STR);
$query->bindParam(':units',$units,PDO::PARAM_STR);
$query->bindParam(':pre_requisite',$pre_requisite,PDO::PARAM_STR);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$msg="Subject Info updated successfully";
}
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin Update Subject </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
  <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Update Subject</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage Subjects</li>
                                        <li class="active">Update Subject</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <!-- <h5>Update Subject</h5> -->
                                                </div>
                                            </div>
                                            <div class="panel-body">
										<?php if($msg){?>
										<div class="alert alert-success left-icon-alert" role="alert">
										 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
										 </div><?php } 
										else if($error){?>
										<div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">

				 <?php
				$sid=intval($_GET['subjectid']);
				$sql = "SELECT * from tbl_subjects where subject_id=:sid";
				$query = $dbh->prepare($sql);
				$query->bindParam(':sid',$sid,PDO::PARAM_STR);
				$query->execute();
				$results=$query->fetchAll(PDO::FETCH_OBJ);
				$cnt=1;
				if($query->rowCount() > 0)
				{
				foreach($results as $result)
				{  
				 ?>                                               
				<div class="form-group">
                <label for="default" class="col-sm-2 control-label">Course</label>
                    <div class="col-sm-5">
                    <select class="form-control" name="course_id" required>
					<option value=""></option>
					<?php 
					$sql = "SELECT * from tbl_courses";
					$query = $dbh->prepare($sql);
					$query->execute();
					$results1=$query->fetchAll(PDO::FETCH_OBJ);
					if($query->rowCount() > 0)
					{
					foreach($results1 as $result1)
					{   
					if($result->course_id == $result1->course_id){
					?>
					<option value="<?php echo htmlentities($result1->course_id); ?>" selected><?php echo htmlentities($result1->course_name); ?></option>
					<?php } else { ?>
					<option value="<?php echo htmlentities($result1->course_id); ?>"><?php echo htmlentities($result1->course_name); ?></option>
					<?php } ?>
					<?php }} ?>
					</select>
                </div>
                </div>
				
				<div class="form-group">
                <label for="default" class="col-sm-2 control-label">Semester</label>
                    <div class="col-sm-5">
                    <select name="semester" class="form-control" id="default" required="required">
						  <?php if($result->semester == 'First') { ?>
							  <option selected="selected" value="First">First</option>
							  <option value="Second">Second</option>
							  <option value="Summer">Summer</option>
						  <?php } else if($result->semester == 'Second') { ?>
							  <option value="First">First</option>
							  <option selected="selected" value="Second">Second</option>
							  <option value="Summer">Summer</option>
						  <?php } else if($result->semester == 'Summer') { ?>
						      <option value="First">First</option>
							  <option value="Second">Second</option>
							  <option selected="selected" value="Summer">Summer</option>
						  <?php } else { ?>
							  <option value="" selected="selected">-- Select Semester -- </option>
							  <option value="First">First</option>
							  <option value="Second">Second</option>
							  <option value="Summer">Summer</option>
						  <?php } ?>
                    </select>
                </div>
				</div>
				<div class="form-group">
                <label for="default" class="col-sm-2 control-label">Year Level</label>
                    <div class="col-sm-5">
                    <select name="year_level" class="form-control" id="default" required="required">
							<option value="">-- Select Year Level -- </option>
							<?php if($result->year_level == '1') { ?>
							<option value="1" selected>1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<?php } else if($result->year_level == '2') { ?>
							<option value="1" >1</option>
							<option value="2" selected>2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<?php } else if($result->year_level == '3') { ?>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" selected>3</option>
							<option value="4">4</option>
							<?php } else if($result->year_level == '4') { ?>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" selected>4</option>
							<?php } ?>
							
                    </select>
                </div>
				</div>
			
				<div class="form-group">
                <label for="default" class="col-sm-2 control-label">Subject Code</label>
					<div class="col-sm-5">
					<input type="text" name="subject_code" class="form-control" value="<?php echo htmlentities($result->subject_code);?>"  id="default" placeholder="Subject Code" required="required">
                   </div>
                </div>

                <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Descriptive Title</label>
                    <div class="col-sm-5">
						<input type="text" name="descriptive_title" value="<?php echo htmlentities($result->descriptive_title);?>" class="form-control" id="default" placeholder="Subject Name" required="required">
                    </div>
                </div>

                <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Units</label>
                    <div class="col-sm-5">
						<input type="text" name="units" value="<?php echo htmlentities($result->units);?>" class="form-control" id="default" placeholder="Units" required="required">

                    </div>
                </div>

                <div class="form-group">
                 <label for="default" class="col-sm-2 control-label">Pre-requisites</label>
                    <div class="col-sm-5">
						<input type="text" name="pre_requisite" value="<?php echo htmlentities($result->pre_requisite);?>" class="form-control" id="default" placeholder="" required="required">

                     </div>
                </div>
                <?php }} ?>

                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" name="Update" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
