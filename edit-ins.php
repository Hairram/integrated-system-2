<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{

	$stid=intval($_GET['stid']);

	if(isset($_POST['submit']))
	{
		$faculty_id   = $_GET['view'];
		$firstname    = $_POST['firstname'];
		$middlename   = $_POST['middlename'];
		$lastname     = $_POST['lastname'];
		$instructorid = $_POST['instructorid']; 
		$address      = $_POST['address'];
		$email        = $_POST['email']; 
		$department   = $_POST['department']; 
		$dob          = $_POST['dob']; 
		$status       = $_POST['status'];
		$sql1		  = "UPDATE tbl_faculty_info SET lastname=?, firstname=?, middlename=? , address=? , email=? , DOB =?,status=?,dept_id=?  WHERE faculty_id  =?";

		$dbh->prepare($sql1)->execute([$lastname, $firstname, $middlename,$address,$email,$dob,$status,$department,$faculty_id]);
		$msg="Instructor info updated successfully";
	}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin| Update Instructor </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
		</head>
		<body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
                    <!-- ========== LEFT SIDEBAR ========== -->
					<?php include('includes/leftbar-ph.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Instructor Update</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                
                                        <li class="active">Instructor Update</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Update Instructor info</h5>
                                                </div> 
                                            </div>
                                            <div class="panel-body">
											<?php if($msg){?>
											<div class="alert alert-success left-icon-alert" role="alert">
											 <strong>Well done! </strong><?php echo htmlentities($msg); ?>
											 </div><?php } 
											else if($error){?>
											<div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
											</div>
											<?php } ?>
                                            <form class="form-horizontal" method="post">
														<?php 
														$sql = "SELECT * from tbl_faculty_info where  faculty_id =:stid";
														$query = $dbh->prepare($sql);
														$query->bindParam(':stid',$_GET['view'],PDO::PARAM_STR);
														$query->execute();
														$results=$query->fetchAll(PDO::FETCH_OBJ);
														$cnt=1;
														if($query->rowCount() > 0)
														{
														foreach($results as $result)
														{  ?>


														<div class="form-group">
														<label for="default" class="col-sm-2 control-label">Full Name</label>
														<div class="row">
															<div class="col-sm-3">
																<input type="text" name="firstname" class="form-control" id="fullanme" value="<?php echo htmlentities($result->firstname)?>" required="required" autocomplete="off">
															</div>
															<div class="col-sm-3">
																<input type="text" name="middlename" class="form-control" id="fullanme" value="<?php echo htmlentities($result->middlename)?>" required="required" autocomplete="off">
															</div>
															<div class="col-sm-3">
																<input type="text" name="lastname" class="form-control" id="fullanme" value="<?php echo htmlentities($result->lastname)?>" required="required" autocomplete="off">
															</div>
														</div>
														</div>

														<div class="form-group">
														<label for="default" class="col-sm-2 control-label">Instrucor ID</label>
														<div class="col-sm-5">
														<input type="text" name="instructorid" class="form-control" id="rollid" value="<?php echo htmlentities($result->faculty_id )?>" required="required" autocomplete="off" readonly>
														</div>
														</div>

														<div class="form-group">
														<label for="default" class="col-sm-2 control-label">Email</label>
														<div class="col-sm-5">
														<input type="email" name="email" class="form-control" id="email" value="<?php echo htmlentities($result->email)?>" required="required" autocomplete="off">
														</div>
														</div>

														<div class="form-group">
														<label for="default" class="col-sm-2 control-label">Address</label>
														<div class="col-sm-5">
														<input type="text" name="address" class="form-control" id="address" value="<?php echo htmlentities($result->address)?>" required="required" autocomplete="off">
														</div>
														</div>

														<div class="form-group">
                                                        <label for="date" class="col-sm-2 control-label">DOB</label>
                                                        <div class="col-sm-5">
														<input type="date"  name="dob" class="form-control" value="<?php echo htmlentities($result->DOB)?>" id="date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Select Department</label>
                                                        <div class="col-sm-5">
                                                        <select name="department" class="form-control">
															<?php $sql = "SELECT * from tbl_department";
															$query = $dbh->prepare($sql);
															$query->execute();
															$results2=$query->fetchAll(PDO::FETCH_OBJ);
															if($query->rowCount() > 0)
															{
															foreach($results2 as $result2)
															{   
															if($result->dept_id == $result2->dept_id ){
															?>
															.<option value="<?php echo $result2->dept_id ; ?>" selected><?php echo htmlentities($result2->dept_name); ?></option>
															<?php } else { ?>
															.<option value="<?php echo $result2->dept_id; ?>" ><?php echo htmlentities($result2->dept_name); ?></option>
															<?php }} } ?>
															</select>
                                                        </div>
                                                    </div>


													<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Status</label>
													<div class="col-sm-10">
													<?php  $stats=$result->status;
													if($stats=="1")
													{
													?>
													<input type="radio" name="status" value="1" required="required" checked> Active <input type="radio" name="status" value="0" required="required"> Block 
													<?php }?>
													<?php  
													if($stats=="0")
													{
													?>
													<input type="radio" name="status" value="1" required="required" > Active <input type="radio" name="status" value="0" required="required" checked> Block 
													<?php }?>


													</div>
													</div>

													<?php }} ?>                                                    

                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" name="submit" class="btn btn-warning">Update</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
