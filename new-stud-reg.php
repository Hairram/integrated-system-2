<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php'); 


if(isset($_POST['submit']))
{
$firstname     = mysqli_real_escape_string($conn,$_POST['firstname']);
$middlename    = mysqli_real_escape_string($conn,$_POST['middlename']);
$lastname      = mysqli_real_escape_string($conn,$_POST['lastname']);
$course_id     = mysqli_real_escape_string($conn,$_POST['course_id']);
$gender        = mysqli_real_escape_string($conn,$_POST['gender']);
$address       = mysqli_real_escape_string($conn,$_POST['address']);
$contactnumber = mysqli_real_escape_string($conn,$_POST['contactnumber']);
$year_level	   = mysqli_real_escape_string($conn,$_POST['year_level']);
$email	       = mysqli_real_escape_string($conn,$_POST['email']);
$schoolyear	   = mysqli_real_escape_string($conn,$_POST['school_year']);
$semester	   = mysqli_real_escape_string($conn,$_POST['semester']);
$examscore	   = mysqli_real_escape_string($conn,$_POST['examscore']);
$DOB	  	   = mysqli_real_escape_string($conn,$_POST['DOB']);

// GET AUTO STUDENT ID / /
$studid        = mysqli_query($conn,"select * from tbl_student order by student_id desc") ;
$idresult      = mysqli_fetch_assoc($studid);
if($idresult ==0){
$studentID     = date('Y') .'-1-00001';
}else{
$pieces = explode("-", $idresult['student_id']);
$id            = str_pad($pieces[2], 5, '0',STR_PAD_LEFT) +   1;
$sid           = str_pad($id, 5, '0',STR_PAD_LEFT);
$studentID     = date('Y') .'-1-'.$sid;
}

// CREATE FOLDER FILE FOR STUDENT REQUIREMENTS
dirname(__FILE__) . 'images/students/'. mkdir($studentID);

// DIPLOMA // 
$diploma    = addslashes(file_get_contents($_FILES['diploma']['tmp_name']));
$image_name = addslashes($_FILES['diploma']['name']);
$image_size = getimagesize($_FILES['diploma']['tmp_name']);
move_uploaded_file($_FILES["diploma"]["tmp_name"], "images/students/" . $_FILES["diploma"]["name"]);
$diplomas   =  $_FILES["diploma"]["name"];

// GOODMORAL // 
$goodmoral    = addslashes(file_get_contents($_FILES['goodmoral']['tmp_name']));
$image_name   = addslashes($_FILES['goodmoral']['name']);
$image_size   = getimagesize($_FILES['goodmoral']['tmp_name']);
move_uploaded_file($_FILES["goodmoral"]["tmp_name"], "images/students/" . $_FILES["goodmoral"]["name"]);
$goodmorals   =  $_FILES["goodmoral"]["name"];

// POLICE CLEARANCE // 
$police   	  = addslashes(file_get_contents($_FILES['police']['tmp_name']));
$image_name   = addslashes($_FILES['police']['name']);
$image_size   = getimagesize($_FILES['police']['tmp_name']);
move_uploaded_file($_FILES["police"]["tmp_name"],  "images/students/" . $_FILES["police"]["name"]);
$polices      =  $_FILES["police"]["name"];

// EXAM RESULT // 
$examresult   = addslashes(file_get_contents($_FILES['examresult']['tmp_name']));
$image_name   = addslashes($_FILES['examresult']['name']);
$image_size   = getimagesize($_FILES['examresult']['tmp_name']);
move_uploaded_file($_FILES["examresult"]["tmp_name"],  "images/students/". $_FILES["examresult"]["name"]);
$examresults  =  $_FILES["examresult"]["name"];

// 2X2 PICTURE // 
$image   	  = addslashes(file_get_contents($_FILES['image']['tmp_name']));
$image_name   = addslashes($_FILES['image']['name']);
$image_size   = getimagesize($_FILES['image']['tmp_name']);
move_uploaded_file($_FILES["image"]["tmp_name"],  "images/students/" . $_FILES["image"]["name"]);
$images       =  $_FILES["image"]["name"];



$sql = mysqli_query($conn,"INSERT INTO tbl_student (student_id, 
													firstname,
													middlename,
													lastname,
													course_id,
													gender , 
													address ,
													contact_number,
													email , 
													year_level , 
													school_year ,
													semester,
													entrance_exam_result,
													form_137,
													good_moral,
													police_clearance,
													exam_result,
													picture,
													password,
													DOB
													
													) 
											VALUES ('$studentID', 
													'$firstname',
													'$middlename',
													'$lastname',
													'$course_id',
													'$gender',
													'$address',
													'$contactnumber',
													'$email',
													'$year_level' ,
													'$schoolyear',
													'$semester',
													'$examscore',
													'$diplomas',
													'$goodmorals',
													'$polices',
													'$examresults',
													'$images',
													'$studentID',
													'$DOB'
													)
												") ;



$getclass       = "SELECT a.* , b.* FROM tbl_classes a
						LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
						WHERE a.course_id='$course_id' and a.year_level='$year_level' and b.semester='$semester'";
$getclassres    = mysqli_query($conn, $getclass);
while($res = $getclassres->fetch_assoc()) {		
		$class_id = $res['class_id'];
		$conn->query("insert into tbl_grades (student_id , class_id , year_level , semester , school_year ) VALUES ('$studentID','$class_id','$year_level','$semester','$schoolyear')");
}	

$misc 		  = mysqli_query($conn, "SELECT * FROM tbl_miscellaneous") or mysqli_error($conn);
while($row1   = $misc->fetch_assoc()) {
$miscellaneous_id 	  = $row1['miscellaneous_id'];
	$conn->query("INSERT INTO tbl_student_miscellaneous_fee (student_id , miscellaneous_id , school_year,semester) VALUES ('$studentID','$miscellaneous_id' ,'$schoolyear' , '$semester')"); 
}

  echo "<script>
  alert('Registered successfully');
  window.location.href='new-stud-reg.php';
  </script>";


}

$semester  = mysqli_query($conn,"select * from tbl_sy_sem where isStatus =1") ;
$semresult = mysqli_fetch_assoc($semester);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style1.css" media="screen" >
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
        <style type="text/css">
            .margin-input-bot{
                margin-top: 5%;
            }
        </style>
    </head>
    <body >
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   
                    <!-- /.left-sidebar -->

                    <div class="page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-4 col-md-offset-4 text-center">
                        <a href="index.php">
                        <img src="images/LogoOcc.png" alt="Home" width="150" height="130"></a>
                        <h6 align="center" style="margin-bottom: 10px; margin: 0px ; padding: 0px;"> <b>Opol Community College <br>
                                           Opol, Misamis Oriental <br>
                                           Tel.No.:(08) 555-0518 </b></h6></a>
                        
                    </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                          
                        </div>
                        <div class="container-fluid">
                           
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="cold-md-1 cold-md-offset-4 text-center">
                                                     <h3 style=""><b>NEW STUDENT SIGN UP</b></h3> 
													 	<b>SCHOOL YEAR : </b> <?php echo $semresult['school_year'];?>
														<br>
													 	<b>SEMESTER : </b> <?php echo $semresult['semester'];?>
                                                </div>
                                            </div>
                                            <div class="panel-body">
											<?php if($msg){?>
											<div class="alert alert-success left-icon-alert" role="alert">
											 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
											 </div><?php } 
											else if($error){?>
											<div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
										<div class="row justify-content-center">
										  <div class="col-md-12 text-center">
											<h4>STUDENT INFORMATION : </h4>
											</div>
										</div>
										<form action="new-stud-reg.php" class="form-horizontal"method="post" enctype="multipart/form-data">
										<div class="form-group">
											<label for="default" class="col-sm-2 control-label">Last Name</label>
												<div class="col-sm-5">
													<input type="text" name="lastname" class="form-control" id="lastname"  required="required">
													<input type="hidden" name="school_year" class="form-control" id="lastname" value="<?php echo $semresult['school_year'];?>"  required="required">
													<input type="hidden" name="semester" class="form-control" id="lastname"  value="<?php echo $semresult['semester'];?>" required="required">
												</div>
										</div>

										<div class="form-group">
											<label for="default" class="col-sm-2 control-label">First Name</label>
												<div class="col-sm-5">
													<input type="text" name="firstname" class="form-control" id="firstname"  required="required" >
												</div>
										</div>

										<div class="form-group">
											<label for="default" class="col-sm-2 control-label">Middle Name</label>
												<div class="col-sm-5">
													<input type="text" name="middlename" class="form-control" id="middlename"  required="required" >
												</div>
										</div>

										<div class="form-group">
											<label for="default" class="col-sm-2 control-label">Gender</label>
												<div class="col-sm-5">
													<input type="radio" name="gender" value="Male" required="required" checked> Male 
													<input type="radio" name="gender" value="Female" required="required"> Female 
													<input type="radio" name="gender" value="Other" required="required"> Other
												</div>
										</div>
										
										<div class="form-group">
											<label for="default" class="col-sm-2 control-label">Home address</label>
												<div class="col-sm-5">
													<input type="text" name="address" class="form-control" id="firstname"  required="required" autocomplete="off">
												</div>
										</div>
										<div class="form-group">
											<label for="default" class="col-sm-2 control-label">Contact Number</label>
												<div class="col-sm-5">
													<input type="text" name="contactnumber" class="form-control" id="firstname"  required="required" autocomplete="off">
												</div>
										</div>
										<div class="form-group">
											<label for="default" class="col-sm-2 control-label">Email Address</label>
												<div class="col-sm-5">
													<input type="text" name="email" class="form-control" id="firstname"  required="required" autocomplete="off">
												</div>
										</div>


										<div class="form-group">
											<label for="date" class="col-sm-2 control-label">Date of Birth</label>
											<div class="col-sm-5">
												<input type="date"  name="DOB" class="form-control"  id="date" required>
											</div>
										</div>


										<div class="form-group">
										<label for="default" class="col-sm-2 control-label">Select Course</label>
										<div class="col-sm-5">
											<select  class="form-control" id="course" name="course_id" required onchange="get_subjects()">
											<option value="" >-- Select Course --</option>
											<?php $sql = "select * from tbl_courses  order by course_code asc";
												  $q   = $conn->query($sql);
													while($r = $q->fetch_assoc()){
													  echo '<option value="'.$r['course_id'].'"  '.(($course_code==$r['course_id'])?'selected="selected"':'').'>'.$r['course_name'].'</option>';
												}
											?>
											</select>
											</div>
										</div>
										<div class="form-group">
										<label for="default" class="col-sm-2 control-label">Year Level</label>
										<div class="col-sm-5">
											<select name="year_level" class="form-control" id="default" required="required">
												<option selected="selected" value="">-- Select Year Level -- </option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
										</div>
										</div>
										<div class="row justify-content-center">
										  <div class="col-md-12 text-center">
											<h4>REQUIREMENTS INFORMATION : </h4>
											</div>
										</div>
										<div class="form-group">
											<label for="date" class="col-sm-2 control-label">Entrance Exam Score</label>
											<div class="col-sm-5">
												<input type="text"  name="examscore" class="form-control" id="date" >
											</div>
										</div>
										<div class="form-group">
											<label for="date" class="col-sm-2 control-label">SHS Diploma /Form 137/ ALS</label>
											<div class="col-sm-5">
												<b>(Upload Photo)</b>
												<input type="file"  name="diploma" class="form-control" id="date" >
											</div>
										</div>

										<div class="form-group">
											<label for="date" class="col-sm-2 control-label">SHS Good Moral</label>
											<div class="col-sm-5">
												<b>(Upload Photo)</b>
												<input type="file"  name="goodmoral" class="form-control" id="date" >
											</div>
										</div>
										
										<div class="form-group">
											<label for="date" class="col-sm-2 control-label">Police Clearance</label>
											<div class="col-sm-5">
												<b>(Upload Photo)</b>
												<input type="file"  name="police" class="form-control" id="date" >
											</div>
										</div>
										
										<div class="form-group">
											<label for="date" class="col-sm-2 control-label">Entrance Exam Result</label>
											<div class="col-sm-5">
												<b>Upload Picture of Exam Result</b>
												<input type="file"  name="examresult" class="form-control" id="date" >
											</div>
										</div>
										<div class="form-group">
											<label for="date" class="col-sm-2 control-label">2x2 Picture</label>
											<div class="col-sm-5">
												<b>(Upload Photo)</b>
												<input type="file"  name="image" class="form-control" id="date" >
											</div>
										</div>

	
										<div class="form-group text-center">
												
										<button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                        </div>
										<div class="col-sm-12 text-center">
												<i><h5 class="text-danger" class="margin-input-bot">**Note! Submit your Documents(Diploma, Form 137, Good Moral etc.) at the Registrar Office.</h5></i>
										</div>
										</form>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>

        <script type="text/javascript">
		function get_subjects() {
        $(document).ready(function () {

            
            var c_id = document.getElementById("course").value;

            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "get_subjects.php",
                data:{
                    c_id: c_id,
                },
                dataType: "html",   //expect html to be returned
                success: function (response) {
                    $("#tbody_container").html(response);
                    // console.log(response);
                }
            });

        });
    }
</script>
<script type="text/javascript">

    

     function add_miscallaneous_fee(fee,checkboxElem) {

        if (checkboxElem.checked) {
            var  total_fee=document.getElementById('fees').value;
            var  miscallaneous_fee=fee;

            var total_fee=Number(total_fee)+Number(fees_amount);

            document.getElementById('fees').value=total_fee;

        } else {
            var  total_fee=document.getElementById('fees').value;
            var  miscallaneous_fee=fee;

            var total_fee=Number(total_fee)-Number(fees_amount);

            document.getElementById('fees').value=total_fee;

        }

    }
</script>
    </body>
</html>


