<?php
session_start();
error_reporting(0); 
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_GET['curr_sub_id'])){
        $curr_sub_id_g = $_GET['curr_sub_id'];
        /*$getYearLevel = "IFNULL((Select yr_level from tbl_curr_subjects i where i.curr_id=cu.curr_id limit 1), 'N/A') as YearLevel";
        $getSemester = "IFNULL((Select semester from tbl_curr_subjects i where i.curr_id=cu.curr_id limit 1), 'N/A') as Semester";
        $getEffectiveSY = "IFNULL((Select effective_sy from tbl_curr_subjects i where i.curr_id=cu.curr_id limit 1), 'N/A') as EffectiveSY";
        $getRevisedSY = "IFNULL((Select revised_sy from tbl_curr_subjects i where i.curr_id=cu.curr_id limit 1), 'N/A') as RevisedSY";*/
         $getCurrName = "IFNULL((Select curr_name from tbl_curriculum i where i.curr_id=cu.curr_id limit 1), 'N/A') as CurriculumName";
          $getCurrDescription = "IFNULL((Select curr_description from tbl_curriculum i where i.curr_id=cu.curr_id limit 1), 'N/A') as CurriculumDescription";
        $sql = "SELECT cu.*,".$getCurrName.",".$getCurrDescription." FROM tbl_curr_subjects cu WHERE curr_sub_id=:stid";
        $query = $dbh->prepare($sql);
        $query->bindParam(':stid',$curr_sub_id_g,PDO::PARAM_STR);
        $query->execute();  
        $results=$query->fetchAll(PDO::FETCH_OBJ);
        foreach($results as $result){
            $CurriculumName = $result->curr_name;
            $CurriculumDescription = $result->curr_description; 
            $YearLevel = $result->yr_level;
            $EffectiveSY = $result->effective_sy;
            $RevisedSY = $result->revised_sy;
            $Semester = $result->semester;
            
            
        }
    }else{
      /*  header("Location: manage-curriculum.php"); */
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style1.css" media="screen" >
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
        <style type="text/css">
            .margin-input-bot{
                margin-top: 5%;
            }
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar.php');?>


                    <div class="page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-4 col-md-offset-4 text-center">
                        <img src="images/LogoOcc.png" width="85" height="85" style="position: absolute; left: 10px; margin-top: 35px;">
                        <h6 align="center"> <b>OPOL COMMUNITY COLLEGE <br>
                        Opol, Misamis Oriental <br>
                            <?php echo htmlentities($CurriculumName);?>
                           <p>Revised S.Y. :</p> <?php echo htmlentities($RevisedSY);?>
                           <p>Effective S.Y. :</p> <?php echo htmlentities($RevisedSY);?>
                                           </b></h6>
                        
                    </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                          
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <!--  <h5>Fill the blanks</h5>  -->
                                                </div>
                                            </div>
                                        


    <div class="panel-body p-20">
        <table border="1" class="table table-hover table-bordered" style="width:100% !important;">
            <thead>

                <tr>
                    <th colspan="8" style="text-align:center;">asdasd<?php echo htmlentities($YearLevel);?></th>
                </tr>

                <tr>
                    <th colspan="8" style="text-align:center;">asdasd<?php echo htmlentities($Semester);?></th>
                </tr>
                    <tr>    
                        <th class="text-center">Subject Code</th>
                        <th class="text-center">Descriptive Title</th>    
                        <th class="text-center">Credit Units</th>
                        <th class="text-center">Lec Hours</th>
                        <th class="text-center">Lab Hours</th>
                        <th class="text-center">Hrs/Week</th>
                        <th class="text-center">PRE-REQUISITES</th>
                    </tr>
            </thead>
  <!-- <tbody>
<?php 
$sql = "SELECT * ";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   
?>
<tr>
        
        <td><?php echo htmlentities($result->subject_code);?></td>
        <td><?php echo htmlentities($result->descriptive_title);?></td>
        <td><?php echo htmlentities($result->units);?></td>
        <td><?php echo htmlentities($result->lec_hrs);?></td>
        <td><?php echo htmlentities($result->lab_hrs);?></td>
        <td><?php echo htmlentities($result->hrs_week);?></td>
        <td><?php echo htmlentities($result->pre_requisite);?></td>
</tr>
<?php $cnt=$cnt+1;}} ?>
                                                       
                                                    
                                                    </tbody> -->
                                                </table>


                                                    

                                                        </tr>

                                    
                                        </div>
                                    </div>

                                    
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

            
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>


<?php } ?>