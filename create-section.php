<?php
session_start();
error_reporting(0);
include('includes/config.php');
include ('includes/dbconnect.php');
if(strlen($_SESSION['alogin'])=="") 
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$section_name = $_POST['section_name'];
$course_id    = $_POST['course_id']; 
$year_level   = $_POST['year_level']; 
$semester     = $_POST['semester'];
$school_year  = $_POST['school_year'];

$check = "select * from tbl_section where course_id ='$course_id' and  year_level= '$year_level' and semester ='$semester' and school_year ='$school_year' and section_name ='$section_name'";
$checkres  = mysqli_query($conn, $check);
$rowcount=mysqli_num_rows($checkres);

if($rowcount == 0){
$sql = "INSERT INTO tbl_section (section_name,course_id,year_level,semester,school_year) VALUES (?,?,?,?,?)";
$dbh->prepare($sql)->execute([$section_name, $course_id, $year_level,$semester,$school_year]);

$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$_SESSION['msg']="Section created Successfully";
header("Location: manage-sections.php");
}
else 
{
$error="Something went wrong. Please try again";
}
} else {
$error="Duplicate Data. Please try again";
}

}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
      
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Create Section</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage Sections</li>
                                        <li class="active">Create Section</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">`
                                                <div class="panel-title">
                                                    <h5><b>Create Section</b></h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
					<?php if($msg){?>
					<div class="alert alert-success left-icon-alert" role="alert">
					 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
					 </div><?php } 
					else if($error){?>
					<div class="alert alert-danger left-icon-alert" role="alert">
                     <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                    </div>
					<?php } ?>
					<form class="form-horizontal" method="post">
					<?php 
					$sql1 = "SELECT * from tbl_sy_sem where isStatus =1";
					$query1 = $dbh->prepare($sql1);
					$query1->execute();
					$results1=$query1->fetchAll(PDO::FETCH_OBJ);
					foreach($results1 as $a => $b){
					$sy  =  $b->school_year;
					$sem =  $b->semester;
					}
					?>

					 <div class="form-group">
						 <label for="default" class="col-sm-2 control-label">School Year</label>
							<div class="col-sm-5">
							<input type="text" name="school_year" class="form-control" value="<?php echo $sy;?>"  required="required" maxlength="9" readonly>
						</div>
					</div>

                    <div class="form-group">
                        <label for="default" class="col-sm-2 control-label">Semester</label>
                        <div class="col-sm-5">
							<input type="text" name="semester" class="form-control" value="<?php echo $sem;?>" required="required" maxlength="9" readonly>
                           
                        </div>
                    </div>

					<div class="form-group">
						<label for="default" class="col-sm-2 control-label">Course</label>
						<div class="col-sm-5">
						<select id="CourseNames" name="course_id" class="form-control" required>
						<option value=""> -- Select Course -- </option>
						<?php $sql = "SELECT * from tbl_courses order by course_code";
						$query = $dbh->prepare($sql);
						$query->execute();
						$results=$query->fetchAll(PDO::FETCH_OBJ);
						if($query->rowCount() > 0)
						{
						foreach($results as $result)
						{   ?>
						<option value="<?php echo htmlentities($result->course_id); ?>"><?php echo htmlentities($result->course_name); ?></option>
						<?php }} ?>
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="default" class="col-sm-2 control-label">Year Level</label>
						<div class="col-sm-5">
							<select name="year_level" class="form-control" id="default" required="required">
								<option selected="selected" value="">- Select Year Level-</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
					</div>

            <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Section</label>
                <div class="col-sm-5">
                    <select name="section_name" class="form-control" id="default" required="required">
                        <option selected="selected" value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="F">F</option>
                    </select>
                </div>
            </div>

                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-5">
                        <button type="submit" name="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
                        
                </form>

                                            </div>
                                        </div>
                                    </div>
                                 <!--/.col-md-12 -->
                                </div>
                    </div>

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
