
<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Intructor Load</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #dd3d36;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
		.succWrap{
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #5cb85c;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
			<?php include('includes/leftbar-ph.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"> Instructor Load</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-proghead.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li>  Instructors</li>
            							<li class="active"> Assigned Loads</li>
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                
                                                </div>
                                            </div>
                                            <div class="panel-body p-20">

                                                <h4>Instructor Name: <?php echo $_GET['name']; ?></h4>
                                                <hr>
                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>School Year</th>
                                                            <th>Semester</th>
                                                            <th>Section</th>
                                                            <th>Subject Code</th>
                                                            <th>Descriptive Title</th>
                                                            <th>Units</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

													<?php 
													
														// GET ACTIVE SY //
														$sql     = "SELECT * from tbl_sy_sem where isStatus =1";
														$query   = $dbh->prepare($sql);
														$query->execute();
														$results = $query->fetchAll(PDO::FETCH_OBJ);
													
														$cnt      = 0;
														$sid      = $_GET['view'];
														$sql2     = "   SELECT a.*,b.* from tbl_classes a 
																		LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
																		where a.faculty_id =:sid ";
														$query2   = $dbh->prepare($sql2);
														$query2->bindParam(':sid',$sid,PDO::PARAM_STR);
														$query2->execute();
														$results2 = $query2->fetchAll(PDO::FETCH_OBJ);
														foreach($results2 as $results2){ 
														$cnt++;
														?>
                                                        <tr>
                                                            <td class="text-center"><?php echo $cnt;?></td>
                                                            <td class="text-center"><?php echo $results[0]->school_year;?></td>
                                                            <td class="text-center"><?php echo $results2->semester;?></td>
                                                            <td class="text-center"><?php echo $results2->section;?></td>
                                                            <td class="text-center"><?php echo $results2->subject_code;?></td>
                                                            <td class="text-center"><?php echo $results2->descriptive_title;?></td>
                                                            <td class="text-center"><?php echo $results2->units;?></td>
                                                        </tr>
													<?php 
													$total_units+=$results2->units;
													
													}
													?>
													<tr>
														<th colspan="6" scope="row" class="text-right" colspan="2">Total Units:</th>           
														<td class="text-center"><?php echo htmlentities($total_units); ?></td>
													</tr>
														 
													<?php $cnt=$cnt+1; ?>
                                                </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->
									<div class="form-group">
                                        <div class="col-sm-10 text-right">
                                        <i class="text-danger">Note! Instructors maximum load is 24 units only.</i>
                                        </div>
                                                               
                                      </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>


