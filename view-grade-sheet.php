<?php
error_reporting(0);
include ('includes/dbconnect.php');
if(isset($_POST["submit"]))
{
	$grade	 =  $_POST['marks'];
	$id		 =  $_POST['gradeID'];
	if($grade > 3){
		$remarks = 'Fail';
	} else {
		$remarks = 'Pass';
	}
	mysqli_query($conn,"UPDATE tbl_grades set grade ='$grade', remarks='$remarks' where id ='$id'");
	echo "<script> alert('Grade Added!'); </script>";
}	
if(isset($_POST["import"]))
{
    // $extension = end(explode(".", $_FILES["excel"]["name"])); // For getting Extension of selected file
    $value = explode(".", $_FILES["excel"]["name"]);
    $extension = strtolower(array_pop($value));
    $allowed_extension = array("csv"); //allowed extension
    if(in_array($extension, $allowed_extension)) //check selected file extension is present in allowed extension array
    {
    $file = $_FILES["excel"]["tmp_name"]; // getting temporary source of excel file
    include("../OccGradeInquiry /Classes/PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code

    $objPHPExcel = PHPExcel_IOFactory::load($file); // create object of PHPExcel library by using load() method and in load method define path of selected file

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
    {
    $highestRow = $worksheet->getHighestRow();
    for($row=2; $row<=$highestRow; $row++)
    {
    $a = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(0, $row)->getValue());
    $b = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(2, $row)->getValue());
    $c = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
    $query_delete = "DELETE FROM tblresult where AssignStudentid=(Select a.id FROM tblassignstudent a, tblstudents s WHERE s.StudentId=a.Studentid and a.Classid=".$_GET['Classid']." and s.RollId='".$a."')";
    mysqli_query($connect, $query_delete);
        if($c == 'INC'){
            $INC = 1;
        }else{
            $INC = 0;
        }
    $query = "INSERT INTO tblresult(AssignStudentid,marks,INC,remarks) VALUES ((Select a.id FROM tblassignstudent a, tblstudents s WHERE s.StudentId=a.Studentid and a.Classid=".$_GET['Classid']." and s.RollId='".$a."'), '".$b."', '".$INC."', '".$c."')";
    mysqli_query($connect, $query);
    }
    }
    $output .= "<label class='text-success'>Imported Successfully!</label>";
    }
    else
    {
    $output = '<label class="text-danger">Invalid File</label>'; //if non excel file then
    }
}
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
       
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #dd3d36;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
		.succWrap{
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #5cb85c;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ins.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
			<?php include('includes/leftbar1.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>View Grade Sheet</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-instructor2.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Classes</li>
            							<li class="active">List of Classes </li>
                                        <li class="active">View Grade Sheet </li>
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

									<?php
										$sql   		 = "SELECT * FROM tbl_sy_sem WHERE isStatus=1";
										$result		 = mysqli_query($conn, $sql);
										$row    	 = mysqli_fetch_assoc($result);
										$lock   	 = $row['isLock'];
									 ?>

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
												<p><b>S.Y.:</b> <?php echo $_GET['sy'];?></p>
												<p><b>Semester:</b>  <?php echo $_GET['semester'];?></p>
												<p><b>Section:</b> <?php echo $_GET['section'];?></p>
                                                </div>
                                            </div>
											<?php if($msg){?>
											<div class="alert alert-success left-icon-alert" role="alert">
											 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
											 </div><?php } 
											else if($error){?>
												<div class="alert alert-danger left-icon-alert" role="alert">
																						<strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
																					</div>
											<?php } ?>
											
                                            <div class="panel-body p-20">
                                                <div class="row">
                                                
                                                </div>
                                                <div class="row">
                                                <form method="post" enctype="multipart/form-data">
                                                    <div class="col-sm-5 text-right"><i class="text-danger">Upload .csv (Student Id, Student Fullname, Grades, Remarks)</i></div>
                                                    <div class="col-sm-5">
                                                        <div class="">
                                                            <input type="file" accept=".csv" name="excel" class="margin-input-bot form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-left">
                                                        <input type="submit" name="import" class="btn btn-info" value="Import" />
                                                    </div>
                                                    <div class="col-sm-12 text-right">
                                                        <?php echo $output; ?>
                                                    </div>
                                                </form>
                                                <div class="row">
                                                    <div class="col-sm-11 col-sm-offset-1">
                                                    <!-- <a href="manage-student-grades.php?Classid=<?php echo htmlentities($Classid_g); ?>&DeclareResult=true"><input type="button" class="btn btn-primary" name="assign" value="Declare Result"> </a>  -->
                                                    </div>
                                                </div>
                                                </div>
                                                <hr>
                                                    <div class="panel-title">
                                                        <h5><b>List of Students</b></h5>
                                                    </div>
                                                    <br>
                                                <table id="example1" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Student ID</th>
                                                            <th>Student Fullname</th>
                                                            <th>Subject</th>
                                                            <th>Course</th>
                                                            <th>Grade</th>
                                                            <th>Remarks</th>
                                                            <th>Action</th>
                                                        </tr>


                                                    </thead>
                                                    <tbody>
														<?php 
														 $classid = $_GET['class_id'];
														 $sql1 =  "SELECT a.* , b.student_id , b.lastname, b.firstname , c.* FROM tbl_grades a
																	LEFT JOIN tbl_student b on a.student_id = b.student_id
																	LEFT JOIN tbl_classes c on a.class_id = c.class_id
																	where a.class_id='$classid'";
															$result1 = mysqli_query($conn, $sql1);
															$x=1;
															while($row = $result1->fetch_assoc()) {
															$courseid  = $row['course_id'];
															$subjectid = $row['subject_id'];
														?>
                                                        <tr>
                                                            <tr>
															<td><?php echo $x++;?></td>
															<td class="text-center"><?php echo htmlentities($row['student_id']);?></td>
															<td class="text-center"><?php echo htmlentities($row['lastname'] . ',' . $row['firstname']);?></td>
															<td>
															<?php 
															$subject     =  "SELECT * from tbl_subjects where subject_id='$subjectid' ";
															$subjectres  = mysqli_query($conn, $subject);
															$res1 = $subjectres->fetch_assoc();
															echo $res1['subject_code'];
															?>
															</td>
															<td class="text-center">
															<?php 
															$course     =  "SELECT * from tbl_courses where course_id='$courseid' ";
															$courseres  = mysqli_query($conn, $course);
															$res = $courseres->fetch_assoc();
															echo $res['course_code'];
															?>
															</td>
															<td class="text-center"><?php echo htmlentities($row['grade']);?></td>
															<td class="text-center"><?php echo htmlentities($row['remarks']);?></td>
                                                            <td class="text-center">
															<?php if($lock ==1){
																echo " SEMESTER LOCKED! ";
															} else { ?>
															<?php if($row['grade'] !="0"){} else {?>
																<input type="button" class="btn btn-info btn-sm" value="Add Grade" data-toggle="modal" data-target="#grades<?php echo $row['student_id'];?>"> 
															<?php } }?>
															</td>
                                                        </tr>
															<div class="modal fade" id="grades<?php echo $row['student_id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
															  <div class="modal-dialog" role="document">
																<div class="modal-content">
																  <div class="modal-header">
																	<div class="row">
																		<div class="col-sm-10">
																			<h4 class="modal-title" id="exampleModalLabel">ADD GRADE</h4>
																		</div>
																		<div class="col-sm-2">
																			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																				<span aria-hidden="true">&times;</span>
																			</button>
																		</div>
																	</div>
																	
																  </div>
																  <div class="modal-body">
																  <form class="row" method="post">
																		<div class="col-sm-12">
																			<div class="form-group">
																				<label for="default" class="col-sm-5 control-label"> Student ID</label>
																				<div class="col-sm-7">
																					<input type="text" name="studentID" class="form-control" id="AssignStudentid" value="<?php echo $row['student_id'];?>" required="required" readonly>
																					<input type="hidden" name="gradeID" class="form-control" id="AssignStudentid" value="<?php echo $row['id'];?>" required="required" readonly>
																				</div>
																			</div>   
																		</div>   
																		<div class="col-sm-12">
																			<div class="form-group">
																				<label for="default" class="col-sm-5 control-label">Grade</label>
																				<div class="col-sm-7">
																					<input type="text" name="marks" class="form-control" id="default" placeholder="Enter Grade" required="required">
																				</div>
																			</div>   
																		</div>   
																  </div>
																  <div class="modal-footer">
																	<div class="form-group">
																		<button type="submit" name="submit" class="btn btn-primary">Add Grade</button>
																	</div>
																  </div>
																  </form> 
																</div>
															  </div>
															</div>
														<?php } ?>

                                                    </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example1').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });

            $(document).on("click", ".exampleModal", function () {
                var passID = $(this).data('id');
                console.log('here');
                $(".modal-body #AssignStudentid").val( passID );
                // As pointed out in comments, 
                // it is unnecessary to have to manually call the modal.
                // $('#addBookDialog').modal('show');
            });
        </script>
    </body>
</html>
<?php } ?>

