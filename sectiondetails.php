<?php
session_start();
error_reporting(0);
include('includes/config.php');
include('../includes/dbconnect.php');

if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{

?>  
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin View Section Details</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
			.errorWrap {
				padding: 10px;
				margin: 0 0 20px 0;
				background: #fff;
				border-left: 4px solid #dd3d36;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
				box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			}
			.succWrap{
				padding: 10px;
				margin: 0 0 20px 0;
				background: #fff;
				border-left: 4px solid #5cb85c;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
				box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
			<?php include('includes/leftbar-ph.php');?>  
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b> Section Class</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage Sections</li>
                                        <li class="active"> View Details</li>
            							
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">
									<?php
									$sid 	  = $_GET['view'];
									$year 	  = $_GET['year_level'];
									$semester = $_GET['semester'];
									$sql11    = "  SELECT a.*,b.* ,c.* from tbl_classes a 
												LEFT JOIN tbl_courses b  on a.course_id = b.course_id
												LEFT JOIN tbl_section c  on a.section = c.section_name
											    where a.section =:sid and a.year_level=:year and c.semester=:semester";
									$query11 = $dbh->prepare($sql11);
									$query11->bindParam(':sid',$sid,PDO::PARAM_STR);
									$query11->bindParam(':year',$year,PDO::PARAM_STR);
									$query11->bindParam(':semester',$semester,PDO::PARAM_STR);
									$query11->execute();
									$results11=$query11->fetchAll(PDO::FETCH_OBJ);
									if(empty($results11)){
										echo "NO DATA AVAILABLE";
									} else {
									foreach($results11 as $result)
									{
										$sectionmame = $result->course_code.'-'.$result->section_name;
										$course 	 = $result->course_code;
										$sy          = $result->school_year;
										$students    = $result->students;
										$semester    = $result->semester;
										$year_level  = $result->year_level;
									}
									?>   
                             

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
													<p style="position: relative; left: 16px"><b>Section Name :</b> <?php echo $year_level. ' - '. $sectionmame;?></p>
													<p style="position: absolute; left: 76%;"><b>Course:</b> <?php echo $course;?></p>
													<p style="position: relative; left: 16px"><b>Shool Year :</b> <?php echo $sy;?></p></p>
													<p style="position: absolute; left: 76%;"><b>No. of Students:</b> <?php echo $students;?></p>
													<p style="position: relative; left: 16px"><b>Semester:</b> <?php echo $semester;?></p>


															
													</b>

													  <div class="panel-body p-20">                                   
													  <table border="1" class="table table-hover table-bordered" style="width:100% !important;">
													  <tbody>
														<p style="position: relative; left: 20px"><b><h2>Classes</h2></p>
														<tr>
                                                            <th class="text-center">No.</th>
                                                            <th class="text-center">Subject Code</th>
                                                            <th class="text-center">Descriptive Title</th> 
                                                             <th class="text-center">Units</th>
                                                            <th class="text-center">Time</th>
                                                            <th class="text-center">Day</th>
                                                            <th class="text-center">Room</th>
                                                            <th class="text-center">Instructor</th>

                                                        </tr>
														<?php 
														 $sql2     = "   SELECT a.*,b.*,c.* from tbl_classes a 
																		LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
																		LEFT JOIN tbl_faculty_info c on c.faculty_id = a.faculty_id
																		where a.section ='$sid' and a.year_level = '$year' and b.semester='$semester'";
														$query2   = $dbh->prepare($sql2);
														$query2->execute();
														$results2 = $query2->fetchAll(PDO::FETCH_OBJ);
														$i = 1;
														foreach($results2 as $resultss2){ ?>
                                                        <tr>
                                                            <td><?php echo $i++;?></td>
                                                            <td><?php echo $resultss2->subject_code;?></td>
                                                            <td><?php echo $resultss2->descriptive_title;?></td>
                                                            <td><?php echo $resultss2->units;?></td>
                                                            <td><?php echo $resultss2->dtime . ' - ' . $resultss2->etime ;?></td>
                                                            <td><?php echo $resultss2->day;?></td>
                                                            <td><?php echo $resultss2->room;?></td>
                                                            <td><?php echo $resultss2->lastname .',' . $resultss2->firstname ;?></td>
                                                        </tr>
														<?php } ?>


											</tbody>

											</table>
											 
												<br>

											 
												</form>  
												
        


                                                </div>
                                            </div>
                                            <br>



                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
									<?php } ?>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
<?php } ?>

