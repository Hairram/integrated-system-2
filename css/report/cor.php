
<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">



<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>

     
<style>
<style>
 @media print {
  @page {
  size: 8.5in x 14in portrait;
    margin: 0;
    padding: 0;
}
} 

body {
  margin: 0px;
  padding: 0px;
}    
        </style>
</style>
</head>
<body style="background:white;">
<!-- Container -->
<div class="container-fluid invoice-container" style="max-width: 1200px!important;padding: 0px!important;border:0px solid black; padding-bottom: 100px;"> 
  <!-- Header -->
  <header>
    <div class="row align-items-center">
  <div class="col-sm-1 text-center text-sm-left mb-3 mb-sm-0"> <h3 class="mb-0"> <img id="logo" src="occ.png" title="Koice" alt="Koice" /></h3>
  </div>
  
      <div class="col-sm-6 text-center text-sm-left mb-1 mb-sm-0"> <h4 class="mb-0">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental
</p></div>
      <div class="col-sm-5 text-center text-sm-right">
        <h6 class="mb-0 text-4">ENROLLMENT FORM</h6>
        <p class="mb-0 text-3">Date Enrolled: 02-22-2021</p>
      </div>
    </div>
  </header>
  
  <!-- Main Content -->
  <main>
    <div class="row text-3">
      <div class="col-sm-6 mb-3"> <strong>Full Name: </strong> <span style="font-size: 15px">Christoper M. Lague</span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Student ID: </strong> <span>2021-1-1515</span> </div>
    </div>
   
    <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span>First Semester</span>
          </div>
          <div class="col-sm-2"> <strong>School Year: </strong><span>2021-2022</span>
          </div>
          <div class="col-sm-3 text-sm-center"> <strong>Course & Year: </strong><span>BIST 1</span>
          </div>
          <div class="col-sm-2"> <strong>Gender: </strong><span>Male</span>
          </div>
          <div class="col-sm-2 text-sm-right"> <strong>Student Type: </strong><span>Old</span>
          </div>
        </div>
      </div>
    </div>
   <hr class="mt-0">
  <main>
     <!-- Passenger Details -->
    <h4 class="text-4">Subject Details</h4>
    <div class="table-responsive">
  
      <table class="table table-bordered text-2 table-sm" style="text-align:center!important;">
    <style>.table thead th {   vertical-align:middle!important;} .table-sm td, .table-sm th {padding: 0px 5px 0px px;}</style>
        <thead>
          <tr >
            <th rowspan="2">No.</th>
            <th rowspan="2">Class Code</th>
            <th rowspan="2">Subject Code</th>
      <th rowspan="2">Description</th>
      <th rowspan="2">Units</th>
        <th colspan="3">Schedule</th>
      </tr>
      <tr>
            
               <th>Day</th>
         <th>Time</th>
         <th>Room</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td>1</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Network Security</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>2</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Multicore and CPU Programming</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>3</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Mobile Media</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>4</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>History of Rizal</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>5</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Programming 2</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px; font-size: 13px;" class="text-sm-right" ><i> Total No. of Units</i></td>
          <td style="border: 0px; border-bottom: 1px solid black;">15</td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
        </tr>
     
        </tbody>
      </table>
      
    </div>

    
  
   <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
        
         
          <div class="col-sm-5"> 
      <h4 class="text-4 mt-4">Miscellaneous Fees Breakdown:</h4>
          <div class="table-responsive">
            <table class="table table-bordered text-2 table-sm">
        
        <tbody>
  <tr>
    <td>Laboratory Fee</td>
    <td>100</td>
    <td>Library Fee</td>
    <td>150</td>
  </tr>

  <tr>
    <td>Athletic Fee</td>
    <td>150</td>
    <td>Detal/Clinic Fee</td>
    <td>150</td>
  </tr>
       
    
        </tbody>
      </table>
    </div>
     <h4 class="text-4 mt-2">Partial Payment:</h4>
          <div class="table-responsive">
            <table class="table table-bordered text-2 table-sm">
        
        <thead>
  <tr>
    <th>Amount</th>
    <th>Date</th>
  </tr> 
        </thead>
        <tbody>
          <tr>
            <td>3,000</td>
            <td>March-1-2021</td>
          </tr>
        </tbody>
      </table>
    </div>
          </div>
      
        <div class="col-sm-7" style="text-align:right!important"><br><br>
          
           <div class="table-responsive">
            <table class="table text-1 table-sm">
        
        <tbody>
          <tr><td  style="border:0px solid red; font-size: 15px;"> <strong>Tuition Fee :</strong> 4,500</td></tr> 
      <tr><td style="border:0px solid red; font-size: 15px;"><strong>Miscellaneous Fees:</strong> 3,000</td></tr>    
      <tr><td style="border:0px solid red; font-size: 15px;"><strong>Total school Fees :</strong> 5,500</td>  </tr> 
      <tr><td style="border:0px solid red; font-size: 15px;"><p><strong>Student Signatures Over Printed Name</strong></p><p class="col-sm-15 text-sm-right" >Christoper M. Lague<br>_________________</p></td></tr> 
    </tbody>
      </table>
    </div>
        
          </div>
          
        </div>
      </div>
    </div>
      
    <!-- Important Info -->
    <div class="row" style="border-bottom:1px dashed black">
     
    </div>
  </main>
  <!-- Footer -->
 </main>
  <!-- Footer -->
 
</div>
 <br/>

<!-- Back to My Account Link -->
 <footer id="breakfooter" > </footer>
<!-- Container -->
<div class="container-fluid invoice-container" style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
  <!-- Header -->
  <header>
    <div class="row align-items-center">
  <div class="col-sm-1 text-center text-sm-left mb-3 mb-sm-0"> <h3 class="mb-0"> <img id="logo" src="occ.png" title="Koice" alt="Koice" /></h3>
  </div>
  
      <div class="col-sm-6 text-center text-sm-left mb-3 mb-sm-0"> <h4 class="mb-0">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental
</p></div>
      <div class="col-sm-5 text-center text-sm-right">
        <h6 class="mb-0 text-4">ENROLLMENT FORM</h6>
        <p class="mb-0 text-3">Date Enrolled: 02-22-2021</p>
      </div>
    </div>
  </header>
  
  <!-- Main Content -->
  <main>
    <div class="row text-3">
      <div class="col-sm-6 mb-3"> <strong>Full Name: </strong> <span style="font-size: 15px">Christoper M. Lague</span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Student ID: </strong> <span>2021-1-1515</span> </div>
    </div>
   
    <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span>First Semester</span>
          </div>
          <div class="col-sm-2"> <strong>School Year: </strong><span>2021-2022</span>
          </div>
          <div class="col-sm-3 text-sm-center"> <strong>Course & Year: </strong><span>BIST 1</span>
          </div>
          <div class="col-sm-2"> <strong>Gender: </strong><span>Male</span>
          </div>
          <div class="col-sm-2 text-sm-right"> <strong>Student Type: </strong><span>Old</span>
          </div>
        </div>
      </div>
    </div>
   <hr class="mt-0">
  <main>
     <!-- Passenger Details -->
    <h4 class="text-4">Subject Details</h4>
    <div class="table-responsive">
  
      <table class="table table-bordered text-2 table-sm " style="text-align:center!important;">
    <style>.table thead th {   vertical-align:middle!important;} .table-sm td, .table-sm th {padding: 0px 5px 0px px;}</style>
        <thead>
          <tr >
            <th rowspan="2">No.</th>
            <th rowspan="2">Class Code</th>
            <th rowspan="2">Subject Code</th>
      <th rowspan="2">Description</th>
      <th rowspan="2">Units</th>
        <th colspan="3">Schedule</th>
      </tr>
      <tr>
            
               <th>Day</th>
         <th>Time</th>
         <th>Room</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td>1</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Network Security</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>2</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Multicore and CPU Programming</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>3</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Mobile Media</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>4</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>History of Rizal</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td>5</td>
          <td>2ABSED-2</td>
          <td>GenEd30</td>
          <td>Programming 2</td>
          <td>3</td>
          <td>Mon</td>
          <td>8-11am</td>
          <td>Lab</td>
        </tr>
        <tr>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px; font-size: 13px;" class="text-sm-right" ><i> Total No. of Units</i></td>
          <td style="border: 0px; border-bottom: 1px solid black;">15</td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
        </tr>
     
        </tbody>
      </table>
      
    </div>
 <h4 class="text-4 mt-2">Partial Payment:</h4>
          <div class="table-responsive" style="width: 40%">
            <table class="table table-bordered text-2 table-sm">
        
       <thead>
  <tr>
    <th>Amount</th>
    <th>Date</th>
  </tr> 
        </thead>
        <tbody>
          <tr>
            <td>3,000</td>
            <td>March-1-2021</td>
          </tr>
        </tbody>
      </table>
    </div>
    
        <div class="row mt-1">
      
      <div class="col-sm-12">
        <div class="row">
        
         
          <div class="col-sm-6"> 
       <div class="row" style="margin-top: 50px;">
      <div class="col-sm-6 mb-3"> <strong>Assessted By: </strong> <span><br>RINA A. CALIZA<br><br>_____________<br>Assessment Incharge</span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Validated By:  </strong> <span><br>BERNADETH T. NACUA<br><br>_____________<br>Collage Registrar</span> </div>
    </div>
          </div>
      
        <div class="col-sm-6" style="text-align:right!important;">
          
           <div class="table-responsive">
            <table class="table text-1 table-sm">
        
        <tbody>
        <tr><td  style="border:0px solid red; font-size: 15px;"> <strong>Tuition Fee :</strong> 4,500</td></tr> 
      <tr><td style="border:0px solid red; font-size: 15px;"><strong>Miscellaneous Fees:</strong> 3,000</td></tr>    
      <tr><td style="border:0px solid red; font-size: 15px;"><strong>Total school Fees :</strong> 5,500</td>  </tr> 
       <tr><td style="border:0px solid red; font-size: 15px;"><strong>Student Signatures Over Printed Name</strong></p><p class="col-sm-15 text-sm-right" >Christoper M. Lague</td></tr>
    </tbody>
      </table>
    </div>
        
          </div>
          
        </div>
      </div>
    </div>
          
      
      
    <!-- Important Info -->
  </main>
  <!-- Footer -->
 </main>
  <!-- Footer -->
 
</div>
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
 
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none">Take   Print</a> </div>
   

    
  </footer>
</div>
<p class="text-center d-print-none"><a href="../new_student.php">&laquo; Back to Dashboard</a></p>


</body>
</html>