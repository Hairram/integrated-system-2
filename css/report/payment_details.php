<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reports - School Fees Payment System</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
<style>

 @media print {
  @page {
  size: landscape;
    margin: 0;
}
}     

.table td, .table th {
   border-top: 1px solid black;
   border-collapse: collapse; 
   text-align: left;
   border:1px solid black;
   vertical-align: middle;
   text-align:center;
}

</style>
</head>
<body style="background:white;  line-height: 12px;">

<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding:10px!important;border:0px solid black"> 
  <!-- Header -->
  <!-- Main Content -->
  <main>



    <div class="table-responsive">

  <table class="table  text-1 table-sm ">
  <tr >
      <td colspan="18">
        <br>
          
            <h5>PAYMENT DETAILS</h5>
           
  </td>
</tr>
 
  
    
    <tr>
      <th>NAME</th>
      <th>COURSE</th>
      <th>SEMESTER</th>
      <th>SCHOOL YEAR</th>
      <th>DATE</th>
      <th>OR NUMBER</th>   
      <th>AMOUNT</th>
  
      
    </tr>
        <tr>
      <td>PEREZ, SACHELLE D.</td>
      <td>BSBA</td>
      <td>1ST SEMESTER</td>
      <td>DALWEN</td>
      <td>2019-2020</td>
      <td>461161</td>
      <td>2,000.00</td>
    </tr>

     <tr>
      <td>VACALARES, CHARLOTE A.</td>
      <td>BEED</td>
      <td>2ND SEMESTER</td>
      <td>2019-2020</td>
      <td>1/28/2020</td>
      <td>462023</td>
      <td>1,000.00</td>
      
    </tr>
     <tr>
      <td>YU, CHRISTIAN</td>
      <td>BSBA</td>
      <td>2ND SEMESTER</td>
      <td>2019-2020</td>
      <td>1/27/2021</td>
      <td></td>
      <td>2,000.00</td>
    
    </tr>
     <tr>
      <td>ELLOVIDO, JANETH KYLDE</td>
      <td>BSED</td>
      <td>1ST SEMESTER</td>
      <td>2019-2020</td>
      <td>1/27/2020</td>
      <td>464435</td>
      <td>3,950.00</td>
      
    </tr>
     <tr>
      <td>ANGUNOR HONEYBELLE</td>
      <td>BSBA</td>
      <td>2ND SEMESTER</td>
      <td>2019-2020</td>
      <td>1/27/2020</td>
      <td>464425</td>
      <td>425.00</td>
      
    </tr>

     <tr>
      <td>BAHIAN, RENALDO</td>
      <td>BSBA</td>
      <td>2ND SEMESTER</td>
      <td>2019-2020</td>
      <td>1/28/2020</td>
      <td>580519</td>
      <td>3,900.00</td>
      
    </tr>
     <tr>
      <td>BALANE, SHELLANE</td>
      <td>BSBA</td>
      <td>2ND SEMESTER</td>
      <td>2019-2020 </td>
      <td>1/28/2020</td>
      <td>445878</td>
      <td>1,000.00</td>
      
    </tr>
     <tr>
      <td>MALEBORN, OPENA B.</td>
      <td>BSBA</td>
      <td>2ND SEMESTER</td>
      <td>2019-2020</td>
      <td>2/1/2021</td>
      <td>580545</td>
      <td>425.00</td>
      
    </tr>
     <tr>
      <td>DELITA, NERI</td>
      <td>BSED</td>
      <td>1ST SEMESTER</td>
      <td>2019-2020</td>
      <td>1/21/2021</td>
      <td>462045</td>
      <td>425.00</td>
      
    </tr>

     
   
  </table>
    </div>
 </main>
  <!-- Footer -->
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"> Take Print</a> </div>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../">&laquo; Back to Dashboard</a></p>


</body>
</html> 