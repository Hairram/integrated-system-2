
<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Student Fee Transaction History</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>

      <link href="css/style2.css" rel="stylesheet" />
<style>
<style>
 @media print {
  @page {
  size: 8.5in x 14in portrait;
    margin: 0;
}
}     
        </style>
</style>
</head>
<body style="background:white;">
<!-- Container -->

<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
  <!-- Header -->
 
  
  <!-- Main Content -->
  <main>
    <br/>
    <br/>
    <br/>
    <br/>
  <div class="row text-3">
      <div class="col-sm-6 mb-3"> <strong>Full Name:</strong> <span>Christoper M. Lague</span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Student ID: </strong> <span>2021-1-15152</span> </div>
    </div>
   
    <div class="row text-2">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span>First Semester</span>
          </div>
          <div class="col-sm-2"> <strong>School Year: </strong><span>2021-2022</span>
          </div>
          <div class="col-sm-3 text-sm-center"> <strong>Course & Year: </strong><span>BIST 4</span>
          </div>
          <div class="col-sm-2"> <strong>Gender: </strong><span>Male</span>
          </div>
          <div class="col-sm-2 text-sm-right"> <strong>Student Type: </strong><span>Old</span>
          </div>
        </div>
      </div>
    </div>
 
  
 <hr class="mt-4">
  <main>
     <!-- Passenger Details -->
    <h4 class="text-5 mt-5" style="color: red">Transaction History</h4>
    <div class="table-responsive">
  
      <table class="table table-bordered text-1 table-sm table-striped" style="text-align:center!important;" >
    <style>.table thead th {   vertical-align:middle!important;}</style>
        <thead>
          <tr >
            <th style="" rowspan="2">OR No.</th>
            <th rowspan="2">Amount Paid</th>
            <th rowspan="2">Date</th>
      <th rowspan="2">Remarks</th>
      </tr>
      
        </thead>
        <tbody>
        <tr>
          <td>12345</td>
          <td>3,000</td>
          <td>Feb-26-2021</td>
          <td>Not Paid</td>
        </tr>
          
        </tbody>
      </table>
    </div>

    
  
   
  
  
 
  </main>
  <!-- Footer -->
 </main>
  <!-- Footer -->
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"> Take Print</a> </div>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../">&laquo; Back to Dashboard</a></p>
<script>

  document.getElementById("breakfooter").style.pageBreakBefore = "avoid!important";

</script>
</body>
</html>