<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reports - School Fees Payment System</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
<style>

 @media print {
  @page {
  size:	landscape;
    margin: 0;
}
}			

.table td, .table th {
   border-top: 1px solid black;
   border-collapse: collapse; 
   text-align: left;
   border:1px solid black;
   vertical-align: middle;
   text-align:center;
}

</style>
</head>
<body style="background:white;  line-height: 12px;">

<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1400px!important;padding:0px!important;border:0px solid black"> 
  <!-- Header -->
  <!-- Main Content -->
  <main>



    <div class="table-responsive">

  <table class="table  text-1 table-sm ">
  <tr >
      <td colspan="18">
						<p>Republic of Philipines<br><i><b>OPOL COMMUNITY COLLEGE</b><br>OPOL  MISAMIS ORIENTAL 9016 </i></p>
						<h5>FREE HE CONSOLIDATED BILLING DETAILS</h5>
						<p>Free HE Billing Details Reference Number: <u>10-OCC-2019-2-1</u> Date:March 4,2020</p>
	</td>
</tr>
    <tr>
      <td style="text-align:left;" colspan="9">INSTRUCTIONS</td>
      <td style="text-align:left;" colspan="9"></td>
    </tr>
     <tr >
      <th style="text-align:left;" colspan="18">To: CHED - Central Office</th>
</tr>
 <tr >
      <th style="text-align:left;" colspan="18">Address: Higher Education Development Center Building, C.P Garcia Ave, UP Campus, Deliman, Quezon City, Metro Manila</th>
</tr>
 <tr >
      <th style="text-align:left;" colspan="18">Copy Furnished: Department of Budget and Management</th>
</tr>
 <tr >
      <th style="text-align:left;" colspan="18">Reconcilliation Details</th>
</tr>
 <tr>
      <th style="text-align:right;" colspan="6">Free TOSF Budget Ceiling</th>
      <th colspan="6"> </th>
      <td style="text-align:left;" colspan="6" rowspan="3">Per Billing Statement <br>
      Per Actually Enrolled</td>
    </tr>
     <tr>
      <th style="text-align:right;" colspan="6">TOSF Subsidy</th>
      <th style="text-align:right;"colspan="6"></th>
 
    </tr>
     <tr>
      <th style="text-align:right;"  colspan="6">Excess (Deficiency) in Free  HE Budget</th>
      <th colspan="6"> </th>

    </tr>
   <tr >
      <th style="text-align:left;"  colspan="18">Justification for (Excess) Deficiency of  Free HE  Budget </th>
  </tr>
  <tr >
      <th style="text-align:left;" colspan="1">PART I</th>
      <th style="text-align:left;" colspan="17">Tuition Fees</th>
  </tr>
  <tr>

      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:center;" colspan="3">Student's name</th>
      <th style="text-align:center;" colspan="4">Student Profile</th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:center;" colspan="2">Contact Information</th>
      <th style="text-align:center;" colspan="2">Tuition Subsidy Information</th>
      <th style="text-align:center;" colspan="2">StuFAP Information</th>
      <th style="text-align:left;" colspan="1"></th>
  </tr>
    <tr>
      <th>5-digit Control Number</th>
      <th>Student Number</th>
      <th>Learner's Reference</th>
      <th>Last Name</th>
      <th>Given Name</th>
      <th>Middle Initial</th>   
       <th>Sex at Birth (M/F)</th>
      <th>Birthdate (mm/dd/yyyy)</th>
      <th>Degree Program</th>
      <th>Year Level</th>
      <th>Zip Code</th>
      <th>Email Address</th>  
      <th>Phone Number</th>
      <th>Academic Units Enrolled (Credit and Non-credit courses</th>
      <th>Tuition Fee Based on enrolled academic units (credit and non-credit courses)</th>
      <th>Type of StuFAP availed apart from free Tuition Fee (if any)</th>
      <th>Amount of stuFAP availment</th>
      <th>Remarks</th>  
    </tr>
        <tr>
      <td>0001</td>
      <td>2019-1-01670</td>
      <td></td>
      <td>ALEXCANDER</td>
      <td>JOHN</td>
      <td>A</td>
      <td>M</td>
      <td>MARCH 16, 2000</td>
      <td>BEED</td>
      <td>1A</td>
      <td>9000</td>
      <td></td>
      <td>09356401264</td>
      <td>26</td>
      <td>3,900.00</td>
      <td></td>
      <td></td>
      <td></td>      
    </tr>
     <tr>
      <td>0002</td>
      <td>2019-1-01617</td>
      <td></td>
      <td>ABERAYA</td>
      <td>NORHATA</td>
      <td>A</td>
      <td>M</td>
      <td>FEBRUARY 7,1999</td>
      <td>BEED</td>
      <td>1A</td>
      <td>9016</td>
      <td>arirayahata@gmail.com</td>
      <td>09069442504</td>
      <td>26</td>
      <td>3,900.00</td>
      <td></td>
      <td></td>
      <td></td>      
    </tr>
     <tr>
      <td>0003</td>
      <td>2019-1-02443</td>
      <td></td>
      <td>BACANGGOY</td>
      <td>MELALYN</td>
      <td>A</td>
      <td>F</td>
      <td>JULY 22, 1990</td>
      <td>BEED</td>
      <td>1A</td>
      <td>9016</td>
      <td>gjdagondon@gmail.com</td>
      <td>09177723542</td>
      <td>26</td>
      <td>3,900.00</td>
      <td></td>
      <td></td>
      <td></td>      
    </tr>
     <tr>
      <td>0004</td>
      <td>2019-1-02447</td>
      <td></td>
      <td>BACULIO</td>
      <td>MAE JOY</td>
      <td>S</td>
      <td>F</td>
      <td>DECEMBER 9, 1990</td>
      <td>BEED</td>
      <td>1A</td>
      <td>9016</td>
      <td>maejoy_baculio@yahoo.com</td>
      <td>09161462836</td>
      <td>26</td>
      <td>3,900.00</td>
      <td></td>
      <td></td>
      <td></td>      
    </tr>
    
     <tr>
      <th style="text-align:left;" colspan="2">Page Total</th>
     <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
    </tr>
    <tr>
      <th style="text-align:left;"  colspan="2">Page Accumulated Total</th>

      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
      <th style="text-align:left;" colspan="1"></th>
    </tr>
  </table>
    </div>
 </main>
  <!-- Footer -->
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"> Take Print</a> </div>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../">&laquo; Back to Dashboard</a></p>


</body>
</html>