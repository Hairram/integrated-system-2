<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reports - School Fees Payment System</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
<style>
* {
font-style: 14px;
}
 @media print {
  @page {
  size:	landscape;
    margin: 0;
}
}			

.table td, .table th {
   border-top: 1px solid black;
   border-collapse: collapse; 
   text-align: left;
   border:1px solid black;
   vertical-align: middle;
   text-align:center;
}

</style>
</head>
<body style="background:white;  line-height: 12px;">

<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1500px!important;padding:0px!important;border:0px solid black"> 
  <!-- Header -->
  <!-- Main Content -->
  <main>



    <div class="table-responsive">

  <table class="table  text-1 table-sm ">
  <tr >
      <td colspan="20">
						<p style="font-size: 15px;">Republic of Philipines<br><i><b>OPOL COMMUNITY COLLEGE</b><br>POBLACION OPOL MISAMIS ORIENTAL 9016 </i></p>
						<h5>FREE HE CONSOLIDATED BILLING DETAILS</h5>
            <br>
            <br>
            <br>
	</td>
</tr>
    <tr>
      <td style="text-align:left;" colspan="20"><b>TUITION AND OTHER SCHOOL FEES (Based on Section 7. Rule II of the IRR of RA 1093</b></td>
    </tr>
   

   
    <tr>
      <th>Sequence Number</th>
      <th>Student Number</th>
      <th>Learner's Reference Number</th>
      <th>Last Name</th>
      <th>Given Name</th>
      <th>Middle Initial</th>   
       <th>Degree Program</th>
      <th>Year Level</th>
      <th>Sex at Birth</th>
      <th>Email Address</th>
      <th>Phone Number</th>
      <th>Laboratory Units/Subjects</th>  
       <th>Computer Lab Units/Subjects</th>
      <th>Academic Units Enrolled (credit and non-credit courses)</th>
      <th>Academic Units of NSTP Enrolled (credit and non-credit courses)</th>
      <th>Tuition Fee based on enrolled academic units (credit and non-credit courses)</th>
      <th>NSTP fee based on enrolled academic units( credit and non-credit courses)</th>
      <th>Athletic fees</th>
      <th>Computer Fees</th>
      <th>Cultural Fees</th>
    </tr>
        <tr>
      <td>0001</td>
      <td>2020-1-02174</td>
      <td></td>
      <td>ALEXCANDER</td>
      <td>JOHN</td>
      <td>A</td>
      <td>BEED</td>
      <td>1A</td>
      <td>M</td>
      <td>John123@gmail.com</td>
      <td>09356401264</td>
      <td></td>
      <td></td>
      <td>35</td>
      <td>35</td>
      <td>5,250</td>
      <td>255</td>
      <td>100</td>
       <td>100</td>
      <td>100</td>           
    </tr>
    <tr>
      <td>0002</td>
      <td>2020-1-01122</td>
      <td></td>
      <td>ABERAYA</td>
      <td>NORHATA</td>
      <td>A</td>
      <td>BEED</td>
      <td>1A</td>
      <td>F</td>
      <td>Aberaya@gmail.com</td>
      <td>09656111264</td>
      <td></td>
      <td></td>
      <td>35</td>
      <td>35</td>
      <td>5,250</td>
      <td>255</td>
      <td>100</td>
       <td>100</td>
      <td>100</td>           
    </tr>
    <tr>
      <td>0003</td>
      <td>2020-1-04814</td>
      <td></td>
      <td>BACANGGOY</td>
      <td>MELALYN</td>
      <td>M</td>
      <td>BEED</td>
      <td>1A</td>
      <td>F</td>
      <td>goy@gmail.com</td>
      <td>09566407231</td>
      <td></td>
      <td></td>
      <td>35</td>
      <td>35</td>
      <td>5,250</td>
      <td>255</td>
      <td>100</td>
       <td>100</td>
      <td>100</td>           
    </tr>
     
  </table>
    </div>
 </main>
  <!-- Footer -->
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"> Take Print</a> </div>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../">&laquo; Back to Dashboard</a></p>


</body>
</html>