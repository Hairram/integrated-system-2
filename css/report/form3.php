<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reports - School Fees Payment System</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
<style>

 @media print {
  @page {
  size: landscape;
    margin: 0;
}
}     

.table td, .table th {
   border-top: 1px solid black;
   border-collapse: collapse; 
   text-align: left;
   border:1px solid black;
   vertical-align: middle;
   text-align:center;
}

</style>
</head>
<body style="background:white;  line-height: 12px;">

<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding:10px!important;border:0px solid black"> 
  <!-- Header -->
  <!-- Main Content -->
  <main>



    <div class="table-responsive">

  <table class="table  text-1 table-sm ">
  <tr >
      <td colspan="18">
        <br>
            <p class="text-2">Republic of Philipines<br><i><b>(Name of State / Local University of College)</b><br>(Address of State / Local University of College)</i></p>
            <h5>FREE HE CONSOLIDATED BILLING DETAILS</h5>
            <p style="margin: 0px">Free HE Billing Details Reference Number: <u>10-OCC-2019-2-1</u></p>
             <p style=" float: right; padding-right: 432px;">Date: <u>March 4,2020</u></p>
  </td>
</tr>
 
  
 
    <tr>
      <td style="text-align:left;" colspan="13"><b>ADMISSION FEES (Based on Section 7, Rule II of the IRR of RA 10931)</b></td>
      
    </tr>
    
    <tr>
      <th>Sequence Number</th>
      <th></th>
      <th>Last Name</th>
      <th>Given Name</th>
      <th>Middle Initial</th>
      <th>Sex at Birth (M/F)</th>   
       <th>Brithdate (mm/dd/yyyy)</th>
      <th>Degree Program</th>
      <th>Year Level</th>
      <th>Email Address</th>
      <th>Phone Number</th>
      <th>Entrance Admission Fees</th>
      <th>Remarks (Passed or Failed)</th>
      
    </tr>
        <tr>
      <td>0001</td>
      <td>2020-1-02174</td>
      <td>ABA</td>
      <td>DALWEN</td>
      <td>Q</td>
      <td>M</td>
      <td>4/12/1984</td>
      <td>BEED</td>
      <td>1A</td>
      <td>Complexion79@gmail.com</td>
      <td>09656614531</td>
      <td>250</td>
      <td>Passed</td>
    </tr>

     <tr>
      <td>0002</td>
      <td>2020-1-01731</td>
      <td>AVANCENA</td>
      <td>EMILIO JR</td>
      <td>R</td>
      <td>M</td>
      <td>5/16/1997</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.avancena@gmail.com</td>
      <td>09065727214</td>
      <td>250</td>
      <td>Passed</td>
    </tr>
     <tr>
      <td>0003</td>
      <td>2020-1-05114</td>
      <td>BAHIAN</td>
      <td>MERRY JOAN</td>
      <td>C</td>
      <td>F</td>
      <td>3/9/2002</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.Bahian.@gmail.com</td>
      <td>09657614531</td>
      <td>250</td>
      <td>Passed</td>
    </tr>
     <tr>
      <td>0004</td>
      <td>2020-1-07234</td>
      <td>BALDON</td>
      <td>ASI</td>
      <td>V</td>
      <td>M</td>
      <td>10/3/2000</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.Baldon.@gmail.com</td>
      <td>09656614531</td>
      <td>250</td>
      <td>Passed</td>
    </tr>
     <tr>
      <td>0005</td>
      <td>2020-1-08124</td>
      <td>BAULO</td>
      <td>CHARMANE JOY</td>
      <td>D</td>
      <td>F</td>
      <td>4/10/2003</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.Baulo.@gmail.com</td>
      <td>09657714545</td>
      <td>250</td>
      <td>Passed</td>
    </tr>

     <tr>
      <td>0006</td>
      <td>2020-1-02774</td>
      <td>BETONIO</td>
      <td>JONAVIC</td>
      <td>M</td>
      <td>F</td>
      <td>10/3/2001</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.Betonio.@gmail.com</td>
      <td>09653114732</td>
      <td>250</td>
      <td>Passed</td>
    </tr>
     <tr>
      <td>0007</td>
      <td>2020-1-09184</td>
      <td>BUNA</td>
      <td>ANGELICA</td>
      <td>D</td>
      <td>F</td>
      <td>10/5/2001</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.Buna@gmail.com</td>
      <td>09566314891</td>
      <td>250</td>
      <td>Passed</td>
    </tr>
     <tr>
      <td>0008</td>
      <td>2020-1-06124</td>
      <td>BUNA</td>
      <td>JAY ANN</td>
      <td>P</td>
      <td>F</td>
      <td>4/15/1995</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.Buna123@gmail.com</td>
      <td>09657714132</td>
      <td>250</td>
      <td>Passed</td>
    </tr>
     <tr>
      <td>0009</td>
      <td>2020-1-09823</td>
      <td>CALDERON</td>
      <td>CHRISTINE</td>
      <td>R</td>
      <td>F</td>
      <td>5/14/2001</td>
      <td>BEED</td>
      <td>1A</td>
      <td>OCC.Calderon.@gmail.com</td>
      <td>09656614531</td>
      <td>250</td>
      <td>Passed</td>
    </tr>

     
   
  </table>
    </div>
 </main>
  <!-- Footer -->
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"> Take Print</a> </div>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../">&laquo; Back to Dashboard</a></p>


</body>
</html>