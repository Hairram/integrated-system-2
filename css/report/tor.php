
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Certificate Of Enrollment</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>

      <link href="css/style2.css" rel="stylesheet" />
<style>
  * {
    font-family: "Times New Roman", Times, serif;
  }
  h6  {
font-family: "Times New Roman", Times, serif;
font-size: 18px;
  }
  h4 {
    font-family: "Times New Roman", Times, serif;
    font-size: 30px;
  }
  th {
    font-size: 14px;
  }
  td {
    font-size: 14px;
    text-align: left;
  }
<style>
 @media print {
  @page {
  size: 8.5in x 14in portrait;
    margin: 0;
    padding: 0
}
}			
        </style>
</style>
</head>
<body style="background:white;">
<!-- Container -->
<div class="container-fluid invoice-container" style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
   <br/>
   <br/>
  <!-- Header -->
  <header>
    <div class="row align-items-center">
	<div class="col-sm-0 text-center text-sm-right mb-4 mb-sm-1"> <h3 class="mb-0"> <img id="logo" src="occ.png" title="Koice" alt="Koice" />
	</div>

	
      <div class="col-sm-11 text-center text-sm-center mb-3 mb-sm-0"> <h4 class="mb-0">Opol Community College</h4>
        <h6 class="mb-4">Opol, Misamis Oriental</h6>
        <h6 class="mb-1">Bachelor of Scice in Information Technology</h6>
        <h6 class="mb-1">Revised for SY 2019-2020</h6>
        <h6 class="mb-5">Effective SY 2020-2021</h6>
      </div>
    </div>
  </h3>
  </header>
  

    
   
 
  <main>
    
    <div class="table-responsive">
      <table class="table table-bordered text-1 table-sm table-striped" style="text-align:center!important;" >
	  <style>.table thead th {   vertical-align:middle!important; border: 1px solid #585858;}  .table-sm td {border: 1px solid #585858;} .table-sm th {padding: 10px 5px 10px 10px; border: 1px solid #585858;}</style>

    <thead>
     
        <tr>
          <th colspan="7" style="background: #e6e6e6; color: #404040; padding: 6px;">FIRST YEAR</th>
        </tr>
        <tr>
    <th colspan="7" style="background: #383838; color: #fff; padding: 6px;">FIRST SEMESTER</th>
  </tr>
   <tr>
      <th>Subject Code</th>
			<th>Descriptive Title</th>
			<th>Credit Units</th>
		    <th>Lec Hours</th>
        <th>Lab Hours</th>
        <th>Hrs/Week</th>
        <th>PRE-REQUISITES</th>
        
			</tr>
    </thead>
        <tbody>
         <tr>
          <td>11111</td>
          <td>asdadasdsadasaddasdsadsadasdasdasdsadsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        
          <tr>
          <td colspan="2" style="text-align: right"><i>Total Hrs/Units</i></td>
          <td>26</td>
          <td>26</td>
          <td>4</td>
          <td>30</td>
          <td></td>
        </tr>
        <br>

        </tbody>
      </table>
    </div>
</main>
<main>
    <div class="table-responsive">
      <table class="table table-bordered text-1 table-sm table-striped" style="text-align:center!important;" >
    <style>.table thead th {   vertical-align:middle!important; border: 1px solid #585858;}  .table-sm td {border: 1px solid #585858;} .table-sm th {padding: 10px 5px 10px 10px; border: 1px solid #585858;}</style>

    <thead>
     
        <tr>
          <th colspan="7" style="background: #e6e6e6; color: #404040; padding: 6px;">FIRST YEAR</th>
        </tr>
        <tr>
    <th colspan="7" style="background: #383838; color: #fff; padding: 6px;">FIRST SEMESTER</th>
  </tr>
   <tr>
      <th>Subject Code</th>
      <th>Descriptive Title</th>
      <th>Credit Units</th>
        <th>Lec Hours</th>
        <th>Lab Hours</th>
        <th>Hrs/Week</th>
        <th>PRE-REQUISITES</th>
        
      </tr>
    </thead>
        <tbody>
         <tr>
          <td>11111</td>
          <td>asdadasdsadasaddasdsadsadasdasdasdsadsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        <tr>
          <td>11111</td>
          <td>asdsad</td>
          <td>3</td>
          <td>3</td>
          <td>0</td>
          <td>3</td>
          <td>None</td>
        </tr>
        
          <tr>
          <td colspan="2" style="text-align: right"><i>Total Hrs/Units</i></td>
          <td>26</td>
          <td>26</td>
          <td>4</td>
          <td>30</td>
          <td></td>
        </tr>
        <br>

        </tbody>
      </table>
    </div>
</main>
  
</body>
</html>