<?php
include ('../includes/dbconnect.php');

$student_id=$_GET['student_id'];

$sql = "SELECT * FROM tbl_student WHERE student_id='$student_id'";
$result = mysqli_query($conn, $sql);



if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $row = mysqli_fetch_assoc($result);
    $student_id=$row["student_id"];
    $firstname=$row["firstname"];
    $middlename=$row["middlename"];
    $lastname=$row["lastname"];
    
}
?>
<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">



<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" type="text/css" href="../css/approve_student.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="../css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="../css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="../css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="../css/toastr/toastr.min.css" media="screen" >
        <link rel="stylesheet" href="../css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="../css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="../css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="../css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>

     
<style>
<style>
 @media print {
  @page {
  size: 8.5in x 14in portrait;
    margin: 0;
}
}			
        </style>
</style>
</head>
<body style="background:white;">
<!-- Container -->
<div class="container-fluid invoice-container" style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
  <!-- Header -->
  <header>
    <div class="row align-items-center">
	<div class="col-sm-1 text-center text-sm-left mb-2 mb-sm-0"> <h3 class="mb-0"> <img id="logo" src="occ.png" title="Koice" alt="Koice" />
	</div>
	
      <div class="col-sm-6 text-center text-sm-left mb-3 mb-sm-0"> <h4 class="mb-0 text-5">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental
</p></div>
      <div class="col-sm-5 text-center text-sm-right">
        <h6 class="mb-0 text-4">ENROLLMENT FORM</h6>
        <p class="mb-0 text-3">Date Enrolled: 02-22-2021</p>
      </div>
    </div>
  </header>
  
  <!-- Main Content -->
  <main>
    <div class="row text-3">
      <div class="col-sm-6 mb-3"> <strong>Full Name: </strong> <span style="font-size: 15px"><?php echo $firstname; ?> <?php echo $middlename; ?> <?php echo $lastname; ?></span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Student ID: </strong> <span><?php echo $student_id; ?></span> </div>
    </div>
   
    <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span>First Semester</span>
          </div>
          <div class="col-sm-2"> <strong>School Year: </strong><span>2021-2022</span>
          </div>
          <div class="col-sm-3 text-sm-center"> <strong>Course & Year: </strong><span>BIST 1</span>
          </div>
          <div class="col-sm-2"> <strong>Gender: </strong><span>Male</span>
          </div>
          <div class="col-sm-2 text-sm-right"> <strong>Student Type: </strong><span>Old</span>
          </div>
        </div>
      </div>
    </div> 
	 <hr class="mt-0">
  <main>
     <!-- Passenger Details -->
    <h4 class="text-4">Student Subject Details</h4>
    <div class="table-responsive">
	
      <table class="table table-bordered text-2 table-sm table-striped" style="text-align:center!important;">
	  <style>.table thead th {   vertical-align:middle!important;} .table-sm td, .table-sm th {padding: 0px 5px 0px px;}</style>
        <thead>
          <tr >
            <th rowspan="2">No.</th>
            <th rowspan="2">Class Code</th>
            <th rowspan="2">Subject Code</th>
			<th rowspan="2">Description</th>
			<th rowspan="2">Units</th>
		    <th colspan="3">Schedule</th>
			</tr>
			<tr>
            
               <th>Day</th>
			   <th>Time</th>
			   <th>Room</th>
          </tr>
        </thead>
        <tbody>
       	<tr>
       		<td>1</td>
       		<td>2ABSED-2</td>
       		<td>GenEd30</td>
       		<td>Network Security</td>
       		<td>3</td>
       		<td>Mon</td>
       		<td>8-11am</td>
       		<td>Lab</td>
       	</tr>
       	<tr>
       		<td>2</td>
       		<td>2ABSED-2</td>
       		<td>GenEd30</td>
       		<td>Multicore and CPU Programming</td>
       		<td>3</td>
       		<td>Mon</td>
       		<td>8-11am</td>
       		<td>Lab</td>
       	</tr>
       	<tr>
       		<td>3</td>
       		<td>2ABSED-2</td>
       		<td>GenEd30</td>
       		<td>Mobile Media</td>
       		<td>3</td>
       		<td>Mon</td>
       		<td>8-11am</td>
       		<td>Lab</td>
       	</tr>
       	<tr>
       		<td>4</td>
       		<td>2ABSED-2</td>
       		<td>GenEd30</td>
       		<td>History of Rizal</td>
       		<td>3</td>
       		<td>Mon</td>
       		<td>8-11am</td>
       		<td>Lab</td>
       	</tr>
       	<tr>
       		<td>5</td>
       		<td>2ABSED-2</td>
       		<td>GenEd30</td>
       		<td>Programming 2</td>
       		<td>3</td>
       		<td>Mon</td>
       		<td>8-11am</td>
       		<td>Lab</td>
       	</tr>
       	<tr>
       		<td style="border: 0px;"></td>
       		<td style="border: 0px;"></td>
       		<td style="border: 0px;"></td>
       		<td style="border: 0px; font-size: 13px;" class="text-sm-right" ><i> Total No. of Units</i></td>
       		<td style="border: 0px; border-bottom: 1px solid black;">15</td>
       		<td style="border: 0px;"></td>
       		<td style="border: 0px;"></td>
       		<td style="border: 0px;"></td>
       	</tr>
		 
        </tbody>
      </table>
      
    </div>

    
	
	 <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
        
         
          <div class="col-sm-5"> 
		  <h4 class="text-4 mt-4">Miscellaneous Fees Breakdown:</h4>
				  <div class="table-responsive">
						<table class="table table-bordered text-2 table-sm">
        
        <tbody>
	<tr>
		<td>Laboratory Fee</td>
		<td>100</td>
		<td>Library Fee</td>
		<td>150</td>
	</tr>

	<tr>
		<td>Athletic Fee</td>
		<td>150</td>
		<td>Dental/Clinic Fee</td>
		<td>150</td>
	</tr>
		   
		
        </tbody>
      </table>
    </div>
          </div>
		  
		    <div class="col-sm-7" style="text-align:right!important"><br><br>
					
					 <div class="table-responsive">
						<table>
        
        <tbody>
          <tr><td  style="border:0px; font-size: 15px;"> <strong>Tuition Fee :</strong> 4,500</td></tr> 
		  <tr><td style="border:0px; font-size: 15px;"><strong>Miscellaneous Fees:</strong> 3,000</td></tr>    
		  <tr><td style="border:0px; font-size: 15px;"><strong>Total school Fees :</strong> 5,500</td>	</tr> 
		  <tr><td style="border:0px; font-size: 15px;"><p><strong>Student Signatures Over Printed Name</strong></p><p class="col-sm-15 text-sm-right" >Christoper M. Lague<br>_________________</p></td></tr> 
		</tbody>
      </table>
    				</div>
        
          </div>
          
        </div>
      </div>
    </div>
  
  
 
      
    <!-- Important Info -->
    <div class="row" style="border-bottom:1px dashed black">
      
    </div>
  </main>
  <!-- Footer -->
 </main>
  <!-- Footer -->
 
</div>
<!-- Back to My Account Link -->
 <footer id="breakfooter" > </footer>
<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
 
  <footer class="text-center">
     <button  class="btn btn-primary btn-xs" id="button"> Remove Fees </button>
    <button  class="btn btn-primary btn-xs" id="button"> Add Fees </button>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../paying_student.php">&laquo; Back to Dashboard</a></p>




<div class="bg-modal">
<div class="modal-content">
		<div class="header" style="padding-top: 10px;"><h5>Select Criteria</h5>
    <div class="close"><p style="font-size: 27px">+</p>

    </div>   <hr></div>

      <div class="panel panel-default" style="margin-top: 8px">
                            <div class="table-sorting table-responsive" id="subjectresult">
                                <table class="table table-striped table-bordered table-hover" id="tSortable22">
                                    <thead style="background-color: lightgrey">
                                        <tr>
                                            <th>Type of Fees</th>
                                            <th>Select</th>
                                            
                                        </tr>
                                    
                                    </thead>
                                    			<tbody>
                               <tr>
                                <td>Athletic Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                               </tr>

                                 <tr>
                                 <td>Cultural Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Library Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Dental/Clinic Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Guidance Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Computer Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Handbook</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Registration Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>School ID</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Student Insurance</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>

                                 <tr>
                                 <td>Entrance Exam Fee</td>
                                <td style="width: 2px; text-align: center;"><input type="checkbox"></td>
                                 </tr>
                                 

							</tbody>
                                </table>

                            </div>

                    </div>


  <div class="button">
    <button type="submit" name="save" class="btn btn-primary">Add</button>
  </div>
 </div>
</div>
  <!-- <div class="button" style="top: 10px; padding-left: 50px">
    <button type="submit" name="save" class="btn btn-primary">Add</button>
  </div>
   -->
</div>
</div>
                          
      <script>
        document.getElementById('button').addEventListener('click', function() {
  document.querySelector('.bg-modal').style.display = 'flex';
});

         document.querySelector('.close').addEventListener('click', function() {
          document.querySelector('.bg-modal').style.display = 'none';
         });
      </script>
<script>

  document.getElementById("breakfooter").style.pageBreakBefore = "avoid!important";
</script>

<script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="../js/jquery-ui/jquery-ui.min.js"></script>
        <script src="../js/bootstrap/bootstrap.min.js"></script>
        <script src="../js/pace/pace.min.js"></script>
        <script src="../js/lobipanel/lobipanel.min.js"></script>
        <script src="../js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="../js/prism/prism.js"></script>
        <script src="../js/waypoint/waypoints.min.js"></script>
        <script src="../js/counterUp/jquery.counterup.min.js"></script>
        <script src="../js/amcharts/amcharts.js"></script>
        <script src="../js/amcharts/serial.js"></script>
        <script src="../js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="../js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="../js/amcharts/themes/light.js"></script>
        <script src="../js/toastr/toastr.min.js"></script>
        <script src="../js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="../js/main.js"></script>
        <script src="../js/production-chart.js"></script>
        <script src="../js/traffic-chart.js"></script>
        <script src="../js/task-list.js"></script>

</body>
</html>