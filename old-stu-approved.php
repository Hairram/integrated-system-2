<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');

$reg_id=$_GET['reg_id'];


$sql = "SELECT * FROM tbl_student WHERE student_id ='$reg_id'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $row 			=	mysqli_fetch_assoc($result);
    $reg_id			=	$row["student_id"];
    $lastname		=	$row["lastname"];
    $firstname		=	$row["firstname"];
    $middlename		=	$row["middlename"];
    $address		=	$row["address"];
    $contact_num	=	$row["contact_number"];
    $exam		    =	$row["entrance_exam_result"];
    $isApproved	    =	$row["isApproved"];
    $diploma	    =	'images/students/'.$row["form_137"];
    $good_moral	    =   'images/students/'.$row["good_moral"];
    $police_cl	    =	'images/students/'.$row["police_clearance"];
    $exam_result	=	'images/students/'.$row["exam_result"];
    $picture		=	'images/students/'.$row["picture"];
	$semester	    =	$row["semester"];
    $school_year    =	$row["school_year"];
    $year_level	    =	$row["year_level"];
    $course_id	    =	$row["course_id"];
}



if(isset($_POST['approve']))
{
$remarks 		= mysqli_real_escape_string($conn,$_POST['remarks']);
$reg_id 		= mysqli_real_escape_string($conn,$_POST['studentid']);
$sql 			= $conn->query("UPDATE tbl_student set isApproved =1 , remarks ='$remarks'  where student_id='$reg_id'") ;
echo '<script type="text/javascript">window.location="evaluate-new-stu-ph.php?updated";</script>';
}


if(isset($_POST['add-subject']))
{
$subject 		= $_POST['subject'];
$course_id 		= $_POST['course_id'];
$semester 		= $_POST['semester'];
$year_level 	= $_POST['year_level'];
 $check = "SELECT a.* FROM tbl_grades a
LEFT JOIN tbl_classes b on a.class_id = b.class_id
WHERE a.student_id ='$reg_id' and a.year_level='$year_level' and a.semester='$semester'
and b.subject_id ='$subject'";
$result = mysqli_query($conn, $check);
$rowcount=mysqli_num_rows($result);
if($rowcount ==1){
echo '<script type="text/javascript">alert("Subject already added!");window.location="old-stud-details.php?reg_id='.$reg_id.'";</script>';
} else {
$subject 		= $_POST['subject'];
$course_id 		= $_POST['course_id'];
$semester 		= $_POST['semester'];
$year_level 	= $_POST['year_level'];
 $check  = "SELECT a.* FROM tbl_classes a WHERE  a.year_level='$year_level' and a.course_id='$course_id' and a.subject_id ='$subject'";
$result = mysqli_query($conn, $check);
$row 	= mysqli_fetch_assoc($result);
$class_id = $row['class_id'];
$insert = "insert into tbl_grades (student_id , year_level ,semester ,school_year,class_id) 
		   VALUES ('$reg_id' , '$year_level' ,'$semester' , '$school_year' ,'$class_id')";
mysqli_query($conn, $insert);
echo '<script type="text/javascript">alert("Subject Added!");window.location="old-stu-approved.php?reg_id='.$reg_id.'";</script>';
}
}
if(isset($_POST['remove-subject']))
{
$id 		= $_POST['id'];

$delete = "DELETE from tbl_grades WHERE id ='$id'";
$result = mysqli_query($conn, $delete);

echo '<script type="text/javascript">alert("Subject Deleted!");window.location="old-stu-approved.php?reg_id='.$reg_id.'";</script>';
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Manage Departments</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="icon" href="images/favicon.png" type="image/x-icon"> 
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
					<?php include('includes/leftbar-ph.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>View Information(New Students)</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li class="active"> Evaluate Student</li>
                                         <li class="active"> New Student Evaluation</li>
                                         <li class="active"> View Information</li>

                                        
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             

                                <div class="row">

								<div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5><b>Student Information</b></h5>
                                                    <div class="col-sm-offset-10 col-sm-3">
                                                
                                            </div>

                                                </div>
                                            </div>
                                            

                                       
                                            <div class="panel-body p-20">

                                                <form action="new-stud-details.php" method="post" id="signupForm1">

												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Pre-reg. ID</label>
														<div class="col-sm-4">
															<input type="text" name="studentid" class="form-control" id="lastname" value="<?php echo $reg_id; ?>" readonly required="required" autocomplete="off">
														</div>
												</div>


												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Last Name</label>
														<div class="col-sm-4">
															<input type="text" name="lastname" class="form-control" id="lastname" readonly value="<?php echo $lastname; ?>" required="required" autocomplete="off">
														</div>
												</div>

												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">First Name</label>
														<div class="col-sm-4">
															<input type="text" name="firstname" class="form-control" id="firstname"  readonly value="<?php echo $firstname; ?>" required="required" autocomplete="off">
														</div>
												</div>

												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Middle Name</label>
														<div class="col-sm-4">
															<input type="text" name="middlename" class="form-control" id="middlename"  readonly value="<?php echo $middlename; ?>" required="required" autocomplete="off">
														</div>
												</div>


												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Present Address</label>
														<div class="col-sm-4">
															<input type="text" name="address" class="form-control" id="address" required="required" readonly value="<?php echo $address; ?>" placeholder="Zone/Barangay/Municipality/Province" autocomplete="off">
														</div>
												</div>


												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Contact Number</label>
														<div class="col-sm-4">
															<input type="text" name="contact_num" class="form-control" id="contact_num" required="required" readonly value="<?php echo $contact_num; ?>" maxlength="11" placeholder="Mobile Number"  autocomplete="off">
														</div>
												</div>



												<div class="row">
												
												<div class="form-group">
													<label for="date" class="col-sm-2 control-label">SHS Diploma /Form 137/ ALS</label>
													<div class="col-sm-4">
														<a href="<?php echo $diploma;?>" target="_blank">Click to View SHS Diploma /Form 137/ ALS </a>
													</div>
												</div>


												<div class="form-group">
													<label for="date" class="col-sm-2 control-label">SHS Good Moral</label>
														<div class="col-sm-4">
															<a href="<?php echo $good_moral;?>" target="_blank">Click to View SHS Good Moral </a>
														</div>
												</div>
												
												</div>
												<div class="row">
												<div class="form-group">
													<label for="date" class="col-sm-2 control-label">Police Clearance</label>
														<div class="col-sm-4">
															<a href="<?php echo $police_cl;?>" target="_blank">Click to View Police Clearance </a>
														 </div>
												</div> 


												<div class="form-group">
													<label for="date" class="col-sm-2 control-label">Entrance Exam Result</label>
														<div class="col-sm-4">
															<a href="<?php echo $exam_result;?>" target="_blank">Click to View Entrance Exam Result </a>
														</div>
												</div>
												
												</div>
												<div class="row">
												<div class="form-group">
												<label for="default" class="col-sm-2 control-label">Exam Score</label>
												<div class="col-sm-4">
												<input type="text" name="contact_num" class="form-control" id="contact_num" required="required" readonly="" maxlength="11" placeholder="" value="<?php echo $exam; ?>"   autocomplete="off">
												</div>
												</div>

												<div class="form-group">
													<label for="date" class="col-sm-2 control-label">2x2 Picture</label>
														<div class="col-sm-4">
															<a href="<?php echo $picture;?>" target="_blank">Click to View 2x2 Picture</a>
														</div>
												</div>
												</div>
												<?php if($isApproved == 0){ ?>
												<div class="row">
												<!--<div class="form-group">
												<label for="default" class="col-sm-2 control-label">Pay Status</label>
												<div class="col-sm-4">
												<select name="paystatus" class="form-control" id="contact_num"  >
													<option value=""> Select Pay Status </option>
													<option value="1"> Paying </option>
													<option value="2"> Non-Paying </option>
												</select>
												</div>
												</div>-->
												<div class="form-group">
												<label for="default" class="col-sm-2 control-label">Remarks</label>
												<div class="col-sm-4">
												<input type="message" name="remarks" class="form-control" id="contact_num"    placeholder="Remarks">
												</div>
												</div>
												</div>


      
												<div class="form-group">
												<br>
												<div class="col-sm-offset-2 col-sm-2"><a href="new-stud-assign-sub.php">
													<input type="hidden" name="action" value="<?php echo $action;?>">
													<button type="approve" name="approve" class="btn btn-primary" >Approve</button>
												</a>

												</div>
												<?php  } ?>
													<!-- /.content-container -->
												</div>
												</div>
            <!-- /.content-wrapper -->
			</form>
        </div>
        <!-- /.main-wrapper -->
</div>
								<div class="col-md-12">
									<div class="panel panel-default">
								  <div class="panel-heading"><h3 class="panel-title"><strong>Subjects </strong></h3></div>
								  <div class="panel-body">
								   <table class="table table-striped table-bordered">
									<thead>
									<a href="#" class="btn btn-primary btn-xs pull-right"  data-toggle="modal" data-target="#squarespaceModal"><b>+</b> Add Subject</a>
									<br><br>
										<tr>
											<th>Subject Code</th>
											<th>Descriptive Title</th>
											<th>Units</th>
											<th class="text-center"></th>
										</tr>
									</thead>
									<?php 
											// CHECK FAIL SUBJECTS //
												  if($semester == 'Second'){
														$ss = 'First';
													}
													if($semester == 'First'){
														$ss = 'Second';
													}
													$getpre  = "SELECT a.* , b.* , c.* , d.* FROM tbl_grades a
																		LEFT JOIN tbl_student b on a.student_id = b.student_id
																		LEFT JOIN tbl_classes c on a.class_id = c.class_id
																		LEFT JOIN tbl_subjects d on d.subject_id = c.subject_id
																		where a.student_id='$reg_id' and a.year_level='$year_level' and a.semester='$ss' and a.school_year='$school_year'  and a.remarks='Fail'";
													$getpreres   = mysqli_query($conn, $getpre);
													while($res = $getpreres->fetch_assoc()) {		
															$subject_code[] = $res['descriptive_title'];
													}
													$json =  json_encode($subject_code, JSON_UNESCAPED_SLASHES);
													$backsubjects =  trim($json, '[]');
															if(empty($subject_code)){
															$sql  = "SELECT a.* , b.* , c.* , d.* FROM tbl_grades a
																LEFT JOIN tbl_student b on a.student_id = b.student_id
																LEFT JOIN tbl_classes c on a.class_id = c.class_id
																LEFT JOIN tbl_subjects d on d.subject_id = c.subject_id
																where a.student_id='$reg_id' and a.year_level='$year_level' and a.semester='$semester' and a.school_year='$school_year'";


															}else {
																$sql  = "SELECT a.* , b.* , c.* , d.* FROM tbl_grades a
																LEFT JOIN tbl_student b on a.student_id = b.student_id
																LEFT JOIN tbl_classes c on a.class_id = c.class_id
																LEFT JOIN tbl_subjects d on d.subject_id = c.subject_id
																where a.student_id='$reg_id' and a.year_level='$year_level' and a.semester='$semester' and a.school_year='$school_year' and  d.pre_requisite NOT IN ($backsubjects)";

															}
															$result1 = mysqli_query($conn, $sql);
															$x=1;
															while($row = $result1->fetch_assoc()) {
															
														 ?>
														<tr>
															<td class="text-center"> <?php echo $row['subject_code'];?></td>
															<td class="text-center"> <?php echo $row['descriptive_title'];?></td>
															<td class="text-center"> <?php echo $row['units'];?></td>
															<td class="text-center"><button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#remove<?php echo $row['id'];?>"> Remove </button></td>
														</tr>
														<div class="modal fade" id="remove<?php echo $row['id'];?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
														<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
																<h3 class="modal-title" id="lineModalLabel">Remove Subject</h3>
															</div>
															<div class="modal-body">
																
																<!-- content goes here -->
																<form method="post">
																  <div class="form-group">
																	<label for="exampleInputEmail1">REMOVE THIS SUBJECT?</label>
																	<input type="hidden" name="id" value="<?php echo $row['id'];?>">
																	
																  </div>
																 
																</div>
																<div class="modal-footer">
																	<div class="btn-group btn-group-justified" role="group" aria-label="group button">
																		<div class="btn-group" role="group">
																			<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
																		</div>
																		<div class="btn-group btn-delete hidden" role="group">
																			<button type="button" id="delImage" class="btn btn-warning btn-hover-red" data-dismiss="modal"  role="button">Close</button>
																		</div>
																		<div class="btn-group" role="group">
																			<button type="submit" name="remove-subject" class="btn btn-info btn-hover-green" data-action="save" role="button">REMOVE</button>
																		</div>
																	</div>
																	</form>
																</div>
															</div>
														  </div>
														</div>
									<?php } ?>		
									</table>
								  </div>
								</div>
								</div>
								<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
								  <div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
											<h3 class="modal-title" id="lineModalLabel">Add Subject</h3>
										</div>
										<div class="modal-body">
											
											<!-- content goes here -->
											<form method="post">
											
											  <div class="form-group">
												<label for="exampleInputEmail1">Subject</label>
												<input type="hidden" name="course_id" value="<?php echo $course_id;?>">
												<input type="hidden" name="semester" value="<?php echo $semester;?>">
												<input type="hidden" name="year_level" value="<?php echo $year_level;?>">
												<select  class="form-control" name="subject" required>
												<option value=""> - Select Subject - </option>
													<?php 
													$getpre  = "SELECT a.* , b.* , c.* , d.* FROM tbl_grades a
																		LEFT JOIN tbl_student b on a.student_id = b.student_id
																		LEFT JOIN tbl_classes c on a.class_id = c.class_id
																		LEFT JOIN tbl_subjects d on d.subject_id = c.subject_id
																		where a.student_id='$reg_id' and a.year_level='$year_level' and a.semester='$ss' and a.school_year='$school_year'  and a.remarks='Fail'";
													$getpreres   = mysqli_query($conn, $getpre);
													while($res = $getpreres->fetch_assoc()) {		
															$subject_code[] = $res['descriptive_title'];
													}
													$json =  json_encode($subject_code, JSON_UNESCAPED_SLASHES);
													$backsubjects =  trim($json, '[]');
													
															if(empty($subject_code)){
															 $subj = "Select a.* ,b.* from tbl_subjects a LEFT JOIN tbl_courses b on a.course_id = b.course_id 
															where a.semester='$semester' and a.year_level='$year_level' and a.course_id='$course_id'
															order by a.subject_code";
															}else {
															 $subj = "Select a.* ,b.* from tbl_subjects a LEFT JOIN tbl_courses b on a.course_id = b.course_id 
															where a.semester='$semester' and a.year_level='$year_level' and a.course_id='$course_id'  and  a.pre_requisite NOT IN ($backsubjects)
															order by a.subject_code ";
															}
													//$subj     =  "SELECT * from tbl_subjects where course_id='$course_id' and semester='$semester' and year_level='$year_level'";
													$subjres  = mysqli_query($conn, $subj);
													while($val = $subjres->fetch_assoc()){
													?>
													<option value="<?php echo  $val['subject_id'];?>"><?php echo $val['descriptive_title'];?></option>
													<?php } ?>
												</select>
											  </div>
											 
											
											</div>
											<div class="modal-footer">
												<div class="btn-group btn-group-justified" role="group" aria-label="group button">
													<div class="btn-group" role="group">
														<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
													</div>
													<div class="btn-group btn-delete hidden" role="group">
														<button type="button" id="delImage" class="btn btn-warning btn-hover-red" data-dismiss="modal"  role="button">Close</button>
													</div>
													<div class="btn-group" role="group">
														<button type="submit" name="add-subject" class="btn btn-info btn-hover-green" data-action="save" role="button">ADD</button>
													</div>
												</div>
												</form>
											</div>
										</div>
									  </div>
									</div>
        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
       <!--  <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>
 -->
        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>


