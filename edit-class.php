<?php
session_start();
error_reporting(0); 
include('includes/config.php');
if(strlen($_SESSION['alogin'])==""){   
	header("Location: index.php"); 
} else {
if(isset($_POST['submit'])){
$subject_id  = $_POST['subject_id'];
$faculty_id  = $_POST['faculty_id'];
$school_year = $_POST['school_year'];
$semester    = $_POST['semester'];
$day         = $_POST['day'];
$dtime       = $_POST['dtime'];
$etime       = $_POST['etime'];
$room        = $_POST['room'];
$classid     = $_POST['classid'];


$section_name = $_POST['section_name'];
$course_id    = $_POST['course_id']; 
$year_level   = $_POST['year_level']; 
$school_year  = $_POST['school_year'];
$section      = $_POST['section'];

$sql = "UPDATE tbl_classes SET faculty_id=?, subject_id=?, course_id=? , year_level=? , section=? , day =?, status =? ,dtime=? ,etime=? ,room=? WHERE class_id =?";

$dbh->prepare($sql)->execute([$faculty_id, $subject_id, $course_id,$year_level,$section,$day,1,$dtime,$etime,$room,$classid]);

$msg="Class created successfully";
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create Class</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
        <style >
            .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 50%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Add/Create Class</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li>Manage Classes</li>
                                        
                                        <li class="active">Create New Class</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5><b>Create New Class</b></h5>

                                                   
                                                </div>
                                            </div>
                                            <div class="panel-body">
											<?php if($msg){?>
											<div class="alert alert-success left-icon-alert" role="alert">
											 <strong>Well done!</strong> <?php echo htmlentities($msg); ?>
											 </div>
											 <?php } else if($error){?>
												<div class="alert alert-danger left-icon-alert" role="alert">
													<strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
												</div>
											<?php } ?>
                                                <form class="form-horizontal" method="post">
												<?php
												$sid=intval($_GET['classid']);
												$sql11 = "SELECT * from tbl_classes where class_id =:sid";
												$query11 = $dbh->prepare($sql11);
												$query11->bindParam(':sid',$sid,PDO::PARAM_STR);
												$query11->execute();
												$results11=$query11->fetchAll(PDO::FETCH_OBJ);
												$cnt=1;
												
												foreach($results11 as $result11)
												{
												?>   
                                                <div class="form-group">
													<label for="default" class="col-sm-2 control-label">Course</label>
													<div class="col-sm-5">
													<select id="CourseNames" name="course_id" class="form-control" required>
													<?php $sql = "SELECT * from tbl_courses order by course_code";
													$query = $dbh->prepare($sql);
													$query->execute();
													$results=$query->fetchAll(PDO::FETCH_OBJ);
													if($query->rowCount() > 0)
													{
													foreach($results as $result){   
													if($result->course_id == $result11->course_id){
													?>
													<option value="<?php echo htmlentities($result->course_id); ?>" selected><?php echo htmlentities($result->course_name); ?></option>
													<?php }}} ?>
													</select>
													</div>
												</div>
												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Year Level</label>
													<div class="col-sm-5">
														<select id="getyearlevel" name="year_level" class="form-control" required>
															<?php 
															$courseid =  $_GET['courseid'];
															$sql1 = "SELECT * from tbl_section where course_id =:sid";
															$query1 = $dbh->prepare($sql1);
															$query1->bindParam(':sid',$courseid,PDO::PARAM_STR);
															$query1->execute();
															$results1=$query1->fetchAll(PDO::FETCH_OBJ);
															foreach($results1 as $result1){   
															if($result1->year_level == $result11->year_level){
															?>
															<option value="<?php echo htmlentities($result1->year_level); ?>" selected><?php echo htmlentities($result1->year_level); ?></option>
															<?php }} ?>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Section </label>
													<div class="col-sm-5">
														<select id="getsubjects" name="section" class="form-control" required>
															<option value="<?php echo htmlentities($result11->section); ?>" selected><?php echo htmlentities($result11->section); ?></option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Subject</label>
													<div class="col-sm-5">
														<select name="subject_id" class="form-control">
															<?php 
															$course =  $_GET['courseid'];
															$sql1 	= "SELECT * from tbl_subjects where  course_id=:cid";
															$query1 = $dbh->prepare($sql1);
															$query1->bindParam(':cid',$course,PDO::PARAM_STR);
															$query1->execute();
															$results=$query1->fetchAll(PDO::FETCH_OBJ);
															foreach($results as $result){
															if($result->subject_id == $result11->subject_id){
															?>
															<option value="<?php echo htmlentities($result->subject_id); ?>" selected><?php echo htmlentities($result->descriptive_title); ?></option>
															<?php } else { ?>
															<option value="<?php echo htmlentities($result->subject_id); ?>" ><?php echo htmlentities($result->descriptive_title); ?></option>
															<?php } } ?>
														</select>
													</div>
												</div>
												
												<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Instructor</label>
													<div class="col-sm-5">
														<select name="faculty_id" class="form-control" required>
															<option value="">-- Select Instructor --</option>
															<?php 
															$sql1 	= "SELECT * from tbl_faculty_info";
															$query1 = $dbh->prepare($sql1);
															$query1->execute();
															$results=$query1->fetchAll(PDO::FETCH_OBJ);
															foreach($results as $result){
															if($result->faculty_id == $result11->faculty_id){
															?>
															<option value="<?php echo htmlentities($result->faculty_id ); ?>" selected><?php echo htmlentities($result->lastname .' , '. $result->firstname); ?></option>
															<?php } else { ?>
															<option value="<?php echo htmlentities($result->faculty_id ); ?>"><?php echo htmlentities($result->lastname .' , '. $result->firstname); ?></option>
															<?php }}?>
														</select>
													</div>
												</div>
                                                   

                                                   
                                                    <!--Time-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Start Time</label>
                                                        <div class="col-sm-5">
														<input type="time" name="dtime" class="form-control"  value="<?php echo $result11->dtime;?>" id="default"  required="required" maxlength="50">
														<input type="hidden" name="classid" class="form-control"  value="<?php echo $_GET['classid'];?>" id="default"  required="required" maxlength="50">
                                                        </div>
                                                    </div>
                                                   <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">End Time</label>
                                                        <div class="col-sm-5">
														<input type="time" name="etime" class="form-control"  value="<?php echo $result11->etime;?>" id="default"  required="required" maxlength="50">
                                                        </div>
                                                    </div>
                                                   

                                                    <!--day-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Day</label>
                                                        <div class="col-sm-5">
                                                            <select name="day" class="form-control" id="default" required="required">
																<?php 
																	$day = $result11->day;
																	if($day =="Monday"){
																		$monday = "selected";
																	}else if($day =="Tuesday"){
																		$tuesday = "selected";
																	}else if($day =="Wednesday"){
																		$wednesday = "selected";
																	}else if($day =="Thursday"){
																		$thursday = "selected";
																	}else if($day =="Friday"){
																		$friday = "selected";
																	}else if($day =="Saturday"){
																		$saturday = "selected";
																	}
																?>
                                                                <option <?php echo $monday;?> value="Monday">Monday</option>
                                                                <option <?php echo $tuesday;?> value="Tuesday">Tuesday</option>
                                                                <option <?php echo $wednesday;?> value="Wednesday">Wednesday</option>
                                                                <option <?php echo $thursday;?> value="Thursday">Thursday</option>
                                                                <option <?php echo $friday;?> value="Friday">Friday</option>
                                                                <option <?php echo $saturday;?> value="Saturday">Saturday</option>
                                                            </select>
                                                        </div>
                                                    </div>



                                                    <!--room no.-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Room</label>
                                                        <div class="col-sm-5">
															<input type="text" name="room" value="<?php echo $result11->room;?>" class="form-control" id="default" placeholder="e.g. room 209" required="required" maxlength="9">
                                                        </div>
                                                    </div>

													<?php } ?>

                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-5">
                                                            <button type="submit" name="submit" class="btn btn-primary">UPDATE</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
			var UrlLink = "http://localhost/StudentInformationSystem/"
			$('#CourseNames').on('change', function() {
				 var course = this.value;
				 $.ajax({
				   type: "POST",
				   url:UrlLink+'get_year_level.php',
				   data : {
							'course_id'       : course, 
					},
				   success: function(data)
				   {
						$("#year_level").html(data);
				   }
			   });
			});
			
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });

            var expanded = false;

			function showCheckboxes() {
			  var checkboxes = document.getElementById("checkboxes");
			  if (!expanded) {
				checkboxes.style.display = "block";
				expanded = true;
			  } else {
				checkboxes.style.display = "none";
				expanded = false;
			  }
			};
        </script>
    </body>
</html>
