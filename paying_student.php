<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');

$student_id=$_GET['student_id'];


$sql = "SELECT * FROM tbl_new_student WHERE student_id='$student_id'";
$result = mysqli_query($conn, $sql);



if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $row = mysqli_fetch_assoc($result);
    $student_id=$row["student_id"];
    $firstname=$row["firstname"];
    $middlename=$row["middlename"];
    $lastname=$row["lastname"];
    $course_id=$row["course_id"];
    
}

$sql = "SELECT * FROM tbl_courses WHERE course_id='$course_id'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $row = mysqli_fetch_assoc($result);
  $cyear=$row["course_code"];
  

}

  ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        
         <link rel="stylesheet" href="css/new_student1.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
       
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>

    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar-billing.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar-billing.php');?>

           

<div class="container">


  <div class="box2">
    List of Approve Students
    
    <hr style="padding: 0px;">
 <div class="position">
     <p><b>Student ID *</b></p>
  <form action="/action_page.php">
    <input type="text" name="" placeholder="Search . . .">
  </form>
</div>
<div class="position1" >
     <p><b>Date *</b></p>
  <form action="/action_page.php">
    <select name="" id="select" >
      <option value="0">2/15/2021</option>
    </select>
  </form>
</div>
<br/>
<br/>
<button type="submit" name="save" class="btn btn-primary" style="float: right; margin: 0px; padding: 0px; border-radius: 16px; width: 80px; height: 32px; font-size: 12px;"><span class="fa fa-search"> </span>Search</button>
<br/>
 <div class="panel panel-default" style="margin-top: 20px">
                        
                      
                            <div class="table-sorting table-responsive" id="subjectresult">
                                <table class="table table-striped table-bordered table-hover" id="tSortable22">
                                    <thead>
                                        <tr>
                                            <th>ID No.</th>
                                            <th>First Name</th>  
                                            <th>Middle Initial</th>
                                            <th>Last Name</th>
                                            <th>Course</th>
                                            <th>Student Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                               <tbody>
                                 <?php

     $sql = "select * from tbl_new_student";
                  $q = $conn->query($sql);
                  $i=1;
                  while($r = $q->fetch_assoc())
                  {
            echo '<tr>
                                            
                                            
                                            <td>'.$r['student_id'].'</td>
                                            <td>'.$r['firstname'].'</td>
                                            <td>'.$r['middlename'].'</td>
                                            <td>'.$r['lastname'].'</td>
                                            <td>'.$r['course_id'].'</td>
                                            <td>'.$r[''].'</td>
                                         
                                            <td>
                                    <span class="fa fa-newspaper-o"> </span> <a href="report/approve_student.php?student_id='.$r['student_id'].'"> View</a>
                                            </td>

                                             
                                        </tr>';
                    $i++;
                  }

     ?>
                                 </tbody>
                                </table>
                            </div>
                            <p style="float: right; margin: 0px; padding-top: 15px;">   </p>
                     
                    </div>
  </div>
</div>




                 <script>
        document.getElementById('button').addEventListener('click', function() {
  document.querySelector('.bg-modal').style.display = 'flex';
});

         document.querySelector('.close').addEventListener('click', function() {
          document.querySelector('.bg-modal').style.display = 'none';
         });
      </script>
      


        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/waypoint/waypoints.min.js"></script>
        <script src="js/counterUp/jquery.counterup.min.js"></script>
        <script src="js/amcharts/amcharts.js"></script>
        <script src="js/amcharts/serial.js"></script>
        <script src="js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="js/amcharts/themes/light.js"></script>
        <script src="js/toastr/toastr.min.js"></script>
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script src="js/production-chart.js"></script>
        <script src="js/traffic-chart.js"></script>
        <script src="js/task-list.js"></script>
        <script>
            $(function(){

                // Counter for dashboard stats
                $('.counter').counterUp({
                    delay: 10,
                    time: 1000
                });
                /*
                // Welcome notification
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "3000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }*/
                /*toastr["success"]( "Welcome "<?php echo(json_encode($instructorlastname .", ". $instructorfirstname));?>);

            });*/

        </script>
      </div>
    </div>
  </div>
    </body>

   

<style> .foot{text-align: center; */}</style>
</html>

