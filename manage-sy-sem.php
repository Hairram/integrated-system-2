<?php
session_start();
error_reporting(0);
include('includes/config.php');
include('includes/dbconnect.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{if(isset($_GET['delete_sure']))
            {
            $sql = "DELETE FROM tbl_sy_sem where school_year=:stid";
            $query = $dbh->prepare($sql);
            $query->bindParam(':stid',$_GET['delete_sure'],PDO::PARAM_STR);
            $query->execute();
            $msg="School Year deleted successfully!";
            }

         if (isset($_SESSION['cr8_sy_sem_msg'])) {
            $msg=$_SESSION['cr8_sy_sem_msg'];
            $_SESSION['cr8_sy_sem_msg']="";
        }
        else{
            $_SESSION['cr8_sy_sem_msg']="";
        }
 if(isset($_POST['lock-semester'])){
	$update = "UPDATE tbl_sy_sem set isLock=1 where isStatus=1";
	mysqli_query($conn, $update);
	echo "<script> alert('Semester Locked!'); window.location.href='manage-sy-sem.php'; </script>";
 }	 
 if(isset($_POST['unlock-semester'])){
	$update = "UPDATE tbl_sy_sem set isLock=0 where isStatus=1";
	mysqli_query($conn, $update);
	echo "<script> alert('Semester Unlocked!'); window.location.href='manage-sy-sem.php'; </script>";
 }	 

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Manage SY & SEM</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
<?php include('includes/leftbar.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Manage S.Y. and Semester</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li>Manage S.Y. and Semester</li>
            							<!-- <li class="active">List of Subjects</li> -->
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h4><b>List of S.Y. and Semester</b></h4>
                                                   <a href="create-sy-sem.php">   <button  type="text" name="text" class="btn btn-primary">Create New S.Y. and Semester</button></a>
                                                    <?php $sems    = "select * from tbl_sy_sem  where isStatus=1";
														  $semsres = mysqli_query($conn, $sems);
														  $rows = mysqli_fetch_assoc($semsres);
														  $lock=$rows["isLock"];
														  if($lock ==1){
													?>
													<button  type="text" name="text" data-toggle="modal" data-target="#unlock" class="btn btn-warning">UNLOCK SEMESTER</button>
													 <?php } else { ?>
													<button  type="text" name="text" data-toggle="modal" data-target="#lock" class="btn btn-info">LOCK SEMESTER</button>
													 <?php } ?>
												</div>
                                                </div>
                                            <br>
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>

                                           <?php if(isset($_GET['delete'])):?>
    <div class="alert alert-danger left-icon-alert" role="alert">
    <strong>Are you sure to delete <?php echo $_GET['delete']; ?>?</strong>
    <a href="manage-sy-sem.php?delete_sure=<?php echo htmlentities($_GET['delete']);?>"><input class="btn btn-danger" type="button" name="edit" value="YES"></a>
    <a href="manage-sy-sem.php"><input class="btn btn-default" type="button" name="edit" value="CANCEL"></a>
    </div
    ><?php endif; ?> 
                                            <div class="panel-body p-20">
                                                
                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>School Year (S.Y.)</th>
                                                            <th>Semester</th>
                                                            <th>Active Semester</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php 
$sql = "SELECT * from tbl_sy_sem order by school_year";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
    <tr>
        <td><?php echo htmlentities($cnt);?></td>
        <td><?php echo htmlentities($result->school_year);?></td>
        <td><?php echo htmlentities($result->semester);?></td>                                                 
        <td style="text-align:center;"><?php if($result->isStatus == 1) { echo "Default SY and Semester"; }?></td>                                                 
        <td>
        <a href="edit-sy.php?sysem_id=<?php echo htmlentities($result->sy_sem_id);?>"><input type="button" name="edit" value="Edit"></i> </a>
        <a href="manage-sy-sem.php?delete=<?php echo htmlentities($result->school_year);?>"><input type="button" name="delete" value="Delete"> </a> 
        <a href="view-sy-sem.php?view=<?php echo htmlentities($result->sy_sem_id);?>"><input type="button" name="view" value="View Courses"><a>
        </td>
</tr>
<?php $cnt=$cnt+1;}} ?>
		


                                                       
                                                    
                                                    </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->
		<div class="modal fade" id="lock" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title" id="lineModalLabel">Lock Semester</h3>
				</div>
				<div class="modal-body">
				<form method="post">
					ARE YOU SURE TO LOCK THIS SEMESTER ?
				   

				</div>
				<div class="modal-footer">
					<div class="btn-group btn-group-justified" role="group" aria-label="group button">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
						</div>
						<div class="btn-group" role="group">
							<button type="submit" name="lock-semester" class="btn btn-primary btn-hover-green" data-action="save" role="button">Lock</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		  </div>
		</div>
		<div class="modal fade" id="unlock" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title" id="lineModalLabel">Lock Semester</h3>
				</div>
				<div class="modal-body">
				<form method="post">
					ARE YOU SURE TO UNLOCK THIS SEMESTER ?
				   

				</div>
				<div class="modal-footer">
					<div class="btn-group btn-group-justified" role="group" aria-label="group button">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
						</div>
						<div class="btn-group" role="group">
							<button type="submit" name="unlock-semester" class="btn btn-warning btn-hover-green" data-action="save" role="button">Unlock</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		  </div>
		</div>
        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
<?php } ?>

