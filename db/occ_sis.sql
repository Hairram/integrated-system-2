-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2021 at 04:56 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `occ_sis`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `DOB` date NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `lastname`, `firstname`, `middlename`, `address`, `email`, `DOB`, `password`) VALUES
('2021-1-1', 'Pinque', 'Angelo', 'Montalba', 'Opol', 'hitsuraan@gmail.com', '1990-02-02', 'admin'),
('2021-1-2', 'Caudor', 'Marriah', 'Arlle', 'Molugan', 'gwapa@gmail.com', '2021-03-02', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_billing`
--

CREATE TABLE `tbl_billing` (
  `billing_id` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `DOB` date DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_billing`
--

INSERT INTO `tbl_billing` (`billing_id`, `lastname`, `firstname`, `middlename`, `address`, `email`, `DOB`, `password`, `status`) VALUES
('123', 'Caliza', 'Rina', 'a', 'Opol', 'rina@gmail.com', '1981-04-12', '123456', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classes`
--

CREATE TABLE `tbl_classes` (
  `class_id` int(11) NOT NULL,
  `faculty_id` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `year_level` varchar(32) NOT NULL,
  `section` varchar(32) NOT NULL,
  `status` int(1) NOT NULL,
  `day` varchar(50) NOT NULL,
  `dtime` varchar(50) NOT NULL,
  `etime` varchar(32) NOT NULL,
  `room` varchar(50) NOT NULL,
  `student_count` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_classes`
--

INSERT INTO `tbl_classes` (`class_id`, `faculty_id`, `subject_id`, `course_id`, `year_level`, `section`, `status`, `day`, `dtime`, `etime`, `room`, `student_count`) VALUES
(1, '2020-1-1', 1, 2, '1', 'A', 1, 'Monday', '12:31', '12:31', '210', 3),
(2, '2020-1-1', 2, 2, '1', 'A', 1, 'Tuesday', '12:31', '12:31', '210', 3),
(3, '2020-1-1', 3, 2, '1', 'A', 1, 'Wednesday', '12:31', '12:31', '210', 3),
(6, '2020-1-1', 5, 2, '1', 'A', 1, 'Friday', '12:31', '12:31', '210', 3),
(7, '2020-1-1', 4, 2, '1', 'A', 1, 'Monday', '12:31', '12:31', '210', 3),
(9, '2020-1-1', 6, 2, '1', 'A', 1, 'Monday', '12:31', '12:31', '210', 3),
(10, '2020-1-1', 7, 2, '1', 'A', 1, 'Tuesday', '12:31', '12:31', '210', 3),
(11, '2020-1-1', 8, 2, '1', 'A', 1, 'Monday', '12:31', '12:31', '210', 3),
(12, '2020-1-2', 9, 2, '1', 'A', 1, 'Monday', '12:31', '12:31', '210', 2),
(13, '2020-1-2', 10, 2, '1', 'A', 1, 'Tuesday', '12:31', '12:31', '210', 2),
(14, '2020-1-2', 11, 2, '1', 'A', 1, 'Monday', '12:31', '12:31', '210', 2),
(15, '2020-1-2', 12, 2, '1', 'A', 1, 'Tuesday', '12:31', '12:31', '210', 2),
(16, '2020-1-2', 13, 2, '1', 'A', 1, 'Monday', '12:31', '12:31', '210', 2),
(17, '', 14, 2, '2', 'A', 1, 'Monday', '12:31', '12:31', '210', 0),
(18, '', 15, 2, '2', 'A', 1, 'Tuesday', '12:31', '12:31', '210', 0),
(19, '', 16, 2, '2', 'A', 1, 'Wednesday', '00:31', '12:31', '210', 0),
(20, '', 17, 2, '2', 'A', 1, 'Thursday', '12:31', '12:31', '210', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class_details`
--

CREATE TABLE `tbl_class_details` (
  `class_details_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `stud_year_level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_class_details`
--

INSERT INTO `tbl_class_details` (`class_details_id`, `class_id`, `student_id`, `stud_year_level`) VALUES
(1, 1, '2018-1-00617', '1'),
(2, 1, '2', '1'),
(3, 1, '123', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courses`
--

CREATE TABLE `tbl_courses` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `course_major` varchar(50) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_courses`
--

INSERT INTO `tbl_courses` (`course_id`, `course_name`, `course_code`, `course_major`, `date_added`, `dept_id`) VALUES
(2, 'Bachelor of Science in Information Technology', 'BSIT', 'Major in Web Dev', '2021-03-02 16:54:49', 1),
(7, 'Bachelor of Science in Business Administration', 'BSBA', 'Major in Finance', '2021-03-03 04:52:43', 2),
(8, 'Bachelor in Secondary Education', 'BSED', 'Major in English', '2021-03-02 15:01:23', 4),
(9, 'Bachelor in Elementary Education', 'BEED', 'Major in Preschool ', '2021-03-02 15:01:33', 4),
(12, 'Civil Engineering', 'CEG', 'asdasda', '2021-03-17 07:54:24', 13),
(14, 'AHA course1', 'AHAHA', '122', '2021-04-02 05:25:15', 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_curriculum`
--

CREATE TABLE `tbl_curriculum` (
  `curr_id` int(11) NOT NULL,
  `curr_name` varchar(255) NOT NULL,
  `curr_description` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_curriculum`
--

INSERT INTO `tbl_curriculum` (`curr_id`, `curr_name`, `curr_description`, `course_id`) VALUES
(1, 'BSIT-2021', 'New curriculum', 2),
(3, 'Bachelor of Science in Business Administration', 'New Curriculum', 7),
(4, 'asads', 'asdasd', 8),
(5, 'asd', 'asdasd', 7),
(6, 'BSCRIM Curriculum', 'New curriculum for S.Y. 2021', 9),
(7, 'NEW CURR ENGINEERING', 'Updated curr', 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_curr_subjects`
--

CREATE TABLE `tbl_curr_subjects` (
  `curr_sub_id` int(11) NOT NULL,
  `curr_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `yr_level` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `effective_sy` varchar(50) NOT NULL DEFAULT '',
  `revised_sy` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_curr_subjects`
--

INSERT INTO `tbl_curr_subjects` (`curr_sub_id`, `curr_id`, `course_id`, `subject_id`, `yr_level`, `semester`, `effective_sy`, `revised_sy`) VALUES
(1, 3, 7, 13, '1', 'First', '2020-2021', '2021-2022'),
(2, 7, 12, 4, '1', 'First', '2020-2021', '2021-2022');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department`
--

CREATE TABLE `tbl_department` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `dept_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_department`
--

INSERT INTO `tbl_department` (`dept_id`, `dept_name`, `dept_code`) VALUES
(1, 'BSIT Department', 1),
(2, 'BSBA Department', 2),
(4, 'EDUC Department', 3),
(6, 'HRM Department', 4),
(7, 'CRIM Department', 5),
(13, 'ENGINEERING Dept', 10),
(14, 'AHA', 11);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_enrollment_details`
--

CREATE TABLE `tbl_enrollment_details` (
  `reg_id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL,
  `date_registered` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `curr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faculty_dept`
--

CREATE TABLE `tbl_faculty_dept` (
  `faculty_id` varchar(255) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_faculty_dept`
--

INSERT INTO `tbl_faculty_dept` (`faculty_id`, `dept_id`, `position`, `status`) VALUES
('2020-1-1', 1, 'Instructor', '1'),
('2020-1-2', 2, 'Program Head', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faculty_info`
--

CREATE TABLE `tbl_faculty_info` (
  `faculty_id` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `DOB` date NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_faculty_info`
--

INSERT INTO `tbl_faculty_info` (`faculty_id`, `lastname`, `firstname`, `middlename`, `address`, `email`, `DOB`, `password`, `status`, `dept_id`) VALUES
('2020-1-1', 'Pangon', 'Johnrome', 'Poblete', 'Molugan', 'gomeg@gmail.com', '2021-02-02', 'pangon', 1, 1),
('2020-1-2', 'Pinque1', 'Dave111111', 'Montalban1', 'Opol1', 'bogtsiktsik@gmail.com1', '2021-02-02', 'dave', 1, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grades`
--

CREATE TABLE `tbl_grades` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `class_id` int(11) NOT NULL,
  `grade` varchar(5) NOT NULL DEFAULT '0',
  `year_level` varchar(35) NOT NULL,
  `semester` varchar(35) NOT NULL,
  `school_year` varchar(35) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `isStatus` int(1) NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_grades`
--

INSERT INTO `tbl_grades` (`id`, `student_id`, `class_id`, `grade`, `year_level`, `semester`, `school_year`, `remarks`, `isStatus`, `date_posted`) VALUES
(2, '2021-1-00001', 2, '1', '1', 'First', '2020-2021', '', 1, '2021-04-29 02:16:33'),
(3, '2021-1-00001', 3, '1', '1', 'First', '2020-2021', '', 1, '2021-04-29 02:16:31'),
(4, '2021-1-00001', 6, '1', '1', 'First', '2020-2021', '', 1, '2021-04-28 15:48:34'),
(5, '2021-1-00001', 7, '1', '1', 'First', '2020-2021', '', 1, '2021-04-28 15:48:35'),
(6, '2021-1-00001', 9, '1', '1', 'First', '2020-2021', '', 1, '2021-04-28 15:48:36'),
(7, '2021-1-00001', 10, '1', '1', 'First', '2020-2021', 'Fail', 1, '2021-04-29 02:16:29'),
(8, '2021-1-00001', 11, '1', '1', 'First', '2020-2021', '', 1, '2021-04-28 15:48:39'),
(9, '2021-1-00001', 1, '1', '1', 'First', '2020-2021', '', 1, '2021-04-28 15:48:41'),
(31, '2021-1-00001', 13, '2.0', '1', 'Second', '2020-2021', 'Pass', 1, '2021-04-29 04:04:49'),
(32, '2021-1-00001', 14, '3.1', '1', 'Second', '2020-2021', 'Fail', 1, '2021-04-29 04:17:52'),
(34, '2021-1-00001', 12, '1', '1', 'Second', '2020-2021', 'Pass', 1, '2021-04-29 04:04:33'),
(35, '2021-1-00001', 16, '1', '1', 'Second', '2020-2021', '', 1, '2021-04-29 04:20:00'),
(36, '2021-1-00002', 1, '1.0', '1', 'First', '2020-2021', 'Pass', 1, '2021-04-29 04:33:47'),
(37, '2021-1-00002', 2, '1.0', '1', 'First', '2020-2021', 'Pass', 1, '2021-04-29 04:33:57'),
(38, '2021-1-00002', 3, '3.1', '1', 'First', '2020-2021', 'Fail', 1, '2021-04-29 04:34:18'),
(39, '2021-1-00002', 6, '1.4', '1', 'First', '2020-2021', 'Pass', 1, '2021-04-29 04:34:28'),
(40, '2021-1-00002', 7, '1.0', '1', 'First', '2020-2021', 'Pass', 1, '2021-04-29 04:34:37'),
(41, '2021-1-00002', 9, '1.4', '1', 'First', '2020-2021', 'Pass', 1, '2021-04-29 04:34:51'),
(42, '2021-1-00002', 10, '1.4', '1', 'First', '2020-2021', 'Pass', 1, '2021-04-29 04:34:59'),
(43, '2021-1-00002', 11, '3.4', '1', 'First', '2020-2021', 'Fail', 1, '2021-04-29 04:35:10'),
(44, '2021-1-00002', 12, '0', '1', 'Second', '2020-2021', '', 1, '2021-04-29 04:42:25'),
(45, '2021-1-00002', 13, '0', '1', 'Second', '2020-2021', '', 1, '2021-04-29 04:42:25'),
(46, '2021-1-00002', 15, '0', '1', 'Second', '2020-2021', '', 1, '2021-04-29 04:42:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_miscellaneous`
--

CREATE TABLE `tbl_miscellaneous` (
  `miscellaneous_id` int(50) NOT NULL,
  `miscellaneous_type` varchar(255) NOT NULL,
  `miscellaneous_amount` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_miscellaneous`
--

INSERT INTO `tbl_miscellaneous` (`miscellaneous_id`, `miscellaneous_type`, `miscellaneous_amount`) VALUES
(1, 'Athletic Fee', 100),
(2, 'Cultural Fee', 200),
(3, 'Library Fee', 100),
(4, 'Dental/Clinic Fee', 400),
(5, 'Guidance Fee', 200),
(6, 'Computer Fee', 500),
(7, 'Handbook', 300),
(8, 'Registration Fee', 200),
(9, 'School ID', 700),
(10, 'Student Insurance', 1000),
(11, 'Entrance Exam Fee', 600);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_new_student`
--

CREATE TABLE `tbl_new_student` (
  `reg_id` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  `middlename` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `contact_num` varchar(50) NOT NULL DEFAULT '',
  `gender` varchar(50) NOT NULL DEFAULT '',
  `DOB` date NOT NULL,
  `course_id` int(11) NOT NULL,
  `picture` text DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_new_student`
--

INSERT INTO `tbl_new_student` (`reg_id`, `lastname`, `firstname`, `middlename`, `address`, `contact_num`, `gender`, `DOB`, `course_id`, `picture`, `status`) VALUES
(1, 'Saretha', 'Saretha', 'Saretha', 'qweqweqweqwe', '09575757575', 'Male', '2021-02-10', 1, NULL, 1),
(2, 'kaja', 'Warrior', 'Knight', 'Opol', '09654003892', 'Male', '2021-02-17', 1, NULL, 1),
(3, 'Pangon', 'Jiah', 'Saretha', 'Molugan', '0965123123', 'Female', '2021-02-15', 2, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_transaction`
--

CREATE TABLE `tbl_payment_transaction` (
  `pay_transac` int(50) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `or_number` varchar(35) NOT NULL,
  `paid_amount` int(50) NOT NULL,
  `submit_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `transaction_remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_payment_transaction`
--

INSERT INTO `tbl_payment_transaction` (`pay_transac`, `student_id`, `or_number`, `paid_amount`, `submit_date`, `transaction_remark`) VALUES
(1, '2021-1-00002', '12222', 122, '2021-04-26 16:03:12', '12'),
(2, '2021-1-00002', '12133', 78, '2021-04-27 13:36:21', 'initial');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prerequisite`
--

CREATE TABLE `tbl_prerequisite` (
  `subject_id` int(11) NOT NULL,
  `pre_requisites` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pre_enrolled_classes`
--

CREATE TABLE `tbl_pre_enrolled_classes` (
  `pre_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_programhead`
--

CREATE TABLE `tbl_programhead` (
  `Prog_id` varchar(50) NOT NULL DEFAULT '',
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  `middlename` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `DOB` date NOT NULL,
  `dept_id` int(1) NOT NULL,
  `password` varchar(50) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_programhead`
--

INSERT INTO `tbl_programhead` (`Prog_id`, `lastname`, `firstname`, `middlename`, `address`, `email`, `DOB`, `dept_id`, `password`, `status`) VALUES
('11111', 'qweqwe', 'qweqwe', 'qweqwe', 'qweqwe', 'qweq@gmail.com', '2021-02-09', 2, '11111', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section`
--

CREATE TABLE `tbl_section` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL,
  `year_level` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `school_year` varchar(50) NOT NULL DEFAULT '',
  `students` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_section`
--

INSERT INTO `tbl_section` (`section_id`, `section_name`, `course_id`, `year_level`, `semester`, `school_year`, `students`) VALUES
(1, 'A', 2, '1', 'First', '2020-2021', 0),
(5, 'A', 2, '1', 'Second', '2020-2021', 0),
(11, 'A', 2, '2', 'First', '2021-2022', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `student_id` varchar(15) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `year_level` varchar(12) NOT NULL,
  `DOB` date NOT NULL,
  `contact_number` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `school_year` varchar(36) NOT NULL,
  `semester` varchar(36) NOT NULL,
  `course_id` int(50) NOT NULL,
  `entrance_exam_result` varchar(36) NOT NULL,
  `form_137` text NOT NULL,
  `good_moral` text NOT NULL,
  `police_clearance` text NOT NULL,
  `exam_result` text NOT NULL,
  `picture` text NOT NULL,
  `pay_status` int(5) NOT NULL,
  `isApproved` int(1) NOT NULL,
  `isPaid` int(1) NOT NULL,
  `remarks` text NOT NULL,
  `tuition_fee` varchar(25) NOT NULL,
  `msc_fee` varchar(25) NOT NULL,
  `total_school_fee` varchar(25) NOT NULL,
  `balance` varchar(25) NOT NULL,
  `old` int(1) NOT NULL,
  `date_registered` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`student_id`, `lastname`, `firstname`, `middlename`, `address`, `email`, `gender`, `year_level`, `DOB`, `contact_number`, `password`, `status`, `school_year`, `semester`, `course_id`, `entrance_exam_result`, `form_137`, `good_moral`, `police_clearance`, `exam_result`, `picture`, `pay_status`, `isApproved`, `isPaid`, `remarks`, `tuition_fee`, `msc_fee`, `total_school_fee`, `balance`, `old`, `date_registered`) VALUES
('2021-1-00001', 'Sample', 'Sample', 'Sample', 'Sample', 'Sample@yahoo.com', 'Male', '1', '2021-04-29', 'Sample', '2021-1-00001', 1, '2020-2021', 'Second', 2, '89', '', '', '', '', '28837483.jpg', 1, 1, 0, '', '1650', '100', '1750', '1750', 1, '2021-04-28 15:46:11'),
('2021-1-00002', 'Pangon', 'Johnrome', 'Poblete', 'Zone 3 Molugan El Salvador City Misamis Oriental', 'gomeg@gmail.com', 'Male', '1', '1993-08-24', '09654003892', '2021-1-00002', 1, '2020-2021', 'Second', 2, '87', 'Capture.PNG', 'GRADE SHEET.PNG', 'ERdigram.png', 'FLOWCHART CREATE CLASS.PNG', 'ins.PNG', 2, 1, 0, '', '1200', '8600', '9800', '0', 1, '2021-04-29 03:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_fees`
--

CREATE TABLE `tbl_student_fees` (
  `student_fees_id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `student_miscellaneous_id` int(11) NOT NULL,
  `total_school_fees` int(11) NOT NULL,
  `student_subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_miscellaneous_fee`
--

CREATE TABLE `tbl_student_miscellaneous_fee` (
  `stu_mis_id` int(12) NOT NULL,
  `student_id` varchar(36) NOT NULL,
  `miscellaneous_id` int(12) NOT NULL,
  `amount` varchar(35) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_student_miscellaneous_fee`
--

INSERT INTO `tbl_student_miscellaneous_fee` (`stu_mis_id`, `student_id`, `miscellaneous_id`, `amount`, `date_added`) VALUES
(1, '2021-1-00001', 3, '100', '2021-04-29 02:58:28'),
(2, '2021-1-00002', 1, '', '2021-04-29 03:43:33'),
(3, '2021-1-00002', 2, '', '2021-04-29 03:43:33'),
(4, '2021-1-00002', 3, '', '2021-04-29 03:43:33'),
(5, '2021-1-00002', 4, '', '2021-04-29 03:43:34'),
(6, '2021-1-00002', 5, '', '2021-04-29 03:43:34'),
(7, '2021-1-00002', 6, '', '2021-04-29 03:43:34'),
(8, '2021-1-00002', 7, '', '2021-04-29 03:43:34'),
(9, '2021-1-00002', 8, '', '2021-04-29 03:43:34'),
(10, '2021-1-00002', 9, '', '2021-04-29 03:43:34'),
(11, '2021-1-00002', 10, '', '2021-04-29 03:43:34'),
(12, '2021-1-00002', 11, '', '2021-04-29 03:43:35'),
(13, '2021-1-00002', 1, '', '2021-04-29 04:36:48'),
(14, '2021-1-00002', 2, '', '2021-04-29 04:36:48'),
(15, '2021-1-00002', 3, '', '2021-04-29 04:36:48'),
(16, '2021-1-00002', 4, '', '2021-04-29 04:36:48'),
(17, '2021-1-00002', 5, '', '2021-04-29 04:36:48'),
(18, '2021-1-00002', 6, '', '2021-04-29 04:36:48'),
(19, '2021-1-00002', 7, '', '2021-04-29 04:36:48'),
(20, '2021-1-00002', 8, '', '2021-04-29 04:36:49'),
(21, '2021-1-00002', 9, '', '2021-04-29 04:36:49'),
(22, '2021-1-00002', 10, '', '2021-04-29 04:36:49'),
(23, '2021-1-00002', 11, '', '2021-04-29 04:36:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_subject`
--

CREATE TABLE `tbl_student_subject` (
  `stu_sub_id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subjects`
--

CREATE TABLE `tbl_subjects` (
  `subject_id` int(11) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `course_id` int(2) NOT NULL,
  `semester` varchar(30) NOT NULL,
  `year_level` int(12) NOT NULL,
  `descriptive_title` varchar(255) NOT NULL,
  `units` int(11) NOT NULL,
  `lec_hrs` int(50) NOT NULL,
  `lab_hrs` int(50) NOT NULL,
  `hrs_week` int(50) NOT NULL,
  `pre_requisite` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_subjects`
--

INSERT INTO `tbl_subjects` (`subject_id`, `subject_code`, `course_id`, `semester`, `year_level`, `descriptive_title`, `units`, `lec_hrs`, `lab_hrs`, `hrs_week`, `pre_requisite`, `date_added`) VALUES
(1, 'GenEnd1', 2, 'First', 1, 'Understanding the self', 3, 3, 0, 3, '', '2021-04-27 14:12:58'),
(2, 'GenEd2', 2, 'First', 1, 'Purposive Communication', 3, 3, 0, 3, '', '2021-04-27 14:13:32'),
(3, 'GenEd3', 2, 'First', 1, 'Mathematics in the Modern World', 3, 3, 0, 3, '', '2021-04-27 14:14:02'),
(4, 'NSTP1', 2, 'First', 1, 'CWTS/ROTC/LTS', 3, 3, 0, 3, '', '2021-04-27 14:15:18'),
(5, 'PF-1', 2, 'First', 1, 'Movement Competency Training', 2, 2, 0, 2, '', '2021-04-27 14:16:00'),
(6, 'IT101', 2, 'First', 1, 'Intro to Programming', 3, 2, 3, 5, '', '2021-04-27 14:17:11'),
(7, 'IT102', 2, 'First', 1, 'IT Fundamentals', 3, 3, 0, 3, '', '2021-04-27 14:17:41'),
(8, 'IT103', 2, 'First', 1, 'PROGRAMMING 1', 3, 3, 2, 0, '', '2021-04-27 14:18:17'),
(9, 'PF-2', 2, 'Second', 1, 'FITNESS TRAINING', 2, 2, 0, 2, 'Movement Competency Training', '2021-04-29 01:53:29'),
(10, 'NSTP2', 2, 'Second', 1, 'CWTS/ROTC/LTS', 3, 3, 0, 3, 'CWTS/ROTC/LTS', '2021-04-29 01:54:51'),
(11, 'IT104', 2, 'Second', 1, 'PROGRAMMING 2', 3, 3, 2, 5, 'PROGRAMMING 1', '2021-04-29 01:52:42'),
(12, 'IT105', 2, 'Second', 1, 'IAS', 3, 3, 2, 0, 'IT Fundamentals', '2021-04-29 01:52:49'),
(13, 'IT105', 2, 'Second', 1, 'DISCRETE MATH', 3, 3, 2, 5, 'Mathematics in the Modern World', '2021-04-29 01:52:56'),
(14, 'IT201', 2, 'First', 2, 'ACCOUNTING FOR IT', 3, 3, 0, 3, 'Mathematics in the Modern World', '2021-04-29 01:53:07'),
(15, 'IT203', 2, 'First', 2, 'OOP', 3, 3, 2, 5, 'PROGRAMMING 2', '2021-04-29 01:53:17'),
(16, '1T204', 2, 'First', 2, 'DATA STRUCTURES', 3, 3, 2, 5, 'PROGRAMMING 2', '2021-04-29 01:52:26'),
(17, 'IT202', 2, 'First', 2, 'NETWORKING 1', 3, 3, 2, 5, '', '2021-04-27 14:26:41'),
(18, 'IT205', 2, 'Second', 2, 'NETWORKING 2', 3, 3, 2, 5, 'NETWORKING 1', '2021-04-29 01:53:57'),
(19, 'PF-4', 2, 'Second', 2, 'TEAM SPORT', 2, 2, 0, 2, 'Movement Competency Training', '2021-04-29 01:54:57'),
(20, 'IT208', 2, 'Second', 2, 'EVENT-DRIVEN PROGRAMMING', 3, 3, 2, 5, 'OOP', '2021-04-29 01:54:05'),
(21, 'IT207', 2, 'Second', 2, 'INFO MANAGEMENT', 3, 3, 2, 5, 'DATA STRUCTURES', '2021-04-29 01:54:01'),
(22, 'IT308', 2, 'First', 3, 'FUNDAMENTALS OF DB', 3, 3, 2, 5, 'INFO MANAGEMENT', '2021-04-29 01:54:18'),
(23, 'IT305', 2, 'First', 2, 'SIA1', 3, 3, 2, 5, 'INFO MANAGEMENT', '2021-04-29 01:54:14'),
(24, 'IT303', 2, 'First', 3, 'HCI2', 3, 3, 2, 5, 'INFO MANAGEMENT', '2021-04-29 01:54:10'),
(25, 'IT309', 2, 'Second', 3, 'IAS2', 3, 3, 2, 5, 'HCI2', '2021-04-29 01:54:21'),
(26, 'IT310', 2, 'Second', 3, 'SIA2', 3, 3, 2, 5, 'SIA1', '2021-04-29 01:54:24'),
(27, 'IT311', 2, 'Second', 3, 'ADVANCED DB', 3, 3, 2, 5, 'FUNDAMENTALS OF DB', '2021-04-29 01:54:28'),
(28, 'IT316', 2, 'Summer', 3, 'CAPSTONE RESEARCH', 3, 3, 2, 5, '', '2021-04-27 14:40:47'),
(29, 'IT401', 2, 'First', 4, 'CAPSTONE RESEARCH 2', 3, 3, 2, 5, 'CAPSTONE RESEARCH', '2021-04-29 01:54:47'),
(30, 'IT407', 2, 'Second', 4, 'OJT', 9, 9, 0, 9, '', '2021-04-27 14:42:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sy_sem`
--

CREATE TABLE `tbl_sy_sem` (
  `sy_sem_id` int(50) NOT NULL,
  `school_year` varchar(50) NOT NULL DEFAULT '',
  `semester` varchar(50) NOT NULL DEFAULT '',
  `isStatus` int(1) NOT NULL,
  `isLock` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_sy_sem`
--

INSERT INTO `tbl_sy_sem` (`sy_sem_id`, `school_year`, `semester`, `isStatus`, `isLock`) VALUES
(1, '2020-2021', 'First', 1, 0),
(2, '2020-2021', 'Second', 0, 0),
(3, '2020-2021', 'Summer', 0, 0),
(4, '2021-2022', 'First', 0, 0),
(5, '2021-2022', 'Second', 0, 0),
(6, '2021-2022', 'Summer', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tbl_class_details`
--
ALTER TABLE `tbl_class_details`
  ADD PRIMARY KEY (`class_details_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  ADD PRIMARY KEY (`course_id`),
  ADD KEY `dept_id` (`dept_id`);

--
-- Indexes for table `tbl_curriculum`
--
ALTER TABLE `tbl_curriculum`
  ADD PRIMARY KEY (`curr_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `tbl_curr_subjects`
--
ALTER TABLE `tbl_curr_subjects`
  ADD PRIMARY KEY (`curr_sub_id`),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `curr_id` (`curr_id`);

--
-- Indexes for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `tbl_enrollment_details`
--
ALTER TABLE `tbl_enrollment_details`
  ADD PRIMARY KEY (`reg_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `curr_id` (`curr_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `tbl_faculty_dept`
--
ALTER TABLE `tbl_faculty_dept`
  ADD KEY `dept_id` (`dept_id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Indexes for table `tbl_faculty_info`
--
ALTER TABLE `tbl_faculty_info`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `tbl_grades`
--
ALTER TABLE `tbl_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `tbl_miscellaneous`
--
ALTER TABLE `tbl_miscellaneous`
  ADD PRIMARY KEY (`miscellaneous_id`);

--
-- Indexes for table `tbl_new_student`
--
ALTER TABLE `tbl_new_student`
  ADD PRIMARY KEY (`reg_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `tbl_payment_transaction`
--
ALTER TABLE `tbl_payment_transaction`
  ADD PRIMARY KEY (`pay_transac`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `tbl_prerequisite`
--
ALTER TABLE `tbl_prerequisite`
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `tbl_pre_enrolled_classes`
--
ALTER TABLE `tbl_pre_enrolled_classes`
  ADD PRIMARY KEY (`pre_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `tbl_programhead`
--
ALTER TABLE `tbl_programhead`
  ADD PRIMARY KEY (`Prog_id`);

--
-- Indexes for table `tbl_section`
--
ALTER TABLE `tbl_section`
  ADD PRIMARY KEY (`section_id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `tbl_student_fees`
--
ALTER TABLE `tbl_student_fees`
  ADD PRIMARY KEY (`student_fees_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `student_miscellaneous_id` (`student_miscellaneous_id`),
  ADD KEY `student_subject_id` (`student_subject_id`);

--
-- Indexes for table `tbl_student_miscellaneous_fee`
--
ALTER TABLE `tbl_student_miscellaneous_fee`
  ADD PRIMARY KEY (`stu_mis_id`);

--
-- Indexes for table `tbl_student_subject`
--
ALTER TABLE `tbl_student_subject`
  ADD PRIMARY KEY (`stu_sub_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `tbl_sy_sem`
--
ALTER TABLE `tbl_sy_sem`
  ADD PRIMARY KEY (`sy_sem_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_class_details`
--
ALTER TABLE `tbl_class_details`
  MODIFY `class_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_curriculum`
--
ALTER TABLE `tbl_curriculum`
  MODIFY `curr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_curr_subjects`
--
ALTER TABLE `tbl_curr_subjects`
  MODIFY `curr_sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_enrollment_details`
--
ALTER TABLE `tbl_enrollment_details`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_grades`
--
ALTER TABLE `tbl_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tbl_miscellaneous`
--
ALTER TABLE `tbl_miscellaneous`
  MODIFY `miscellaneous_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_payment_transaction`
--
ALTER TABLE `tbl_payment_transaction`
  MODIFY `pay_transac` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_pre_enrolled_classes`
--
ALTER TABLE `tbl_pre_enrolled_classes`
  MODIFY `pre_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_section`
--
ALTER TABLE `tbl_section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_student_fees`
--
ALTER TABLE `tbl_student_fees`
  MODIFY `student_fees_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_student_miscellaneous_fee`
--
ALTER TABLE `tbl_student_miscellaneous_fee`
  MODIFY `stu_mis_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_student_subject`
--
ALTER TABLE `tbl_student_subject`
  MODIFY `stu_sub_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_sy_sem`
--
ALTER TABLE `tbl_sy_sem`
  MODIFY `sy_sem_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  ADD CONSTRAINT `tbl_courses_ibfk_1` FOREIGN KEY (`dept_id`) REFERENCES `tbl_department` (`dept_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
