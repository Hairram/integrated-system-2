<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['Update']))
{
$sid=intval($_GET['subjectid']);
$descriptive_title=$_POST['descriptive_title'];
$subject_code=$_POST['subject_code']; 
$units=$_POST['units']; 
$sql="update  tbl_subjects set descriptive_title=:descriptive_title,subject_code=:subject_code,units=:units where id=:sid";
$query = $dbh->prepare($sql);
$query->bindParam(':descriptive_title',$descriptive_title,PDO::PARAM_STR);
$query->bindParam(':subject_code',$subject_code,PDO::PARAM_STR);
$query->bindParam(':units',$units,PDO::PARAM_STR);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$msg="Subject Info updated successfully";
}
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin Update Section / Add class </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
  <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Edit Class Details</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage Sections</li>
                                        <li class="active">View Details</li>
                                        <li class="active">Edit Class Details</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <!-- <h5>Update Subject</h5> -->

<!-- =+================================================================================================================================================================================================================================================================
                                                        !!!SAMPLE UI ONLY!!!!
=+================================================================================================================================================================================================================================================================ -->
                <form class="form-horizontal" method="post">

                    <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">Subject Code</label>
                                 <div class="col-sm-10">
                                    <input type="text" name="descriptive_title" value="<?php echo htmlentities($result->descriptive_title);?>" class="form-control" id="default" placeholder="School Year" required="required">
                                </div>
                            </div>

                   <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">Descriptive Title</label>
                                 <div class="col-sm-10">
                                    <input type="text" name="descriptive_title" value="2020-2021" class="form-control" id="default" placeholder="School Year" required="required">
                                </div>
                            </div>



                    <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">Time</label>
                                 <div class="col-sm-10">
                                    <input type="text" name="descriptive_title" value="First" class="form-control" id="default" placeholder="Semester" required="required">
                                </div>  
                            </div>

                    <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">Day</label>
                                 <div class="col-sm-10">
                                    <input type="text" name="descriptive_title" value="<?php echo htmlentities($result->descriptive_title);?>" class="form-control" id="default" placeholder="Semester" required="required">
                                </div>
                            </div>


                    <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">Room</label>
                                 <div class="col-sm-10">
                                    <input type="text" name="descriptive_title" value="<?php echo htmlentities($result->descriptive_title);?>" class="form-control" id="default" placeholder="Semester" required="required">
                                </div>
                            </div>


                     <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">Instructor</label>
                                 <div class="col-sm-10">
                                    <input type="text" name="descriptive_title" value="<?php echo htmlentities($result->descriptive_title);?>" class="form-control" id="default" placeholder="Semester" required="required">
                                </div>
                            </div>



                    <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="Update" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>


                                                </div>
                                            </div>

                                            <!-- =+================================================================================================================================================================================================================================================================
                                                        !!!SAMPLE UI ONLY!!!!
=+================================================================================================================================================================================================================================================================ -->
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">

 <!-- <?php
$sid=intval($_GET['subjectid']);
$sql = "SELECT * from tbl_subjects where subject_id=:sid";
$query = $dbh->prepare($sql);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>                -->                                


                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Subject Name</label>
                                                        <div class="col-sm-10">
 <input type="text" name="descriptive_title" value="<?php echo htmlentities($result->descriptive_title);?>" class="form-control" id="default" placeholder="Subject Name" required="required">
                                                        </div>
                                                    </div>
<div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Subject Code</label>
                                                        <div class="col-sm-10">
 <input type="text" name="subject_code" class="form-control" value="<?php echo htmlentities($result->subject_code);?>"  id="default" placeholder="Subject Code" required="required">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Units</label>
                                                        <div class="col-sm-10">
 <input type="text" name="units" value="<?php echo htmlentities($result->units);?>" class="form-control" id="default" placeholder="Units" required="required">
                                                        </div>
                                                    </div>
                                                    <?php }} ?>

                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" name="Update" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
