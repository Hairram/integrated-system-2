<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{

$subject_id=$_POST['subject_id'];
$var2 = explode("/", $subject_id);
$subject_id=$var2[0];
$school_year=$_POST['school_year'];
$semester=$_POST['semester'];
$day=$_POST['day'];
$dtime=$_POST['dtime'];
$room=$_POST['room'];
$sql = "SELECT * from tbl_classes where subject_id=:subject_id and semester=:semester and school_year=:school_year";
$query = $dbh->prepare($sql);
$query->bindParam(':subject_id',$subject_id,PDO::PARAM_STR);
$query->bindParam(':semester',$semester,PDO::PARAM_STR);
$query->bindParam(':school_year',$school_year,PDO::PARAM_STR);
$query->bindParam(':day',$day,PDO::PARAM_STR);
$query->bindParam(':dtime',$dtime,PDO::PARAM_STR);
$query->bindParam(':room',$room,PDO::PARAM_STR);
$query->execute();
$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() <= 0)
{
$sql="INSERT INTO  tbl_classes (subject_id,day,dtime,room) VALUES(:subject_id,:day,:dtime,:room)";
$query = $dbh->prepare($sql);
$query->bindParam(':subject_id',$subject_id,PDO::PARAM_STR);
$query->bindParam(':day',$day,PDO::PARAM_STR);
$query->bindParam(':dtime',$dtime,PDO::PARAM_STR);
$query->bindParam(':room',$room,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Class created successfully";
}
else 
{
$error="Something went wrong. Please try again";
}
}
else
{
$error="Adding forbidden due to duplicate entry of records.";
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create Class</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
        <style >
            .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 50%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Add/Create Class</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li>Manage Sections</li>
                                        <li class="active">View Details</li>
                                        <li class="active">Create Class</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                   <p style="position: relative; left: 16px"><b>Section Name :</b> BSIT - 1A</p>
 <p style="position: absolute; left: 76%;"><b>Course:</b> BSIT</p>
<p style="position: relative; left: 16px"><b>Shool Year :</b> 2020-2021</p></p>

 <p style="position: absolute; left: 76%;"><b>No. of Students:</b> 3</p>
<p style="position: relative; left: 16px"><b>Semester:</b> First</p>


        
</b>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong> <?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">
                                                <div class="form-group">

                                                     

                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Subject</label>
                                                        <div class="col-sm-5">
                                                        <input list="descriptive_titles" name="subject_id" id="default" class="form-control" placeholder="Subject" required>
                                                        <datalist id="descriptive_titles">
<?php $sql = "SELECT * from tbl_subjects order by subject_id";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->subject_id . '/' .$result->descriptive_title); ?>"></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

                                                   
                                                                  <!--Time-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Time</label>
                                                        <div class="col-sm-5">
 <input type="time" name="dtime" class="form-control" id="default"  required="required" maxlength="50">
                                                        </div>
                                                    </div>
                                                   

                                                    <!--day-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Day</label>
                                                        <div class="col-sm-5">
                                                            <select name="day" class="form-control" id="default" required="required">
                                                                <option selected="selected" value="Monday">Monday</option>
                                                                <option value="Tuesday">Tuesday</option>
                                                                <option value="Wednesday">Wednesday</option>
                                                                   <option value="Thursday">Thursday</option>
                                                                      <option value="Friday">Friday</option>
                                                                         <option value="Saturday">Saturday</option>
                                                            </select>
                                                        </div>
                                                    </div>



                                                    <!--room no.-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Room</label>
                                                        <div class="col-sm-5">
 <input type="text" name="room" class="form-control" id="default" placeholder="e.g. room 209" required="required" maxlength="9">
                                                        </div>
                                                    </div>

                                           

                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-5">
                                                            <button type="submit" name="submit" class="btn btn-primary">Create</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });

            var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
};
        </script>
    </body>
</html>
<?PHP } ?>
