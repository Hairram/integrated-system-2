<!DOCTYPE html>
<html>
<head>
	<title></title>

	
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
        <link rel="stylesheet" type="text/css" href="add_subject1.css">
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
</head>
<body>

 <div class="panel panel-default" style="width: 80%; margin-left: auto; margin-right: auto;	margin-top: 100px;">

                        <div class="panel-body">
                        	<div class="search">
               <input type="text" placeholder="Search..." style="height: 35px; width: 21%; outline-color: #0080ff; border: 1px solid black">
               	</div>

                             <div class="table-sorting table-responsive">

                                <table class="table table-striped table-bordered table-hover" id="tSortable22">
                                    <thead>
                                        <tr>
                                        	<th>No.</th>
                                            <th>Subject Code</th>
                                            <th>Descriptive Title</th>
                                            <th>Units</th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>

                                        <tr>
                                          <td>1</td>
                                          <td>GenEd1</td>
                                          <td>Understanding the self</td>
                                          <td>3</td>
                                        
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                        	<td>2</td>
                                          <td>GenEd2</td>
                                          <td>Purposive Communication</td>
                                          <td>3</td>
                                        
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>GenEd3</td>
                                          <td>Mathematics in the Modern World</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>VED</td>
                                          <td>Good Manners and Right Conduct</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>5</td>
                                          <td>PF-1</td>
                                          <td>Movement Compentency Training</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                   		<tr>
                                          <td>6</td>
                                          <td>ARTS101</td>
                                          <td>Arts and Appreciation</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>7</td>
                                          <td>IT-101</td>
                                          <td>Basic Programming</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>8</td>
                                          <td>NATH101</td>
                                          <td>College Algebra</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>9</td>
                                          <td>FILI101</td>
                                          <td>Filipino 101</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>10</td>
                                          <td>GenEd2</td>
                                          <td>Purposive Communication</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>11</td>
                                          <td>GenEd3</td>
                                          <td>Mathematics in the Modern World</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                        <tr>
                                          <td>12</td>
                                          <td>NSTP101</td>
                                          <td>NSTP 1</td>
                                          <td>3</td>
                                          <td><input type="checkbox"></td>
                                        </tr>
                                </table>
                            </div>
                            <button type="submit" name="save" class="btn btn-primary">Save</button>
                        </div>

                    </div>

</body>
</html>