  
<?php
session_start();
error_reporting(0);

include('includes/dbconnect.php');


$reg_id=$_GET['reg_id'];


$sql = "SELECT * FROM tbl_new_student WHERE reg_id='$reg_id'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $row = mysqli_fetch_assoc($result);
    $reg_id=$row["reg_id"];
    $lastname=$row["lastname"];
    $firstname=$row["firstname"];
    $middlename=$row["middlename"];
    $address=$row["address"];
    $contact_num=$row["contact_num"];


    

}


?>  
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
<?php include('includes/leftbar-ph.php');?>  

  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Assigning Subjects to New Student</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
            							 <li> Evaluate Student</li>
                                        <li> New Student Evaluation</li>
                                        <li class="active">View Information</li>
                                         <li class="active">Assign Subjects</li>
            							
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    


<p style="position: relative; left: 16px"><b>Pre-Registration ID: </b><?php echo $reg_id; ?></p>
 <p style="position: absolute; left: 76%;"><b>Course: </b> BSIT</p>
<p style="position: relative; left: 16px"><b>Full name : </b><?php echo $firstname; ?></p></p>
<p style="position: absolute; left: 76%;"><b>Status: </b> Approved</p>
<p style="position: relative; left: 16px"><b>S.Y.: </b> 2020-2021</p>
<p style="position: relative; left: 16px"><b>Semester: </b> First</p>
        
</b>

  <div class="panel-body p-20">                                   
     <table border="1" class="table table-hover table-bordered" style="width:100% !important;">
            <tbody>


<p style="position: relative; left: 20px"><b><h2>BSIT Curriculum</h2></p>
     <!-- SAMPLE FOR UI ONLY!!! -->
                                            

<tr>
       <th colspan="8" style="text-align:center;">FIRST YEAR</th>
    </tr>
    <tr>
       <th colspan="8" style="text-align:center;">FIRST SEMESTER</th>
    </tr>
    <tr>
                                                            
                                                            <th class="text-center">Subject Code</th>
                                                            <th class="text-center">Descriptive Title</th>    
                                                            <th class="text-center">Credit Units</th>
                                                            <th class="text-center">Lec Hours</th>
                                                            <th class="text-center">Lab Hours</th>
                                                            <th class="text-center">Hrs/Week</th>
                                                            <th class="text-center">PRE-REQUISITES</th>
                                                             <th class="text-center">Action</th>
                                                             



                                                    

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>GenEd1</td>
                                                            <td>Understanding the self</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>3</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                           

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>GenEd2</td>
                                                            <td>Purposive Communication</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>3</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>GenEd3</td>
                                                            <td>Mathematics in the Modern World</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>3</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                            

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>VED</td>
                                                            <td>Good Manners and Right Conduct</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>3</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                            

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>PF1</td>
                                                            <td>Movement Competency and Training</td>
                                                            <td>2</td>
                                                            <td>2</td>
                                                            <td>0</td>
                                                            <td>2</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                            
            </td>

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>NSTP 1</td>
                                                            <td>CWTS/ROTC/LTS</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>3</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                            

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>IT101</td>
                                                            <td>Introduction to Computing</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>2</td>
                                                            <td>5</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                            

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>IT102</td>
                                                            <td>IT Fundamentals</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>3</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                            
            

                                                        </tr>

                                                        <tr>
                                                            
                                                            <td>IT103</td>
                                                            <td>Programming 1</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>2</td>
                                                            <td>5</td>
                                                            <td>None</td>
                                                            <td><input type="checkbox"></td>
                                                            

                                                        </tr>

                                                       

        </tbody>
        </table>
         <div class="form-group">
                                                        <div class="col-sm-offset-11 col-sm-2">
                                                            <button type="submit" name="submit" class="btn btn-primary">Assign</button>
                                                        </div>
                                                        </form>
        </div>
                                             <!-- SAMPLE FOR UI ONLY!!! -->


                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
        