<div class="left-sidebar bg-black-300 box-shadow ">
    <div class="sidebar-content">
        <div class="user-info closed">
            <img src="http://placehold.it/90/c2c2c2?text=User" alt="John Doe" class="img-circle profile-img">
            <h6 class="title">User</h6>
        
        </div>
        <!-- /.user-info -->

        <div class="sidebar-nav">
            <ul class="side-nav color-gray">
                <li class="nav-header">
                    <span class="">Main Category</span>
                </li>
                <li>
                    <a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a>
                    
                </li>

                <li><a href="profile.php"><i class="fa fa-user"></i> <span> My Profile</span></a></li>


                 <li>
                    <a href="manage-departments.php"><i class="fa fa-university"></i> <span>Manage Departments</span></i></a>
                    <!-- <ul class="child-nav">
                        <li><a href="create-department.php"><i class="fa fa-bars"></i> <span>Create Department</span></a></li>
                        <li><a href="manage-departments.php"><i class="fa fa fa-server"></i> <span>View Departments</span></a></li>
                    </ul> -->
                </li>

                   <li>
                    <a href="manage-courses.php"><i class="fa fa-object-group"></i> <span>Manage Courses</span> </a>
                    
                       <!--  <li><a href="create-course.php"><i class="fa fa-bars"></i> <span>Create Course</span></a></li>
                        <li><a href="manage-courses.php"><i class="fa fa fa-server"></i> <span>View Courses</span></a></li> -->
                   
                </li>
             

                <li>
                    <a href="manage-subjects.php"><i class="fa fa-book"></i> <span>Manage Subjects</span></a>
                    <!-- <ul class="child-nav">
                        <li><a href="create-subject.php"><i class="fa fa-bars"></i> <span>Create Subject</span></a></li>
                        <li><a href="manage-subjects.php"><i class="fa fa fa-server"></i> <span>View Subjects</span></a></li>
                    </ul> -->
                </li>

                  <li>
                    <a href="manage-sy-sem.php"><i class="fa fa-calendar"></i> <span>Manage S.Y. & Semester</span></a>
                    <!-- <ul class="child-nav">
                        <li><a href="create-subject.php"><i class="fa fa-bars"></i> <span>Create Subject</span></a></li>
                        <li><a href="manage-subjects.php"><i class="fa fa fa-server"></i> <span>View Subjects</span></a></li>
                    </ul> -->
                </li>

                   <li>
                    <a href="manage-curriculum.php"><i class="fa fa-book"></i> <span>Manage Curriculum</span> </i></a>
                   <!--  <ul class="child-nav">
                        <li><a href="create-curriculum.php"><i class="fa fa-bars"></i> <span>Create curriculum</span></a></li>
                        <li><a href="view-curr-list.php"><i class="fa fa fa-server"></i> <span>BSIT Curriculum</span></a></li>
                        <li><a href="manage-subjects.php"><i class="fa fa fa-server"></i> <span>BSBA Curriculum</span></a></li>
                        <li><a href="manage-subjects.php"><i class="fa fa fa-server"></i> <span>BEED Curriculum</span></a></li>
                        <li><a href="manage-subjects.php"><i class="fa fa fa-server"></i> <span>BSED Curriculum</span></a></li>
                    </ul> -->
                </li>

                

                 <li>
                    <a href="manage-sections.php"><i class="fa fa-puzzle-piece"></i> <span>Manage Sections</span> </i></a>
                   <!--  <ul class="child-nav">
                        <li><a href="create-section.php"><i class="fa fa-bars"></i> <span>Create Section</span></a></li>
                        
                        <li><a href="create-class-try.php"><i class="fa fa fa-server"></i> <span>Create Class</span></a></li>
                        <li><a href="manage-sections.php"><i class="fa fa fa-server"></i> <span>View Sections</span></a></li>
                        <li><a href="manage-classes.php"><i class="fa fa fa-server"></i> <span>View Classes</span></a></li> -->
                        
                  
                </li>
               


                 <li>
                    <a href="manage-classes.php"><i class="fa fa-clipboard"></i> <span>Manage Classes</span> </a>
                </li>

             
               
                
               
                <li class="has-children">
                    <a href="#"><i class="fa fa-users"></i> <span>Manage Program Head </span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="add-proghead.php"><i class="fa fa-bars"></i> <span>Add Program Head</span></a></li>
                      <li><a href="create-student-try.php"><i class="fa fa-bars"></i> <span>Update Program Head</span></a></li>
                          <li><a href="create-student-try.php"><i class="fa fa-bars"></i> <span>Delete Program Head</span></a></li>  
                          <li><a href="manage-progheads.php"><i class="fa fa fa-server"></i> <span>View Program Head</span></a></li>
                    </ul>
                </li>


                  


                <li class="has-children">
                    <a href="#"><i class="fa fa-users"></i> <span>Student Masterlist</span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="old-student-list.php"><i class="fa fa-bars"></i> <span>Old Students</span></a></li>
                        <li><a href="new-student-list.php"><i class="fa fa-bars"></i> <span>New Students</span></a></li>
                    </ul>
                </li>
















    <li class="has-children">
        <a href="#"><i class="fa fa-info-circle"></i> <span>Generate Reports</span> <i class="fa fa-angle-right arrow"></i></a>
        <ul class="child-nav">

        <li><a href="TOR.php"><i class="fa fa-bars"></i> <span>Transcript of Records</span></a></li>
        <li><a href="view-failed-stu.php"><i class="fa fa-bars"></i> <span>Failed students</span></a></li>
        <li><a href="view-pass-stu.php"><i class="fa fa-bars"></i> <span>Passed students</span></a></li>

        </ul>
    
            
    </li>


    <li><a href="change-password.php"><i class="fa fa fa-server"></i> <span>Change Password</span></a></li>


        </div>
        <!-- /.sidebar-nav -->
    </div>
    <!-- /.sidebar-content -->
</div>