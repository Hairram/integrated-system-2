
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="left-sidebar bg-black-300 box-shadow "  style="width: 230px;">
    <div class="sidebar-content">
        <div class="user-info closed">
        </div>
        <!-- /.user-info -->

        <div class="sidebar-nav">
            <ul class="side-nav color-gray">
                <li class="nav-header">
                    <span class="">Main Category</span>
                </li>
                <li>
                    <a href="find-billing.php" id="nav1"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a>
                    
                </li>
                <li class="has-children">              
                     <a href="#"><i class="fa fa-users"></i> <span>Students</span> <i class="fa fa-angle-right arrow"></i></a> 
                     <ul class="child-nav">
                         <li><a href="approved.php"><i class="fa fa-user"></i> <span>Approved Student</span></a></li>
                         <li><a href="masterlist.php"><i class="fa fa-id-badge" aria-hidden="true"></i> <span>Master List Classes</span></a></li>
                     </ul>   
                </li>
                 <li>              
                     <li><a href="payment.php" id="nav3"><i class="fa fa-credit-card"></i> <span>Payment</span></a></li>    
                </li>
                 <li>              
                     <li><a href="enrolled.php" id="nav4"><i class="fa fa-registered"></i> <span>Enrolled Students</span></a></li>    
                </li>
                 <li >
                    <a href="reports_billing.php"><i class="fa fa-file-text"></i> <span>Reports</span></a>
                  
                </li>
              <li>              
                     <li><a href="miscellaneous.php" id="nav5"><i class="fa fa-money"></i> <span>Miscellaneous</span></a></li>
                </li>
                <li>              
                     <li><a href="billing-admin-profile.php" id="nav6"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                </li>
                <li>              
                     <li><a href="billing-changepass.php" id="nav7"><i class="fa fa-undo"></i> <span>Change Password</span></a></li>
                </li>




               

   


        </div>
        <!-- /.sidebar-nav -->
    </div>
    <!-- /.sidebar-content -->
</div>
<script>
var cr = window.location.toString();
 if(cr.search("find-billing")>0) {document.getElementById("nav1").style.backgroundColor= "black";}
 if(cr.search("approved")>0) {document.getElementById("nav2").style.backgroundColor= "black";}
  if(cr.search("payment")>0) {document.getElementById("nav3").style.backgroundColor= "black";}
   if(cr.search("enrolled")>0) {document.getElementById("nav4").style.backgroundColor= "black";}
    if(cr.search("miscellaneous")>0) {document.getElementById("nav5").style.backgroundColor= "black";}
     if(cr.search("billing-admin-profile")>0) {document.getElementById("nav6").style.backgroundColor= "black";}
      if(cr.search("billing-changepass")>0) {document.getElementById("nav7").style.backgroundColor= "black";}     
</script>