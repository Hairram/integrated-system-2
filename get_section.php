<?php 
error_reporting(0);
include('includes/config.php'); 
include('includes/dbconnect.php');
$sems    = "select * from tbl_sy_sem  where isStatus=1";
$semsres = mysqli_query($conn, $sems);
$rows = mysqli_fetch_assoc($semsres);
$school_year	= $rows["school_year"];
$semester		= $rows["semester"];
?>


<div class="form-group">
	<label for="default" class="col-sm-2 control-label">Section </label>
	<div class="col-sm-5">
		<select id="getsubjects" name="section" class="form-control" required>
			<option value=""> -- Select Section -- </option>
			<?php 
			$yearlevel =  $_POST['yearlevel'];
			$course    =  $_POST['course'];
			$sql1 = "SELECT * from tbl_section where year_level ='$yearlevel' and course_id='$course' and semester='$semester' and school_year='$school_year'";
			$query1 = $dbh->prepare($sql1);
			$query1->execute();
			$results1=$query1->fetchAll(PDO::FETCH_OBJ);
			foreach($results1 as $result1){   
			?>
			<option value="<?php echo htmlentities($result1->section_name); ?>"><?php echo htmlentities($result1->section_name); ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<script>
var UrlLink = "http://localhost/StudentInformationSystem/"
$('#getsubjects').on('change', function() {
	$.ajax({
		type: "POST",
		url:UrlLink+'get_subject.php',
		data : {'course':<?php echo $course;?> },
		success: function(data) {
				$("#subject_level").html(data);
			 }
		 });
});
</script>