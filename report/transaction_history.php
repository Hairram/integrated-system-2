<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {
    header("Location: index.php");
    }
    else{
        $billing_id = $_SESSION['billing_id'];
        $billinglastname = $_SESSION['lastname'];
         $billingrfirstname = $_SESSION['firstname'];
        ?>
<!DOCTYPE html>
<html lang="en">
    <head>
       <link rel="stylesheet" href="css/payment.js" >
         <link rel="stylesheet" href="css/payment1.css" >
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>

    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar-billing.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar-billing.php');?>

           

<div class="container">


  <div class="box2">
    List of Paying Students
    <hr style="padding-bottom: 3px;">
    <div class="wrap">
  <button style="background-color: #fff; border: 1px solid #fff; margin: 0px;padding: 0px; position: absolute; right: 81px"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true" style="margin-right: 3px"></i></button>
<button style="background-color: #fff; border: 1px solid #fff; margin: 0px;padding: 0px; position: absolute; right: 58px"><i class="fa fa-file-word-o fa-lg" aria-hidden="true" style="margin-right: 5px"></i></button>
<button style="background-color: #fff; border: 1px solid #fff; margin: 0px;padding: 0px; position: absolute; right: 42px"><i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i>
</button>
   <div class="search">
      <input type="text" class="searchTerm" placeholder="Search. . . ">
     <i class="fa-file-pdf"></i>

   </div>

   <div class="table">
   <table>
  <tr>
  <th>No.</th>                                    
    <th>Studen ID</th>
    <th>First Name</th>
    <th>Middle Initial</th>
    <th>Last Name</th>
    <th>Course</th>
    <th>Fees</th>
    <th>Balance</th>
    <th>Date Approve</th>
    <th>Action</th>
    <th>History</th>
   </tr>
  <tr>
    <td>1</td>
     <td>2021-1-1001</td>
     <td>Christopher</td>
     <td>M.</td>
     <td>Lague</td>
     <td>BSIT</td>
     <td>12,000</td>
     <td>3,000</td>
     <td>Feb-26-2021</td>
     <td>
      <button  class="btn btn-primary btn-xs" id="button"> Take Fees </button>
     </td>
     <td>
       <a href="report/transaction_history.php" class="btn btn-primary btn-xs" id="button">View</a>
     </td>
   </tr>
   <tr>
    <td>2</td>
     <td>2021-1-1001</td>
     <td>Johnrome</td>
     <td>M.</td>
     <td>Pangon</td>
     <td>BSBA</td>
     <td>8,000</td>
     <td>4,000</td>
     <td>Feb-26-2021</td>
     <td>
      <button  class="btn btn-primary btn-xs" id="button"> Take Fees </button>
     </td>
     <td>
       <a href="#" class="btn btn-primary btn-xs" id="button">View</a>
     </td>
   </tr>
</table>
</div>
</div>

  </div>
</div>


        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/waypoint/waypoints.min.js"></script>
        <script src="js/counterUp/jquery.counterup.min.js"></script>
        <script src="js/amcharts/amcharts.js"></script>
        <script src="js/amcharts/serial.js"></script>
        <script src="js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="js/amcharts/themes/light.js"></script>
        <script src="js/toastr/toastr.min.js"></script>
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script src="js/production-chart.js"></script>
        <script src="js/traffic-chart.js"></script>
        <script src="js/task-list.js"></script>
        <script>
            $(function(){

                // Counter for dashboard stats
                $('.counter').counterUp({
                    delay: 10,
                    time: 1000
                });
                /*
                // Welcome notification
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "3000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }*/
                /*toastr["success"]( "Welcome "<?php echo(json_encode($instructorlastname .", ". $instructorfirstname));?>);

            });*/
        </script>
    </body>

    <div class="foot"><footer>

</footer> </div>

<style> .foot{text-align: center; */}</style>
</html>
<?php } ?>
