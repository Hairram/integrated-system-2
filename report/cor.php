<?php 
include('../includes/dbconnect.php');
error_reporting(0);
$student_id=$_GET['student_id'];
$sql = "SELECT * FROM tbl_student WHERE student_id='$student_id'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $student_id			=	$row["student_id"];
    $name				=	$row["firstname"] . ' '. $row["lastname"];
    $middlename			=	$row["middlename"];
    $course_id			=	$row["course_id"];
    $year_level			=	$row["year_level"];
    $semester			=	$row["semester"];
    $schoolyear			=	$row["school_year"];
    $gender				=	$row["gender"];
    $tuition_fee		=	$row["tuition_fee"];
    $msc_fee			=	$row["msc_fee"];
    $total_school_fee	=	$row["total_school_fee"];
    $balance			=	$row["balance"];
	$date 				= date('d-m-Y', strtotime($row['date_registered']));
}

$sql = "SELECT * FROM tbl_courses WHERE course_id='$course_id'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
	$cyear=$row["course_code"];
}


?>
<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">



<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>

     
<style>
<style>
 @media print {
  @page {
  size: 8.5in x 14in portrait;
    margin: 0;
    padding: 0;
}
} 

body {
  margin: 0px;
  padding: 0px;
}    
        </style>
</style>
</head>
<body style="background:white;">
<!-- Container -->
<div class="container-fluid invoice-container" style="max-width: 1200px!important;padding: 0px!important;border:0px solid black; padding-bottom: 100px;"> 
  <!-- Header -->
  <header>
    <div class="row align-items-center">
  <div class="col-sm-1 text-center text-sm-left mb-3 mb-sm-0"> <h3 class="mb-0"> <img id="logo" src="occ.png" title="Koice" alt="Koice" /></h3>
  </div>
  
      <div class="col-sm-6 text-center text-sm-left mb-1 mb-sm-0"> <h4 class="mb-0">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental
</p></div>
      <div class="col-sm-5 text-center text-sm-right">
        <h6 class="mb-0 text-4">ENROLLMENT FORM</h6>
        <p class="mb-0 text-3">Date Enrolled: <?php echo $date; ?></p>
      </div>
    </div>
  </header>
  
  <!-- Main Content -->
  <main>
    <div class="row text-3">
      <div class="col-sm-6 mb-3"> <strong>Full Name: </strong> <span style="font-size: 15px"><?php echo $name; ?></span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Student ID: </strong> <span><?php echo $student_id; ?></span> </div>
    </div>
   
    <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span><?php echo $semester;?> Semester</span>
          </div>
          <div class="col-sm-2"> <strong>School Year: </strong><span><?php echo $schoolyear;?></span>
          </div>
          <div class="col-sm-3 text-sm-center"> <strong>Course & Year: </strong><span><?php echo $cyear .'-'. $year_level;?></span>
          </div>
          <div class="col-sm-2"> <strong>Gender: </strong><span><?php echo $gender;?></span>
          </div>
          <div class="col-sm-2 text-sm-right"> <strong>Student Type: </strong><span>Old</span>
          </div>
        </div>
      </div>
    </div>
   <hr class="mt-0">
  <main>
     <!-- Passenger Details -->
    <h4 class="text-4">Subject Details</h4>
    <div class="table-responsive">
  
      <table class="table table-bordered text-2 table-sm" style="text-align:center!important;">
    <style>.table thead th {   vertical-align:middle!important;} .table-sm td, .table-sm th {padding: 0px 5px 0px px;}</style>
        <thead>
          <tr >
            <th rowspan="2">No.</th>
            <th rowspan="2">Class Code</th>
            <th rowspan="2">Subject Code</th>
      <th rowspan="2">Description</th>
      <th rowspan="2">Units</th>
        <th colspan="3">Schedule</th>
      </tr>
      <tr>
            
          <th>Day</th>
         <th>Time</th>
         <th>Room</th>
          </tr>
        </thead>
        <tbody>
			<?php
		   $sql1 =  "SELECT a.* , b.* FROM tbl_classes a
				     LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
					 WHERE a.course_id='$course_id' and a.year_level='$year_level' and b.semester='$semester'
					 ";
		   $result1 = mysqli_query($conn, $sql1);
			$x=1;
			while($row = $result1->fetch_assoc()) {
			$units +=$row['units'];
			?>
			<tr>
					<td><?php echo $x++;?></td>
					<td><?php echo $row['class_id'];?></td>
					<td><?php echo $row['subject_code'];?></td>
					<td><?php echo $row['descriptive_title'];?></td>
					<td><?php echo $row['units'];?></td>
					<td><?php echo $row['day'];?></td>
					<td><?php echo $row['dtime'].'-'.$row['etime'];?></td>
					<td><?php echo $row['room'];?></td>
					</tr>
			 <?php
			}
		?>
		  
        
        <tr>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px; font-size: 13px;" class="text-sm-right" ><i> Total No. of Units</i></td>
          <td style="border: 0px; border-bottom: 1px solid black;">
          <?php echo $units; ?>
          </td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
        </tr>
     
        </tbody>
      </table>
      
    </div>

    
  
   <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
        
         
          <div class="col-sm-7"> 
      <h4 class="text-4 mt-4">Miscellaneous Fees Breakdown:</h4>
          <div class="table-responsive">
            <table class="table table-bordered text-2 table-sm">
        
        <tbody>
        <?php
        $mis_fee_query = mysqli_query($conn, "SELECT * FROM tbl_student_miscellaneous_fee WHERE student_id= '$student_id' ") or die(mysqli_error($conn));
        while($mis_fee_row = mysqli_fetch_array($mis_fee_query)){

        $miss_data_query = mysqli_query($conn, "SELECT * FROM tbl_miscellaneous WHERE miscellaneous_id='".$mis_fee_row['miscellaneous_id']."'") or die(mysqli_error($conn));
        $mis_data_row = mysqli_fetch_assoc($miss_data_query);
          
        ?>

        <tr>
          <td>
            <?php echo $mis_data_row['miscellaneous_type']; ?>
          </td>

          <td>
            <?php echo $mis_data_row['miscellaneous_amount']; ?>
          </td>
        </tr>
        
       
        <?php

        }
       
      ?>
       
    
        </tbody>
      </table>

    </div>
     <h4 class="text-4 mt-2">Partial Payment:</h4>
          <div class="table-responsive">
            <table class="table table-bordered text-2 table-sm">
        <thead>
		  <tr>
			<th>Amount</th>
			<th>Date</th>
		  </tr> 
        </thead>
        <tbody>
          <tr>
            <td>
            <?php echo number_format($total_school_fee-$balance,2); ?>
            </td>
            <td>
            <?php echo $date; ?>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
          </div>
      
        <div class="col-sm-5" style="text-align:right!important"><br><br>
          
           <div class="table-responsive">
            <table class="table text-1 table-sm">
        
        <tbody>
			<tr><td  style="border:0px; font-size: 15px;"> <strong>Tuition Fee :</strong> <?php echo number_format($tuition_fee,2);?></td></tr> 
			<tr><td style="border:0px; font-size: 15px;"><strong>Miscellaneous Fees:</strong> <?php echo number_format($msc_fee,2);?></td></tr>    
			<tr><td style="border:0px; font-size: 15px;"><strong>Total school Fees :</strong> <?php echo number_format($total_school_fee,2); ?></php></td>  </tr> 
			<tr><td style="border:0px; font-size: 15px;"><p><strong>Student Signatures Over Printed Name</strong></p><p class="col-sm-15 text-sm-right" ><?php echo $name; ?><br>_________________</p></td></tr> 
		</tbody>
      </table>
    </div>
        
          </div>
          
        </div>
      </div>
    </div>
      
    <!-- Important Info -->
    <div class="row" style="border-bottom:1px dashed black">
     
    </div>
  </main>
  <!-- Footer -->
 </main>
  <!-- Footer -->
 
</div>
 <br/>
<div class="container-fluid invoice-container" style="max-width: 1200px!important;padding: 0px!important;border:0px solid black; padding-bottom: 100px;"> 
  <!-- Header -->
  <header>
    <div class="row align-items-center">
  <div class="col-sm-1 text-center text-sm-left mb-3 mb-sm-0"> <h3 class="mb-0"> <img id="logo" src="occ.png" title="Koice" alt="Koice" /></h3>
  </div>
  
      <div class="col-sm-6 text-center text-sm-left mb-1 mb-sm-0"> <h4 class="mb-0">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental
</p></div>
      <div class="col-sm-5 text-center text-sm-right">
        <h6 class="mb-0 text-4">ENROLLMENT FORM</h6>
        <p class="mb-0 text-3">Date Enrolled: <?php echo $date; ?></p>
      </div>
    </div>
  </header>
  
  <!-- Main Content -->
  <main>
    <div class="row text-3">
      <div class="col-sm-6 mb-3"> <strong>Full Name: </strong> <span style="font-size: 15px"><?php echo $name; ?></span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Student ID: </strong> <span><?php echo $student_id; ?></span> </div>
    </div>
   
    <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span><?php echo $semester;?> Semester</span>
          </div>
          <div class="col-sm-2"> <strong>School Year: </strong><span><?php echo $schoolyear;?></span>
          </div>
          <div class="col-sm-3 text-sm-center"> <strong>Course & Year: </strong><span><?php echo $cyear .'-'. $year_level;?></span>
          </div>
          <div class="col-sm-2"> <strong>Gender: </strong><span><?php echo $gender;?></span>
          </div>
          <div class="col-sm-2 text-sm-right"> <strong>Student Type: </strong><span>Old</span>
          </div>
        </div>
      </div>
    </div>
   <hr class="mt-0">
  <main>
     <!-- Passenger Details -->
    <h4 class="text-4">Subject Details</h4>
    <div class="table-responsive">
  
      <table class="table table-bordered text-2 table-sm" style="text-align:center!important;">
    <style>.table thead th {   vertical-align:middle!important;} .table-sm td, .table-sm th {padding: 0px 5px 0px px;}</style>
        <thead>
          <tr >
            <th rowspan="2">No.</th>
            <th rowspan="2">Class Code</th>
            <th rowspan="2">Subject Code</th>
      <th rowspan="2">Description</th>
      <th rowspan="2">Units</th>
        <th colspan="3">Schedule</th>
      </tr>
      <tr>
            
          <th>Day</th>
         <th>Time</th>
         <th>Room</th>
          </tr>
        </thead>
        <tbody>
			<?php
		   $sql1 =  "SELECT a.* , b.* FROM tbl_classes a
				     LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
					 WHERE a.course_id='$course_id' and a.year_level='$year_level' and b.semester='$semester'
					 ";
		   $result1 = mysqli_query($conn, $sql1);
			$x=1;
			while($row = $result1->fetch_assoc()) {
			$units +=$row['units'];
			?>
			<tr>
					<td><?php echo $x++;?></td>
					<td><?php echo $row['class_id'];?></td>
					<td><?php echo $row['subject_code'];?></td>
					<td><?php echo $row['descriptive_title'];?></td>
					<td><?php echo $row['units'];?></td>
					<td><?php echo $row['day'];?></td>
					<td><?php echo $row['dtime'].'-'.$row['etime'];?></td>
					<td><?php echo $row['room'];?></td>
					</tr>
			 <?php
			}
		?>
		  
        
        <tr>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px; font-size: 13px;" class="text-sm-right" ><i> Total No. of Units</i></td>
          <td style="border: 0px; border-bottom: 1px solid black;">
          <?php echo $units; ?>
          </td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
          <td style="border: 0px;"></td>
        </tr>
     
        </tbody>
      </table>
      
    </div>

    
  
   <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
        
         
          <div class="col-sm-7"> 
      <h4 class="text-4 mt-4">Miscellaneous Fees Breakdown:</h4>
          <div class="table-responsive">
            <table class="table table-bordered text-2 table-sm">
        
        <tbody>
        <?php
        $mis_fee_query = mysqli_query($conn, "SELECT * FROM tbl_student_miscellaneous_fee WHERE student_id= '$student_id' ") or die(mysqli_error($conn));
        while($mis_fee_row = mysqli_fetch_array($mis_fee_query)){

        $miss_data_query = mysqli_query($conn, "SELECT * FROM tbl_miscellaneous WHERE miscellaneous_id='".$mis_fee_row['miscellaneous_id']."'") or die(mysqli_error($conn));
        $mis_data_row = mysqli_fetch_assoc($miss_data_query);
          
        ?>

        <tr>
          <td>
            <?php echo $mis_data_row['miscellaneous_type']; ?>
          </td>

          <td>
            <?php echo $mis_data_row['miscellaneous_amount']; ?>
          </td>
        </tr>
        
       
        <?php

        }
       
      ?>
       
    
        </tbody>
      </table>

    </div>
     <h4 class="text-4 mt-2">Partial Payment:</h4>
          <div class="table-responsive">
            <table class="table table-bordered text-2 table-sm">
        <thead>
		  <tr>
			<th>Amount</th>
			<th>Date</th>
		  </tr> 
        </thead>
        <tbody>
          <tr>
            <td>
            <?php echo number_format($total_school_fee-$balance,2); ?>
            </td>
            <td>
            <?php echo $date; ?>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
          </div>
      
        <div class="col-sm-5" style="text-align:right!important"><br><br>
          
           <div class="table-responsive">
            <table class="table text-1 table-sm">
        
        <tbody>
			<tr><td  style="border:0px; font-size: 15px;"> <strong>Tuition Fee :</strong> <?php echo number_format($tuition_fee,2);?></td></tr> 
			<tr><td style="border:0px; font-size: 15px;"><strong>Miscellaneous Fees:</strong> <?php echo number_format($msc_fee,2);?></td></tr>    
			<tr><td style="border:0px; font-size: 15px;"><strong>Total school Fees :</strong> <?php echo number_format($total_school_fee,2); ?></php></td>  </tr> 
			<tr><td style="border:0px; font-size: 15px;"><p><strong>Student Signatures Over Printed Name</strong></p><p class="col-sm-15 text-sm-right" ><?php echo $name; ?><br>_________________</p></td></tr> 
		</tbody>
      </table>
    </div>
        
          </div>
          
        </div>
      </div>
    </div>
      
   
  </main>
  <!-- Footer -->
 </main>
  <!-- Footer -->
 
</div>
<!-- Back to My Account Link -->
 <footer id="breakfooter" > </footer>
<!-- Container -->

<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
 
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none">Take   Print</a> </div>
   

    
  </footer>
</div>
<p class="text-center d-print-none"><a href="../new_student.php">&laquo; Back to Dashboard</a></p>


</body>
</html>