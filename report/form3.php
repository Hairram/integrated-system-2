<?php 
include('../includes/dbconnect.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reports - School Fees Payment System</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
<style>

 @media print {
  @page {
  size: landscape;
    margin: 0;
}
}     

.table td, .table th {
   border-top: 1px solid black;
   border-collapse: collapse; 
   text-align: left;
   border:1px solid black;
   vertical-align: middle;
   text-align:center;
}

</style>
</head>
<body style="background:white;  line-height: 12px;">

<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding:10px!important;border:0px solid black"> 
  <!-- Header -->
  <!-- Main Content -->
  <main>



    <div class="table-responsive">

  <table class="table  text-1 table-sm ">
  <tr >
      <td colspan="18">
        <br>
            <p class="text-2">Republic of Philipines<br><i><b>(Name of State / Local University of College)</b><br>(Address of State / Local University of College)</i></p>
            <h5>FREE HE CONSOLIDATED BILLING DETAILS</h5>
           	<p>Free HE Billing Details Reference Number: <u> <?php echo date('m') .'-OCC-'. date('Y') .'-'. date('d') .'-1';?></u> Date: <?php echo date('M d , Y');?></p>

  </td>
</tr>
 
  
 
    <tr>
      <td style="text-align:left;" colspan="13"><b>ADMISSION FEES (Based on Section 7, Rule II of the IRR of RA 10931)</b></td>
      
    </tr>
    
    <tr>
      <th>Sequence Number</th>
      <th></th>
      <th>Last Name</th>
      <th>Given Name</th>
      <th>Middle Initial</th>
      <th>Sex at Birth (M/F)</th>   
       <th>Brithdate (mm/dd/yyyy)</th>
      <th>Degree Program</th>
      <th>Year Level</th>
      <th>Email Address</th>
      <th>Phone Number</th>
      <th>Entrance Admission Fees</th>
      <th>Remarks (Passed or Failed)</th>
      
    </tr>
	<?php
		$i = 1;
		$control_number = 00001;
		$student_query = mysqli_query($conn, "	SELECT a.* , b.* FROM tbl_student a
												LEFT JOIN tbl_courses b on a.course_id = b.course_id
												WHERE a.status='1' and a.pay_status=2") or die(mysqli_error($conn));
		while ($student_row = mysqli_fetch_array($student_query)) {
		
		$student_id 	= $student_row['student_id'];
		$balance 		= $student_row['balance'];
	?>
    <tr>
      <td><?php echo str_pad($control_number++, 5, '0',STR_PAD_LEFT);?></td>
      <td><?php echo $student_row['student_id'];?></td>
      <td><?php echo $student_row['lastname'];?></td>
      <td><?php echo $student_row['firstname'];?></td>
      <td><?php echo $student_row['middlename'];?></td>
      <td><?php echo $student_row['gender'];?></td>
      <td><?php echo $student_row['DOB'];?></td>
      <td><?php echo $student_row['course_code'];?></td>
      <td><?php echo $student_row['year_level'];?></td>
      <td><?php echo $student_row['email'];?></td>
      <td><?php echo $student_row['contact_number'];?></td>
      <td>250</td>
      <td>Passed</td>
    </tr>
	<?php } ?>
    
     
   
  </table>
    </div>
 </main>
  <!-- Footer -->
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"> Take Print</a> </div>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../reports_billing.php">&laquo; Back to Dashboard</a></p>


</body>
</html>