<?php
include ('../includes/dbconnect.php');
error_reporting(0);
$student_id=$_GET['student_id'];

$sql = "SELECT * FROM tbl_student WHERE student_id='$student_id'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $student_id	=	$row["student_id"];
    $firstname	=	$row["firstname"];
    $middlename	=	$row["middlename"];
    $lastname	=	$row["lastname"];
    $course_id	=	$row["course_id"];
    $year_level	=	$row["year_level"];
    $semester	=	$row["semester"];
    $school_year=	$row["school_year"];
	$old	    =	$row["old"];
	$date_registered	    =	$row["date_registered"];
	if($old == 0){
		$oldnew = 'New';
	} else {
		$oldnew = 'Old';
	}
}

$sql = "SELECT * FROM tbl_courses WHERE course_id='$course_id'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
  $cyear=$row["course_code"];
}

if(isset($_POST['approvestudent'])){
	$student_id	=	$_POST['student_id'];
	$tuition 	= 	$_POST['tuition'];
	$miscfee 	=	$_POST['miscfee'];
	$totalfees	=	$_POST['totalfees'];
	$type 		= 	$_POST['type'];
	if($type==1){
		$update 	= "UPDATE tbl_student SET  status =1  , pay_status ='$type' ,tuition_fee='$tuition' , msc_fee='$miscfee' ,total_school_fee='$totalfees' , balance='$totalfees' where student_id='$student_id'";
	} else {
		$update 	= "UPDATE tbl_student SET  status =1  , pay_status ='$type' ,tuition_fee='$tuition' , msc_fee='$miscfee' ,total_school_fee='$totalfees' , balance='0' where student_id='$student_id'";
	}
	mysqli_query($conn, $update);
	
	$getclass       = "SELECT a.* , b.* FROM tbl_classes a
						LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
						WHERE a.course_id='$course_id' and a.year_level='$year_level' and b.semester='$semester'";
	$getclassres    = mysqli_query($conn, $getclass);
	while($res = $getclassres->fetch_assoc()) {		
		$class_id = $res['class_id'];
		mysqli_query($conn, "UPDATE tbl_grades set isStatus=1 where student_id ='$student_id' and  class_id ='$class_id' and  year_level ='$year_level' and semester='$semester' and school_year ='$school_year'");
		mysqli_query($conn, "UPDATE tbl_classes set student_count = student_count + 1 where class_id = $class_id");
	}	
	header("location:../approved.php?data=updated");
}
?>
<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">



<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" type="text/css" href="../css/approve_student.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="../css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="../css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="../css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="../css/toastr/toastr.min.css" media="screen" >
        <link rel="stylesheet" href="../css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="../css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="../css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="../css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
		<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="all.min.css"/>
		<link rel="stylesheet" type="text/css" href="stylesheet.css"/>

		<style>
		 @media print {
		  @page {
		  size: 8.5in x 14in portrait;
			margin: 0;
		}
		}			
        </style>
</head>
<body style="background:white;">
<!-- Container -->
<div class="container-fluid invoice-container" style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
  <!-- Header -->
  <header>
    <div class="row align-items-center">
	<div class="col-sm-1 text-center text-sm-left mb-2 mb-sm-0"> <h3 class="mb-0"> <img id="logo" src="occ.png" title="Koice" alt="Koice" />
	</div>
	
      <div class="col-sm-6 text-center text-sm-left mb-3 mb-sm-0"> <h4 class="mb-0 text-5">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental</p></div>
      <div class="col-sm-5 text-center text-sm-right">
        <h6 class="mb-0 text-4">ENROLLMENT FORM</h6>
		<?php $date=date_create($date_registered);?>
        <p class="mb-0 text-3">Date Enrolled: <?php echo date_format($date,"F d , Y");?></p>
      </div>
    </div>
  </header>
  
  <!-- Main Content -->
  <main>
    <div class="row text-3">
      <div class="col-sm-6 mb-3"> <strong>Full Name: </strong> <span style="font-size: 15px"><?php echo $firstname; ?> <?php echo $middlename; ?> <?php echo $lastname; ?></span> </div>
      <div class="col-sm-6 mb-3 text-sm-right"> <strong>Student ID: </strong> <span><?php echo $student_id; ?></span> </div>
    </div>
   
    <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-3"> <strong>Semester: </strong><span>First Semester</span>
          </div>
          <div class="col-sm-2"> <strong>School Year: </strong><span>2021-2022</span>
          </div>
          <div class="col-sm-3 text-sm-center"> <strong>Course & Year: </strong><span><?php echo $cyear; ?></span>
          </div>
          <div class="col-sm-2"> <strong>Gender: </strong><span>Male</span>
          </div>
          <div class="col-sm-2 text-sm-right"> <strong>Student Type: </strong><span><?php echo $oldnew;?></span>
          </div>
        </div>
      </div>
    </div> 
	 <hr class="mt-0">
  <main>
     <!-- Passenger Details -->
    <h4 class="text-4">Student Subject Details</h4>
    <div class="table-responsive">
	
      <table class="table table-bordered text-2 table-sm table-striped" style="text-align:center!important;">
	  <style>.table thead th {   vertical-align:middle!important;} .table-sm td, .table-sm th {padding: 0px 5px 0px px;}</style>
        <thead>
          <tr >
            <th rowspan="2">No.</th>
            <th rowspan="2">Class Code</th>
            <th rowspan="2">Subject Code</th>
			<th rowspan="2">Description</th>
			<th rowspan="2">Units</th>
		    <th colspan="3">Schedule</th>
			<th rowspan="2">Instructor</th>
			</tr>
			<tr>
               <th>Day</th>
			   <th>Time</th>
			   <th>Room</th>
			</tr>
        </thead>
        <tbody>
		<?php
			$sql1 =  "SELECT a.* , b.* , c.* FROM tbl_grades a
						LEFT JOIN tbl_classes b on a.class_id = b.class_id
						LEFT JOIN tbl_subjects c on b.subject_id = c.subject_id
						where a.student_id='$student_id' and a.year_level='$year_level' and a.semester='$semester' and a.school_year='$school_year'";
		   // $sql1 =  "SELECT a.* , b.* FROM tbl_classes a
				     // LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
					 // WHERE a.course_id='$course_id' and a.year_level='$year_level' and b.semester='$semester'
					 // ";
		   $result1 = mysqli_query($conn, $sql1);
			$x=1;
			while($row = $result1->fetch_assoc()) {
			$units +=$row['units'];
			
			?>
			<tr>
					<td><?php echo $x++;?></td>
					<td><?php echo $row['class_id'];?></td>
					<td><?php echo $row['subject_code'];?></td>
					<td><?php echo $row['descriptive_title'];?></td>
					<td><?php echo $row['units'];?></td>
					<td><?php echo $row['day'];?></td>
					<td><?php
					$date = new DateTime($row['dtime']);
					$date1 = new DateTime($row['etime']);
					echo $date->format('h:i A') .'-'.  $date1->format('H:i A');
					?></td>
					<td><?php echo $row['room'];?></td>
					<td>-</td>
					</tr>
			 <?php
			}
		?>
			<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><i> Total No. of Units</i></td>
					<td><?php echo $units;?></td>
					<td></td>
					<td></td>
					<td></td>
				  <td></td>
				</tr>
		
        </tbody>
      </table>
      
    </div>

    
	
	 <div class="row">
      
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-5"> 
		  <h4 class="text-4 mt-4">Miscellaneous Fees Breakdown:</h4>
				  <div class="table-responsive">
						<table class="table table-bordered text-2 table-sm">  
        <tbody>
		<?php 
        
        $mis_fee_query = mysqli_query($conn, "SELECT a.*, b.* FROM tbl_student_miscellaneous_fee a  
											  LEFT JOIN tbl_miscellaneous b on a.miscellaneous_id  = b.miscellaneous_id 
											  WHERE a.student_id= '$student_id' and a.school_year ='$school_year' and a.semester='$semester'") or die(mysqli_error($conn));
        while($mis_fee_row = mysqli_fetch_array($mis_fee_query)){
			
		$total_mis_fee +=  $mis_fee_row['miscellaneous_amount'];
        ?>

        <tr>
          <td>
            <?php echo $mis_fee_row['miscellaneous_type']; ?>
          </td> 
          <td>
            <?php echo $mis_fee_row['miscellaneous_amount']; ?>
          </td>
        </tr>
       
        <?php

        }

        $total_fee_query 	= mysqli_query($conn, "SELECT SUM(units) as units FROM tbl_subjects  WHERE course_id='$course_id' and year_level='$year_level' and semester='$semester'") or die(mysqli_error($conn));
        $total_fee_row 		= mysqli_fetch_assoc($total_fee_query);
        $total_fee 			= $units * 150;

      ?>


		   
		
        </tbody>
      </table>
    </div>
    </div>
	<div class="col-sm-7" style="text-align:right!important"><br><br>
	<div class="table-responsive">
	<table>
		<tbody>
		 <tr><td  style="border:0px; font-size: 15px;"> <strong>Tuition Fee :</strong> <?php echo number_format($total_fee,2);?></td></tr> 
		  <tr><td style="border:0px; font-size: 15px;"><strong>Miscellaneous Fees:</strong> <?php echo number_format($total_mis_fee,2);?></td></tr>    
		  <tr><td style="border:0px; font-size: 15px;"><strong>Total school Fees :</strong> <?php echo number_format($total_fee + $total_mis_fee,2); ?></php></td>  </tr> 
		  <tr><td style="border:0px; font-size: 15px;"><p><strong>Student Signatures Over Printed Name</strong></p><p class="col-sm-15 text-sm-right" ><?php echo $firstname; ?> <?php echo $middlename; ?> <?php echo $lastname; ?><br>_________________</p></td></tr> 
		</tbody>
	</table>
   	</div>
    </div>
          
        </div>
      </div>
    </div>
  
  
 
      
    <!-- Important Info -->
    <div class="row" style="border-bottom:1px dashed black">
      
    </div>
  </main>
  <!-- Footer -->
 </main>
  <!-- Footer -->
 
</div>
<!-- Back to My Account Link -->
 <footer id="breakfooter" > </footer>
<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding: 0px!important;border:0px solid black"> 
 
  <footer class="text-center">
    <button  class="btn btn-primary btn-xs" id="button"> Remove Fees </button>
    <button  class="btn btn-primary btn-xs" id="add_button"> Add Fees </button>

    <form method="POST">
		<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
		<input type="hidden" name="tuition" value="<?php echo $total_fee; ?>">
		<input type="hidden" name="miscfee" value="<?php echo $total_mis_fee; ?>">
		<input type="hidden" name="totalfees" value="<?php echo $total_fee + $total_mis_fee; ?>">
		<div class="form-group mt-3">
		<select class="form-control" name="type" required>
			<option value="">Select student type</option>
			<option value="1">Paying student</option>
			<option value="2">Non-Paying student</option>

		</select>
		</div>
		<button type="submit"  name="approvestudent" class="btn btn-primary btn-xs" id="button" style="float: right"> Approve </button>
    </form>
    
  </footer>
</div>
<p class="text-center d-print-none"><a href="../paying_student.php">&laquo; Back to Dashboard</a></p>




<div class="bg-modal delete_modal">
<div class="modal-content">
		<div class="header" style="padding-top: 10px; font-weight: bolder;"><h5 style="color: red">REMOVE FEE</h5>
    <div class="close"><p style="font-size: 27px">+</p>
    </div>
  </div>
     <form method="POST" action="delete_fees.php">

     <input type="hidden" name='student_id' value="<?php echo $student_id; ?>">

      <div class="panel panel-default" style="margin-top: 8px">
                            <div class="table-sorting table-responsive" id="subjectresult">
                                <table class="table table-striped table-bordered table-hover" id="tSortable22">
                                
                                    <thead style="background-color: lightgrey">
                                        <tr>
                                            <th>Type of Fees</th>
                                            <th>Select</th>
                                            <th></th>
                                            
                                        </tr>
                                    
                                    </thead>
                                    <tbody>
                                          <?php
                                          $m_query = mysqli_query($conn, "SELECT * FROM tbl_student_miscellaneous_fee WHERE student_id ='$student_id' ") or die(mysqli_error($conn));
                                          while($m_row = mysqli_fetch_array($m_query)){
                                            $m_sql = mysqli_query($conn, "SELECT * FROM tbl_miscellaneous WHERE miscellaneous_id='".$m_row['miscellaneous_id']."'") or die(mysqli_error($conn));
                                            $m_r = mysqli_fetch_assoc($m_sql);
                                          ?>
                                          <tr>
                                          <td>
                                          <?php echo $m_r['miscellaneous_type'] ?>
                                          </td>

                                          <td>
                                          <?php echo $m_r['miscellaneous_amount'] ?>
                                          </td>

                                          <td>
                                          <input type="checkbox" name="mis_delete[]" value="<?php echo $m_r['miscellaneous_id'] ?>">
                                          </td>
                                          </tr>
                                         <?php } ?>

							         </tbody>
                                </table>

                            </div>

                    </div>


    <div class="button">
    <button type="submit" name="delete" class="btn btn-primary">Remove</button>
  </div>
  </form>
 </div>
</div>
  
</div>
</div>

<div class="bg-modal add_modal" style="display: none">
<div class="modal-content">
		<div class="header" style="padding-top: 10px;"><h5>ADD FEE</h5>
    <div class="close close_add"><p style="font-size: 27px">+</p>

    </div>
  </div>
     <form method="POST" action="delete_fees.php">

     <input type="hidden" name='student_id' value="<?php echo $student_id; ?>">

      <div class="panel panel-default" style="margin-top: 8px">
                            <div class="table-sorting table-responsive" id="subjectresult">
                                <table class="table table-striped table-bordered table-hover" id="tSortable22">
                                
                                    <thead style="background-color: lightgrey">
                                        <tr>
                                            <th>Type of Fees</th>
                                            <th>Select</th>
                                            <th></th>
                                            
                                        </tr>
                                    
                                    </thead>
                                    			<tbody>
												  <?php
													$m_sql = mysqli_query($conn, "SELECT * FROM tbl_miscellaneous") or die(mysqli_error($conn));
													while($m_r = mysqli_fetch_assoc($m_sql)){
													$mid = $m_r['miscellaneous_id'];
													
												  ?>
												  <tr>
												  <td>
												  <?php echo $m_r['miscellaneous_type'] ?>
												  </td>

												  <td>
												  <?php echo $m_r['miscellaneous_amount'] ?>
												  </td>

												  <td>
												  <input type="checkbox" name="mis_add[]" value="<?php echo $m_r['miscellaneous_id'] ?>">
												  </td>
												  </tr>
													<?php } ?>
							                    </tbody>
												</table>

                            </div>

                    </div>


    <div class="button">
    <button type="submit" name="add" class="btn btn-primary">Add</button>
  </div>
  </form>
 </div>
</div>
  
</div>

</div>
                          
<script>
document.getElementById('button').addEventListener('click', function() {
	document.querySelector('.delete_modal').style.display = 'flex';
});

document.getElementById('add_button').addEventListener('click', function() {
	document.querySelector('.add_modal').style.display = 'flex';
});

document.querySelector('.close').addEventListener('click', function() {
	document.querySelector('.delete_modal').style.display = 'none';
});

document.querySelector('.close_add').addEventListener('click', function() {
	document.querySelector('.add_modal').style.display = 'none';
});
      </script>
<script>

  document.getElementById("breakfooter").style.pageBreakBefore = "avoid!important";
</script>

<script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="../js/jquery-ui/jquery-ui.min.js"></script>
        <script src="../js/bootstrap/bootstrap.min.js"></script>
        <script src="../js/pace/pace.min.js"></script>
        <script src="../js/lobipanel/lobipanel.min.js"></script>
        <script src="../js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="../js/prism/prism.js"></script>
        <script src="../js/waypoint/waypoints.min.js"></script>
        <script src="../js/counterUp/jquery.counterup.min.js"></script>
        <script src="../js/amcharts/amcharts.js"></script>
        <script src="../js/amcharts/serial.js"></script>
        <script src="../js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="../js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="../js/amcharts/themes/light.js"></script>
        <script src="../js/toastr/toastr.min.js"></script>
        <script src="../js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="../js/main.js"></script>
        <script src="../js/production-chart.js"></script>
        <script src="../js/traffic-chart.js"></script>
        <script src="../js/task-list.js"></script>

</body>
</html>