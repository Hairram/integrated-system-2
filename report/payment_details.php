<?php
session_start();
error_reporting(0);
include('../includes/dbconnect.php');
?>
<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reports - School Fees Payment System</title>

<!-- Web Fonts
======================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
======================= -->
<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="all.min.css"/>
<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
<style>

 @media print {
  @page {
  size: landscape;
    margin: 0;
}
}     

.table td, .table th {
   border-top: 1px solid black;
   border-collapse: collapse; 
   text-align: left;
   border:1px solid black;
   vertical-align: middle;
   text-align:center;
}

</style>
</head>
<body style="background:white;  line-height: 12px;">

<!-- Container -->
<div class="container-fluid invoice-container " style="max-width: 1200px!important;padding:10px!important;border:0px solid black"> 
  <!-- Header -->
  <!-- Main Content -->
  <main>



    <div class="table-responsive">

  <table class="table  text-1 table-sm ">
  <tr >
      <td colspan="18">
        <br>
          
            <h5>PAYMENT DETAILS</h5>
           
  </td>
</tr>
 
  
    
    <tr>
      <th>NAME</th>
      <th>COURSE</th>
      <th>SEMESTER</th>
      <th>SCHOOL YEAR</th>
      <th>DATE</th>
      <th>OR NUMBER</th>   
      <th>AMOUNT</th>
  
      
    </tr>
		<?php 	
		$sql 	= "select a.* ,b.* ,c.* from tbl_payment_transaction  a
					LEFT JOIN tbl_student b on a.student_id=b.student_id
					LEFT JOIN tbl_courses c on c.course_id = b.course_id";
		$q 		= $conn->query($sql);
		$i		= 1;
		while($r= $q->fetch_assoc())
		{
		?>
        <tr>
			<td><?php echo $r['lastname'] .' ,' . $r['firstname'];?></td>
		    <td><?php echo $r['course_code'];?></td>
			<td><?php echo $r['year_level'] .'-' . $r['semester'];?></td>
		    <td><?php echo $r['school_year'];?></td>
		    <td><?php echo $r['submit_date'];?></td>
		    <td><?php echo $r['or_number'];?></td>
		    <td><?php echo $r['paid_amount'];?></td>
		</tr>
	<?php } ?>
     
   
  </table>
    </div>
 </main>
  <!-- Footer -->
  <footer class="text-center">

    <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"> Take Print</a> </div>
  </footer>
</div>
<p class="text-center d-print-none"><a href="../reports_billing.php">&laquo; Back to Dashboard</a></p>


</body>
</html> 