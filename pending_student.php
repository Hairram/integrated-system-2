<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');
        ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        
         <link rel="stylesheet" href="css/new_student.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
       
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>

    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar-billing.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar-billing.php');?>

           

<div class="container">


  <div class="box2">
  <div class="">
    <header>
    <div class="row align-items-center">
  <div class="col-sm-1 text-center text-sm-left mb-3 mb-sm-0"> <h3 class="mb-0"> <img id="logo" src="images/occlogo.png" title="Koice" alt="Koice" style="width: 60px;" />
  </div>
  
      <div class="col-sm-4 text-center text-sm-left mb-3 mb-sm-0"> <h4 class="mb-0">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental
</p></div>
      <div class="col-sm-5 text-center text-sm-right mb-3 mb-sm-0">
        <h6 class="mb-0 text-4">ENROLLMENT FORM</h6>
        <p class="mb-0 text-3">Date Enrolled: 02-22-2021</p>
      </div>
    </div>
  </header>
  </div>
<br/>
 <div class="panel panel-default" style="margin-top: 20px">
                        
                      
                            <div class="table-sorting table-responsive" id="subjectresult">
                                <table class="table table-striped table-bordered table-hover" id="tSortable22">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>First Name</th>  
                                            <th>Middle Name</th>
                                            <th>Last Name</th>
                                            <th>Student Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                <tr>
                                <td>1</td>
                                <td>Christoper</td>
                                 <td>M.</td>
                                  <td>Lague</td>
                                  <td>New</td>
                             <!-- 
                                   <td>
                                   <input type="checkbox"> Insurance  <input type="checkbox" style="margin: 5px;"> Entrance  <input type="checkbox" style="margin: 5px;"> Tuition  <input type="checkbox" style="margin: 5px;"> Miscellaneous 
                                   </td>
                               -->
                                <td>
                                 <span class="fa fa-newspaper-o"> </span> <a href="report/approve_student.php" id="button"> View</a>
                                </td>
                                 </tr>

                                 <tr>
                                <td>2</td>
                                <td>Prodencio</td>
                                 <td>M.</td>
                                  <td>Montalbal</td>
                                  <td>Old</td>
                                  <!-- 
                                   <td>
                                    <input type="checkbox"> Insurance  <input type="checkbox" style="margin: 5px;"> Entrance <input type="checkbox" style="margin: 5px;"> Tuition  <input type="checkbox" style="margin: 5px;"> Miscellaneous 
                                   </td>
                                 -->
                                  <td>
                                    <span class="fa fa-newspaper-o"> </span> <a href="#"> View</a>
                                </td>
                                 </tr>

                                 <tr>
                                <td>3</td>
                                <td>Maleborn</td>
                                 <td>M.</td>
                                  <td>Opena</td>
                                  <td>New</td>
                                   <!-- 
                                   <td>
                                    <input type="checkbox"> Insurance  <input type="checkbox" style="margin: 5px;"> Entrance <input type="checkbox" style="margin: 5px;"> Tuition  <input type="checkbox" style="margin: 5px;"> Miscellaneous 
                                   </td>
                                 -->
                                  <td>
                                    <span class="fa fa-newspaper-o"> </span> <a href="#"> View</a>
                                </td>
                                 </tr>

                                 <tr>
                                <td>4</td>
                                <td>Girlborn</td>
                                 <td>M.</td>
                                  <td>Opena</td>
                                  <td>Old</td>
                                   <!-- 
                                   <td>
                                    <input type="checkbox"> Insurance  <input type="checkbox" style="margin: 5px;"> Entrance <input type="checkbox" style="margin: 5px;"> Tuition  <input type="checkbox" style="margin: 5px;"> Miscellaneous 
                                   </td>
                                 -->
                                  <td>
                                    <span class="fa fa-newspaper-o"> </span> <a href="#"> View</a>
                                </td>
                                 </tr>
                                </table>
                            </div>
                            <p style="float: right; margin: 0px; padding-top: 15px;"><<   >></p>
                     
                    </div>
  </div>
</div>




                 <script>
        document.getElementById('button').addEventListener('click', function() {
  document.querySelector('.bg-modal').style.display = 'flex';
});

         document.querySelector('.close').addEventListener('click', function() {
          document.querySelector('.bg-modal').style.display = 'none';
         });
      </script>
      


        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/waypoint/waypoints.min.js"></script>
        <script src="js/counterUp/jquery.counterup.min.js"></script>
        <script src="js/amcharts/amcharts.js"></script>
        <script src="js/amcharts/serial.js"></script>
        <script src="js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="js/amcharts/themes/light.js"></script>
        <script src="js/toastr/toastr.min.js"></script>
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script src="js/production-chart.js"></script>
        <script src="js/traffic-chart.js"></script>
        <script src="js/task-list.js"></script>
        <script>
            $(function(){

                // Counter for dashboard stats
                $('.counter').counterUp({
                    delay: 10,
                    time: 1000
                });
                /*
                // Welcome notification
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "3000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }*/
                /*toastr["success"]( "Welcome "<?php echo(json_encode($instructorlastname .", ". $instructorfirstname));?>);

            });*/

        </script>
      </div>
    </div>
  </div>
    </body>

   

<style> .foot{text-align: center; */}</style>
</html>
<?php } ?>
