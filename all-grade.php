<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Grade Inquiry System</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
    <div class="main-wrapper">

		<!-- ========== TOP NAVBAR ========== -->
		<?php include('includes/topbar-stu.php');?> 
		<!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
		<div class="content-wrapper">
			<div class="content-container">
		<?php include('includes/leftbarStudent.php');?>  

        <div class="main-page">
            <div class="container">
                <div class="row page-title-div">
                    <div class="col-md-6">
                        <h2 class="title">Semestral Grades</h2>
                    
                    </div>
                    
                    <!-- /.col-md-6 text-right -->
                </div>
                <!-- /.row -->
                <div class="row breadcrumb-div">
                    <div class="col-md-6">
                        <ul class="breadcrumb">
                            <li><a href="find-student.php"><i class="fa fa-home"></i> Home</a></li>
                             <li> View Grades</li>
                            <li> Semestral Grades</li>
                        </ul>
                    </div>
                 
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

                        <section class="section">
                        <div id="printMe">
                                <div class="row">

                                    <div class="col-md-10 col-md-offset-1">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                     
                                                     <h4 class="text-center">
                                                        <b>SEMESTRAL GRADE REPORT</b>
                                         </h4><br>
										<?php
											$student_id = $_SESSION['student_id']; 
											$sqlsy       = "SELECT * FROM tbl_sy_sem WHERE isStatus=1";
											$resultsy	 = mysqli_query($conn, $sqlsy);
											$rowsy    	 = mysqli_fetch_assoc($resultsy);
											$sy1     	 = $rowsy['school_year'];
											$sem1   	 = $rowsy['semester'];
											
											$sql   		 = "SELECT * FROM tbl_student WHERE student_id='$student_id'";
											$result		 = mysqli_query($conn, $sql);
											$row    	 = mysqli_fetch_assoc($result);
											$student_id  = $row['student_id'];
											$sy			 = $row['school_year'];
											$name		 = $row['lastname'] . ', '.$row['firstname'];
											$sem   		 = $row['semester'];
											$year_level  = $row['year_level'];
											$courseid    = $row['course_id'];
											$faculty_id	 = $_SESSION['faculty_id'];
											
											$course     =  "SELECT * from tbl_courses where course_id='$courseid' ";
											$courseres  = mysqli_query($conn, $course);
											$res = $courseres->fetch_assoc();
											
										?>

										<p><b>Student ID :</b> <?php echo $student_id;?></p>
										<p><b>Student Name :</b> <?php echo $name;?></p></p>
										<p><b>Course-Year:</b> <?php  echo $res['course_code'] .' -'. $row['year_level']; ?> </p>                               


                                        <div class="panel-body p-20">
											
											
											<?php 
											$sql2 =  "SELECT * from  tbl_sy_sem order by sy_sem_id desc";
											$result2 = mysqli_query($conn, $sql2);
											while($row2  = $result2->fetch_assoc()) {
											?>
											<p><b>SEMESTER : </b> <?php echo $sems = $row2['semester'];?></p>
											<p><b>SCHOOL YEAR : </b> <?php echo $sys = $row2['school_year'];?></p>
											
                                                <table border="1" class="table table-hover table-bordered" style="width:100% !important;">
                                                    <tbody>

                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Subject Code</th>
                                                            <th>Descriptive Title</th>    
                                                            <th>Final Grade</th>
                                                            <th>Remarks</th>
                                                            <th>Units </th>
                                                            <th>Instructor</th>

                                                        </tr>
                                                        </tr>
														<?php 
															$sql1 =  "SELECT a.* , b.* , c.* FROM tbl_grades a
																	LEFT JOIN tbl_student b on a.student_id = b.student_id
																	LEFT JOIN tbl_classes c on a.class_id = c.class_id
																	where a.student_id='$student_id' and a.semester='$sems' and a.school_year='$sys'  and a.isStatus=1";
														
														$result1 = mysqli_query($conn, $sql1);
															$x=1;
															while($row  = $result1->fetch_assoc()) {
															$courseid   = $row['course_id'];
															$subjectid  = $row['subject_id'];
															$faculty_id = $row['faculty_id'];
                                                            
														 ?>
                                                        <tr>
                                                            <td><?php echo $x++;?></td>
															<?php 
															$subject     =  "SELECT * from tbl_subjects where subject_id='$subjectid' ";
															$subjectres  = mysqli_query($conn, $subject);
															$res1 = $subjectres->fetch_assoc();
															?>
                                                            <td><?php 	echo $res1['subject_code'];?></td>
                                                            <td><?php 	echo $res1['descriptive_title'];?></td>
                                                            <td><?php 	echo $row['grade'];?></td>
                                                            <td><?php 	echo $row['remarks'];?></td>
                                                            <td><?php 	echo $res1['units'];?></td>
                                                            <?php 
															$faculty     =  "SELECT * from tbl_faculty_info where faculty_id='$faculty_id' ";
															$facultyres  = mysqli_query($conn, $faculty);
															$res2 = $facultyres->fetch_assoc();
															?>
                                                            <td><?php 	echo $res2['firstname'] . ' ' . $res2['lastname'];?></td>

                                                        </tr>
														<?php } ?>
                                                       


     
                                        </div>



                                                    </tbody>
                                                </table>
										<?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="row text-right"><div class="col-sm-10 offset-1"><a class="btn btn-primary printMeButton">Print this page</a></div></div>
                                        <!-- /.panel -->
                                    </div>
                                    
                                    <!-- /.col-md-6 -->
<?php  ?>

  
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                
            });
            $(document).on("click", ".printMeButton", function () {
                console.log('here');
                var printContents = document.getElementById("printMe").innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->

    </body>
</html>
