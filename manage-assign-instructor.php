<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{

    if(isset($_POST['RemoveInstructor']))
     {
		$classID      = $_POST['classID'];
        $sql = "Update tbl_classes set faculty_id='' where  class_id =:class_id";
        $query = $dbh->prepare($sql);
		$query->bindParam(':class_id',$classID,PDO::PARAM_STR);
        $query->execute();
		$msg="Instructor Removed Successfully";
     }

    if(isset($_POST['AssignInstructor']))
    {
		
    $classID      = $_POST['classID'];
    $instructor   = $_POST['instructor']; 
    $sql="update tbl_classes set faculty_id=:instructor where class_id =:class_id";
    $query = $dbh->prepare($sql);
    $query->bindParam(':class_id',$classID,PDO::PARAM_STR);
    $query->bindParam(':instructor',$instructor,PDO::PARAM_STR);
    $query->execute();
    $msg="Instructor Assigned Successfully";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin Manage Assigned Instructors</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
			.errorWrap {
				padding: 10px;
				margin: 0 0 20px 0;
				background: #fff;
				border-left: 4px solid #dd3d36;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
				box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			}
			.succWrap{
				padding: 10px;
				margin: 0 0 20px 0;
				background: #fff;
				border-left: 4px solid #5cb85c;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
				box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
			<?php include('includes/leftbar-ph.php');?>  
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Assign Instructor</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-proghead.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Assign Instructor</li>
            							<!--      -->
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                    <section class="section">
                    <div class="container-fluid">
                    <div class="row">
                    <div class="col-md-12">
                    <div class="panel">
                    <div class="panel-heading">
                    <div class="panel-title">
                    </div>
                    </div>
		         	<?php if($msg){?>
					<div class="alert alert-success left-icon-alert" role="alert">
						<strong>Well done!</strong><?php echo htmlentities($msg); ?>
					</div>
					<?php } ?>
                    <div class="panel-body p-20">
                                           
                    <hr>
                    <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th class="text-center class">Section</th>
                                <th class="text-center class">Subject Code</th>
                                <th class="text-center class">Descriptive Title</th>
                                <th class="text-center class">Units</th>
                                <th class="text-center class">Instructor</th>
                                <th class="text-center class">Action</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php 
							$sql2     = "   SELECT a.*,b.*,c.* from tbl_classes a 
											LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
											LEFT JOIN tbl_courses c on a.course_id = c.course_id
											";
							$query2   = $dbh->prepare($sql2);
							$query2->bindParam(':sid',$sid,PDO::PARAM_STR);
							$query2->bindParam(':year',$year_level,PDO::PARAM_STR);
							$query2->execute();
							$results2 = $query2->fetchAll(PDO::FETCH_OBJ);
							foreach($results2 as $results2){ ?>
							<tr>
							<td class="text-center">1</td>
							<td class="text-center"><?php echo $results2->course_code .' - ' .$results2->section;?></td>
							<td class="text-center"><?php echo $results2->subject_code;?></td>
							<td class="text-center"><?php echo $results2->descriptive_title;?></td>
                            <td class="text-center"><?php echo $results2->units;?></td>
							<td class="text-center">
								<?php if($results2->faculty_id == ""){ echo " NO ASSIGN INSTRUCTOR" ;  }
									  else {
											$faculty_id =  $results2->faculty_id;
											$sql4       = "SELECT * from tbl_faculty_info where faculty_id  =:sid";
											$query4     = $dbh->prepare($sql4);
											$query4->bindParam(':sid',$faculty_id,PDO::PARAM_STR);
											$query4->execute();
											$result4    = $query4->fetchAll(PDO::FETCH_OBJ);
											foreach($result4 as $results4){ 
												 echo $results4->lastname .' , '.$results4->firstname;
											}
									  }
								?>
							</td>
							<td class="text-center">
							<?php if($results2->faculty_id == ""){ ?>
								<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#assign-instructor<?php echo $results2->class_id;?>"> Assign Instructor </button>
							<?php } else { ?>
								<button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#remove-instructor<?php echo $results2->class_id;?>"> Remove Instructor </button>
								<a href="view_students.php?class_id=<?php echo $results2->class_id;?>"><button class="btn btn-primary btn-sm"> View Students </button></a>
							<?php } ?>
							</td>
							</tr>
							<!-- line modal -->
							<div class="modal fade" id="assign-instructor<?php echo $results2->class_id;?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
										<h3 class="modal-title" id="lineModalLabel">Assign Instructor to this Class</h3>
									</div>
									<div class="modal-body">
										
										<!-- content goes here -->
										<form method="POST">
										<input type="hidden" value="<?php echo $results2->class_id;?>" name="classID">
										  <div class="form-group">
											<label for="exampleInputEmail1">Instructor : </label>
											<select class="form-control" name="instructor" required>
											<option value="">-- Select Instructor -- </option>
											<?php $sql3     = "SELECT * from tbl_faculty_info";
											$query3   = $dbh->prepare($sql3);
											$query3->execute();
											$results3 = $query3->fetchAll(PDO::FETCH_OBJ);
											foreach($results3 as $results33){ ?>
												<option value="<?php echo $results33->faculty_id;?>"><?php echo $results33->lastname .' , '.$results33->firstname;?> </option>
											<?php } ?>
											</select>
										  </div>
										

									</div>
									<div class="modal-footer">
										<div class="btn-group btn-group-justified" role="group" aria-label="group button">
											<div class="btn-group" role="group">
												<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
											</div>
											<div class="btn-group" role="group">
												<button type="submit" name="AssignInstructor" class="btn btn-info btn-hover-green" data-action="save" role="button">Assign</button>
											</div>
										</div>
									</div>
									</form>
								</div>
							  </div>
							</div>
							
							<div class="modal fade" id="remove-instructor<?php echo $results2->class_id;?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
										<h3 class="modal-title" id="lineModalLabel">Remove Instructor to this Class</h3>
									</div>
									<div class="modal-body">
										
										<!-- content goes here -->
										<form method="POST">
										<input type="hidden" value="<?php echo $results2->class_id;?>" name="classID">
										 ARE YOUR SURE TO REMOVE INSTRUCTOR TO THIS CLASS?
											</select>
										  </div>
										

									<div class="modal-footer">
										<div class="btn-group btn-group-justified" role="group" aria-label="group button">
											<div class="btn-group" role="group">
												<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
											</div>
											<div class="btn-group" role="group">
												<button type="submit" name="RemoveInstructor" class="btn btn-warning btn-hover-green" data-action="save" role="button">Remove</button>
											</div>
										</div>
									</div>
									</form>
								</div>
							  </div>
							</div>
						<?php } ?>
						</tbody>
                         </table>
                         </div>
                         </div>
                         </div>
                        </div>
                        </div>
						</div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
<?php } ?>

