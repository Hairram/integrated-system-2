<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');
if(strlen($_SESSION['alogin'])==""){
    header("Location: index.php");
} else {
	$billing_id = $_SESSION['billing_id'];
    $billinglastname = $_SESSION['lastname'];
    $billingrfirstname = $_SESSION['firstname'];
	$student_id	 = $_GET['student_id'];
	$sql = "SELECT * FROM tbl_student WHERE student_id='$student_id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		$student_id=$row["student_id"];
		$firstname=$row["firstname"];
		$middlename=$row["middlename"];
		$address=$row["address"];
		$lastname=$row["lastname"];
		$course_id=$row["course_id"];
		$semester=$row["semester"];
		$schoolyear=$row["school_year"];
		$yearlevel=$row["year_level"];
		$contact_number=$row["contact_number"];
		$tuition_fee=$row["tuition_fee"];
		$msc_fee=$row["msc_fee"];
		$balance=$row["balance"];
	}
	$sql = "SELECT * FROM tbl_courses WHERE course_id='$course_id'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		$cyear=$row["course_code"];
	}
	$or = $_GET['or'];
	$sql1 	= "select * from tbl_payment_transaction where student_id='$student_id' and or_number='$or'";
	$result1 = mysqli_query($conn, $sql1);
	if (mysqli_num_rows($result1) > 0) {
		$row1 = mysqli_fetch_assoc($result1);
		$or_number=$row1["or_number"];
		$submit_date=$row1["submit_date"];
		$transaction_remark=$row1["transaction_remark"];
		$paid_amount=$row1["paid_amount"];
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--  This file has been downloaded from bootdey.com    @bootdey on twitter -->
    <!--  All snippets are MIT license http://bootdey.com/license -->
    <title>OCC-Receipt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
      body{
    margin-top: 0px;
    color: #484b51;
    margin: 0px;
    padding: 0px;
}
.text-secondary-d1 {
    color: #728299!important;
}
.page-header {
    margin: 0 0 1rem;
    padding-bottom: 1rem;
    padding-top: .5rem;
    border-bottom: 1px dotted #e2e2e2;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-align: center;
    align-items: center;
}
.page-title {
    padding: 0;
    margin: 0;
    font-size: 1.75rem;
    font-weight: 300;
}
.brc-default-l1 {
    border-color: #dce9f0!important;
}

.ml-n1, .mx-n1 {
    margin-left: -.25rem!important;
}
.mr-n1, .mx-n1 {
    margin-right: -.25rem!important;
}
.mb-4, .my-4 {
    margin-bottom: 1.5rem!important;
}

hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0,0,0,.1);
}

.text-grey-m2 {
    color: #888a8d!important;
}

.text-success-m2 {
    color: #86bd68!important;
}

.font-bolder, .text-600 {
    font-weight: 600!important;
}

.text-110 {
    font-size: 110%!important;
}
.text-blue {
    color: #478fcc!important;
}
.pb-25, .py-25 {
    padding-bottom: .75rem!important;
}

.pt-25, .py-25 {
    padding-top: .75rem!important;
}
.bgc-default-tp1 {
    background-color: rgba(121,169,197,.92)!important;
}
.bgc-default-l4, .bgc-h-default-l4:hover {
    background-color: #f3f8fa!important;
}
.page-header .page-tools {
    -ms-flex-item-align: end;
    align-self: flex-end;
}

.btn-light {
    color: #757984;
    background-color: #f5f6f9;
    border-color: #dddfe4;
}
.w-2 {
    width: 1rem;
}

.text-120 {
    font-size: 120%!important;
}
.text-primary-m1 {
    color: #4087d4!important;
}

.text-danger-m1 {
    color: #dd4949!important;
}
.text-blue-m2 {
    color: #68a3d5!important;
}
.text-150 {
    font-size: 150%!important;
}
.text-60 {
    font-size: 60%!important;
}
.text-grey-m1 {
    color: #7b7d81!important;
}
.align-bottom {
    vertical-align: bottom!important;
}
 </style>
</head>
<body>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<div class="page-content container">
    <div class="page-header text-blue-d2">
      <div>
        <h3 class="mb-0"> <img id="logo" src="images/favicon.png" width="100px" /></h3>
  </div>
      <div class="col-sm-10 text-center text-sm-left mb-1 mb-sm-0"> <h4 class="mb-0">Opol Community College</h4>
        <p class="mb-0">Opol, Misamis Oriental
</p></div>
      

        <div class="page-tools">
            <div class="action-buttons">
               <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none">Take   Print</a> </div>
                   <!--  <i class="mr-1 fa fa-print text-primary-m1 text-120 w-2"></i> -->
            
                
            </div>
        </div>
    </div>

    <div class="container px-0">
        <div class="row mt-0">
            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="row">
                    <div class="col-0">
                       <div class="text-center text-10">
                        <span class="text-120"> OR No.</span> 
                             <span> <i class="fa fa-angle-double-right text-80"></i>
                               #111-222 </span>
                        
                            
                        </div>
                    </div>
                </div>
                <!-- .row -->

                <hr class="row brc-default-l1 mx-n1 mb-4" />

                <div class="row">
                    <div class="col-sm-6 mt-4">
                        <div>
                           <span class="text-600 text-90">Student Name :</span>
                            <span class="text-110"><?php echo $lastname .','.$firstname;?></span>
                        </div>
                       
                            <div>
                             <span class="text-600 text-90">Address :</span>
                               <span class="text-110"><?php echo $address;?></span>
                            </div>
                            <div>
                              <span class="text-600 text-90">Phone No. :</span>
                            <i class="fa fa-phone fa-flip-horizontal text-secondary"></i> <span class="text-110"><?php echo $contact_number;?></span>
                          </div>
                             <div>
                              <span class="text-600 text-90">Course :</span>
                               <span class="text-110"><?php echo $cyear .'-'.$yearlevel;?></span>
                            </div>
                          
                    </div>
                    <!-- /.col -->

                    <div class="text-95 col-sm-6 align-self-start d-sm-flex justify-content-end">
                        <hr class="d-sm-none" />
                        <div class="text-grey-m2">
                            <div class="mt-1 mb-2 text-secondary-m1 text-600 text-125">
                                Invoice
                            </div>

                            <div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Student ID:</span> <?php echo $_GET['student_id'];?></div>

                            <div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Issue Date:</span><?php echo $submit_date;?></div>

                            <div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Remarks:</span> <span class="badge badge-warning badge-pill px-25"><?php echo $transaction_remark;?></span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>

                <div class="mt-4">
                    <div class="row text-600 text-black-50 bgc-default-tp1 py-25">
                        <div class="d-none d-sm-block col-2"><b>OR #</b></div>
                        <div class="col-10 col-sm-7"><b>Particulars of payment</b></div>
                     
                        <div class="col-sm-3"><b>Amount</b></div>
                    </div>

                    <div class="text-95 text-secondary-d3">
                        <div class="row mb-2 mb-sm-0 py-25">
                            <div class="d-none d-sm-block col-2"><b><?php echo $or_number;?></b></div>
                            <div class="col-10 col-sm-7">Tuition Fee</div>
                           <div class="col-2 text-secondary-d2"><?php echo number_format($tuition_fee,2);?></div>
                        </div>

                        <div class="row mb-2 mb-sm-0 py-25">
                            <div class="d-none d-sm-block col-2"></div>
                            <div class="col-10 col-sm-7">Miscellaneous Fee</div>
                           <div class="col-2 text-secondary-d2"><?php echo number_format($msc_fee,2);?></div>
                        </div>
						
						<div class="row mb-2 mb-sm-0 py-25 bgc-default-l4">
                            <div class="d-none d-sm-block col-2"></div>
                            <div class="col-10 col-sm-7">Paid Amount </div>
                           <div class="col-2 text-secondary-d2"><?php echo number_format($paid_amount,2);?></div>
                        </div>

						<div class="row mb-2 mb-sm-0 py-25 bgc-default-l4">
                            <div class="d-none d-sm-block col-2"></div>
                            <div class="col-10 col-sm-7">Balance </div>
                           <div class="col-2 text-secondary-d2"><?php echo number_format($balance,2);?></div>
                        </div>

						
                    
                    </div>

                    <div class="row border-b-2 brc-default-l2"></div>

                   

                    <div class="row mt-3">
                        <div class="col-12 col-sm-6 text-grey-d2 text-100 row mt-lg-0 text-110">
                           Paid by :  <u>Cash</u>
                        </div>
                      

                        <div class="col-12 col-sm-5 text-grey text-90 order-first order-sm-last">

                            <div class="row my-2 align-items-center bgc-primary-l3 p-2">
                                <div class="col-8 text-right">
                                    <b> Total Remaining Balance</b>
                                </div>
                                <div class="col-4">
                                    <span class="text-100 text-success-d3 opacity-2"> <b><u> <?php echo number_format($balance,2);?></u></b></span>
                                </div>
                            </div>
        
                             <div class="row my-2 align-items-center bgc-primary-l3 p-2 mt-4">
                           
                                <div class="col-12 text-right">
                                    <span class="text-110 text-success-d3 opacity-2 "> <u>Student Signature</u></span>
                                </div>

                            </div>

                        </div>
                    </div>
                    

                    <hr />

                    <div>
                        <span class="text-secondary-d2 text-105 t">All above mentioned Amount once paid are non refundale in any case whatsover.</span>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
<?php } ?>
</script>
</body>
</html>