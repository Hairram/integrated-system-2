<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
        
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" type="text/css" href="file:///C:/Users/User/Downloads/fontawesome-free-5.15.1-web/css/all.css">
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN View Grade Sheet IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #dd3d36;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
		.succWrap{
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #5cb85c;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ins.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
				<?php include('includes/leftbar1.php');?>  
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>List of Classes</b></h2>
                                </div>
                            </div>
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-instructor2.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Classes</li>
            							<li class="active">List of Classes</li>
            						</ul>
                                </div>
                            </div>
                        </div>
                        <section class="section">
                            <div class="container-fluid">
                                <div class="row">
                                 <div class="col-md-12">
                                  <div class="panel">
                                   <div class="panel-heading">
                                     <div class="panel-title">
									 <?php
										$sql   		 = "SELECT * FROM tbl_sy_sem WHERE isStatus=1";
										$result		 = mysqli_query($conn, $sql);
										$row    	 = mysqli_fetch_assoc($result);
										$sy     	 = $row['school_year'];
										$sem   		 = $row['semester'];
										$faculty_id	 = $_SESSION['faculty_id'];
									 ?>
										<p><b>SCHOOL YEAR : </b><?php echo $row['school_year'];?></p>
										<p><b>SEMESTER :</b> <?php echo $row['semester'];?></p>
                                     </div>
                                  </div>


                                            <div class="panel-body p-20">

                                                <table id="example1" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Section</th>
                                                            <th class="text-center">Subject Code</th>
                                                            <th class="text-center">Descriptive Title</th>
                                                            <th>Units</th>
                                                            <th>Students</th>
                                                            <th>Action</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
													<?php
													   $sql1 =  "SELECT a.* , b.*,c.* FROM tbl_classes a
																 LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
																 LEFT JOIN tbl_courses c on a.course_id = c.course_id
																 WHERE  b.semester='$sem' and a.faculty_id='$faculty_id'
																 ";
													   $result1 = mysqli_query($conn, $sql1);
														$x=1;
														while($row = $result1->fetch_assoc()) {
													?>
                                                        <tr>
															<td><?php echo $x++;?></td>
															<td class="text-center"><?php echo $row['section'];?></td>
															<td class="text-center"><?php echo $row['subject_code'];?></td>
															<td class="text-center"><?php echo $row['descriptive_title'];?></td>
															<td class="text-center"><?php echo $row['units'];?></td>
															<td class="text-center"><?php echo $row['student_count'];?></td>
															<td class="text-center"> <a href="view-grade-sheet.php?class_id=<?php echo $row['class_id'];?>&semester=<?php echo $sem;?>&sy=<?php echo $sy;?>&section=<?php echo $row['course_code'] .' - '.$row['year_level'].' '.$row['section'];?>"><input type="button" name="view" class="btn btn-info btn-sm" value="View Grade Sheet"></i> </a></td>

                                                            </tr>
													<?php } ?>
                                                           

                                                       
                                                    
                                                    </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
              
        
       

            $(function($) {
                $('#example1').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });


                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>

<?php } ?>
