<?php
session_start(); 
// error_reporting(0);
include('includes/config.php');

if(isset($_POST['login']))
{
    if($_POST['loginAs'] == 'Registrar'){
        $admin_id=$_POST['username'];
        $password=$_POST['password'];
       $sql ="SELECT * FROM tbl_admin WHERE admin_id=:admin_id and password=:password";
        $query= $dbh -> prepare($sql);
        $query-> bindParam(':admin_id', $admin_id, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            $_SESSION['alogin']='success';
            $_SESSION['admin_id']=$_POST['admin_id'];
            foreach ($results as $row) {
                $_SESSION['admin_id'] =  $row['admin_id'];
                $_SESSION['lastname'] = $row['lastname'];
                $_SESSION['firstname'] = $row['firstname'];
                
            }
            header("Location: dashboard.php");
        } else{

            echo "<script>alert('Invalid Details');</script>";

        }
    }elseif($_POST['loginAs'] == 'Billing'){
        $billing_id=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT billing_id,lastname,firstname,password FROM tbl_billing WHERE billing_id=:billing_id and password=:password";
        $query= $dbh->prepare($sql);
        $query-> bindParam(':billing_id', $billing_id, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {   
            foreach ($results as $row) {
                $_SESSION['billing_id'] =  $row['billing_id'];
                $_SESSION['lastname'] = $row['lastname'];
                $_SESSION['firstname'] = $row['firstname'];
         }
        $_SESSION['alogin']=$_POST['username'];
        echo "<script type='text/javascript'> document.location = 'find-billing.php'; </script>";
        } else{
        
            echo "<script>alert('Invalid Details');</script>";
        
        }
    }elseif($_POST['loginAs'] == 'Student'){
        $student_id=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT student_id,lastname,firstname,password FROM tbl_student WHERE student_id=:student_id and password=:password";
        $query= $dbh->prepare($sql);
        $query-> bindParam(':student_id', $student_id, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            foreach ($results as $row) {
                $_SESSION['student_id'] =  $row['student_id'];
                $_SESSION['lastname'] = $row['lastname'];
                $_SESSION['firstname'] = $row['firstname'];
         }
        $_SESSION['alogin']=$_POST['username'];
        echo "<script type='text/javascript'> document.location = 'find-student.php'; </script>";
        } else{
        
            echo "<script>alert('Invalid Details');</script>";
        
        }
    }elseif($_POST['loginAs'] == 'Faculty'){
        $faculty_id=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT faculty_id,lastname,firstname,password FROM tbl_faculty_info WHERE faculty_id=:faculty_id and password=:password";
        $query= $dbh->prepare($sql);
        $query-> bindParam(':faculty_id', $faculty_id, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            foreach ($results as $row) {
                $_SESSION['faculty_id'] =  $row['faculty_id'];
                $_SESSION['lastname'] = $row['lastname'];
                $_SESSION['firstname'] = $row['firstname'];
                $_SESSION['dept_id'] = $row['dept_id'];
         }
        $_SESSION['alogin']=$_POST['username'];
        echo "<script type='text/javascript'> document.location = 'find-instructor2.php'; </script>";
        } else{
        
            echo "<script>alert('Invalid Details');</script>";
        
        }
    }elseif($_POST['loginAs'] == 'ProgHead'){
        $Prog_id=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT * FROM tbl_programhead WHERE Prog_id=:Prog_id and password=:password";
        $query= $dbh->prepare($sql);
        $query-> bindParam(':Prog_id', $Prog_id, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            foreach ($results as $row) {
                $_SESSION['Prog_id']   = $row['Prog_id'];
                $_SESSION['lastname']  = $row['lastname'];
                $_SESSION['firstname'] = $row['firstname'];
                $_SESSION['dept_id']   = $row['dept_id'];
         }
        $_SESSION['alogin']=$_POST['username'];
        echo "<script type='text/javascript'> document.location = 'find-proghead.php'; </script>";
        } else{
        
            echo "<script>alert('Invalid Details');</script>";
        
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>School Management System</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
        <style type="text/css">
          body{
                background-image: url(images/back.jfif);
                background-size: cover;
                background-attachment: fixed;
            }

            .content {

                background: white;
                width: 50%;
                padding: 40px;
                margin:100px auto;
                font-family: calibri;
            }

            p{
                font-size: 25px;
                color: black;

            }
        </style>
    </head>
    <body class="">
        <div class="main-wrapper">

            <div class="">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <a href="index.php">
                        <img src="images/LogoOcc.png" width="230" height="200"> <br>
                      <!--   <h1 align="center">Opol Community College Grade Inquiry</h1> -->
                        <br></a>
                    </div>
        
                                        <div class="col-md-4 col-md-offset-4">
                                            <div class="panel section-row">
                                                <div class="panel-heading">
                                                    <div class="panel-title text-center">
                                                        <h6><i class="fa fa-user"></i> User Login</h6>
                                                    </div>
                                                </div>
                                                <div class="panel-body p-10">
                                                    <form class="form-horizontal" method="post">
                                                        <div class="form-group">
                                                            <label for="loginAs" class="col-sm-3 control-label">Login as:  </label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control" id="loginAs" name="loginAs">
                                                            <option value="Registrar">Admin(Registrar)</option>
                                                            <option value="Billing">Admin(Billing)</option>
                                                            <option value="ProgHead">Program Head</option>
                                                            <option value="Faculty">Instructor</option>  
                                                            <option value="Student">Student</option>
                                                        </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group mt-20">
                                                            <label for="username" class="col-sm-3 control-label">Username</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="username" class="form-control" id="username" placeholder=" Issued ID">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                                                            <div class="col-sm-9">
                                                                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                                                            </div>
                                                        </div>



                                                        <div class="form-group mt-20">
                                                            <div class="col-sm-offset-2 col-sm-10">
                                                                <button type="submit" name="login" class="btn btn-success btn-labeled pull-right">Sign in<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
                                                            </div>
                                                        </div>

                                                    

                                                        <div>
                                                            <div class="form-group mt-10">
                                                            <div class="col-sm-offset-1 col-sm-5">
                                                           <a href="new-stud-reg.php"><b>New Student? <br> Register here!</a>
                                                           </div>

                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.panel -->

                                        </div>
                                        <!-- /.col-md-11 -->
                                    </div>

                </div>
                <!-- /.row -->






            </div>
            <!-- /. -->




        </div>
        <!-- /.main-wrapper -->


        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function(){

            });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->
    </body>
</html>
