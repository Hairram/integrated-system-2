<?php
session_start();
error_reporting(0);
include('includes/config.php');
if($_SESSION['alogin']!=''){
$_SESSION['alogin']='';
}
else{
    $_SESSION['alogin']='success';
}
if(isset($_POST['login']))
{
$rollid=$_POST['rollid'];
$password=$_POST['password'];
$sql ="SELECT * FROM tblproghead WHERE RollId=:rollid and Password=:password";
$query= $dbh->prepare($sql);
$query-> bindParam(':rollid', $rollid, PDO::PARAM_STR);
$query-> bindParam(':password', $password, PDO::PARAM_STR);
$query-> execute();
$results=$query->fetchAll();
if($query->rowCount() > 0)
{
    foreach ($results as $row) {
        $_SESSION['ph_id'] =  $row['ph_id'];
        $_SESSION['ph_FullName'] = $row['ProgHeadFullName'];
        $_SESSION['Departmentid'] = $row['Departmentid'];
 }
$_SESSION['alogin']=$_POST['rollid'];
echo "<script type='text/javascript'> document.location = 'find-proghead.php'; </script>";
} else{

    echo "<script>alert('Invalid Details');</script>";

}

}



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Grading Inquiry</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/icheck/skins/flat/blue.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
        <body class="">
        <div class="main-wrapper">

            <div class="login-bg-color bg-black-300">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel login-box">
                            <div class="panel-heading">
                                <div class="panel-title text-center">
                                    <h4>Program Head Login</h4>
                                </div>
                            </div>
                            <div class="panel-body p-20">



                                                    <form class="form-horizontal" action="prog-head.php" method="post">
                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-2 control-label">UserName</label>
                                                            <div class="col-sm-12">
                                                                <input type="text" name="rollid" class="form-control" id="rollid" placeholder="Roll ID">
                                                        
                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                                            <div class="col-sm-12">
                                                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                                            </div>
                                                        </div>

                                                        <div class="form-group mt-20">
                                                            <div class="col-sm-offset-2 col-sm-10">

                                                                <button type="submit" name="login" class="btn btn-success btn-labeled pull-right">Sign in<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
                                                            </div>
                                                        </div>
                                                    </form>

    </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                               <a href="index.php">Back to Home</a>
                                                            </div>



                                                </div>
                                            </div>
                                            <!-- /.panel -->
                                           
                                        </div>
                                        <!-- /.col-md-11 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.col-md-12 -->
                            </div>
                            <!-- /.row -->
                        </section>

                    </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function(){
                $('input.flat-blue-style').iCheck({
                    checkboxClass: 'icheckbox_flat-blue'
                });
            });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->
    </body>
</html>
