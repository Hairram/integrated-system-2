<?php
session_start();
error_reporting(0);
include('includes/config.php');
include('includes/dbconnect.php');
$student_id = $_SESSION['student_id']; 
$sql   		 = "SELECT * FROM tbl_student WHERE student_id='$student_id'";
$result		 = mysqli_query($conn, $sql);
$row    	 = mysqli_fetch_assoc($result);
$courseid    = $row['course_id'];
$balance     = $row['balance'];
$year_levels = $row['year_level'];
$sem1 	     = $row['semester'];
$sy1 	     = $row['school_year'];

if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$sy 		= $_POST['sy'];
$semester 	= $_POST['semester'];
$year_level = $_POST['year_level'];

$validate    = "SELECT * FROM tbl_student WHERE student_id='$student_id' and course_id='$courseid' and year_level='$year_level' and semester='$semester' and school_year='$sy'";
$validateres = mysqli_query($conn, $validate);
$cntres    	 = mysqli_num_rows($validateres);
if($cntres ==1){
echo "<script>
		alert('Data already registered!');
		window.location.href='self-evaluate.php';
	</script>";
} else { 
$update = "UPDATE tbl_student set year_level='$year_level' , semester='$semester' , school_year='$sy' , pay_status=0,status=0 , isApproved=0 , isPaid=0, tuition_fee=0,msc_fee=0, total_school_fee=0 ,old=1 where student_id='$student_id'";
mysqli_query($conn, $update);

// CHECK FAIL SUBJECTS //
$getpre  = "SELECT a.* , b.* , c.* , d.* FROM tbl_grades a
					LEFT JOIN tbl_student b on a.student_id = b.student_id
					LEFT JOIN tbl_classes c on a.class_id = c.class_id
					LEFT JOIN tbl_subjects d on d.subject_id = c.subject_id
					where a.student_id='$student_id' and a.year_level='$year_levels' and a.semester='$sem1' and a.school_year='$sy1'  and a.remarks='Fail'";
$getpreres   = mysqli_query($conn, $getpre);
while($res = $getpreres->fetch_assoc()) {		
		$subject_code[] = $res['descriptive_title'];
}
$json =  json_encode($subject_code, JSON_UNESCAPED_SLASHES);
$backsubjects =  trim($json, '[]');
if(empty($subject_code)){
$getclass   = "SELECT a.* , b.* FROM tbl_classes a
					LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
					WHERE a.course_id='$courseid' and a.year_level='$year_level' and b.semester='$semester'";	
} else {
echo $getclass   = "SELECT a.* , b.* FROM tbl_classes a
					LEFT JOIN tbl_subjects b on a.subject_id = b.subject_id
					WHERE a.course_id='$courseid' and a.year_level='$year_level' and b.semester='$semester' and  b.pre_requisite NOT IN ($backsubjects) ";	
}
$getclassres = mysqli_query($conn, $getclass);
while($res = $getclassres->fetch_assoc()) {		
		$class_id = $res['class_id'];
	
		$conn->query("insert into tbl_grades (student_id , class_id , year_level , semester , school_year ) VALUES ('$student_id','$class_id','$year_level','$semester','$sy')");
}	

$misc 		  = mysqli_query($conn, "SELECT * FROM tbl_miscellaneous") or mysqli_error($conn);
while($row1   = $misc->fetch_assoc()) {
$miscellaneous_id 	  = $row1['miscellaneous_id'];
$conn->query("INSERT INTO tbl_student_miscellaneous_fee (student_id , miscellaneous_id , school_year,semester) VALUES ('$student_id','$miscellaneous_id' ,'$sy','$semester')"); 
}
echo "<script>
		alert('Success !');
		window.location.href='self-evaluate.php?sy=$sy&semester=$semester&year_level=$year_level';
	</script>";
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Self Evaluation</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
        <style >
            .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar-stu.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbarStudent.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Student Self Evaluation</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="find-student.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Student</li>
                                        <li class="active">Self-Evaluate</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Self Evaluation</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
											<?php if($msg){?>
											<div class="alert alert-success left-icon-alert" role="alert">
											 <strong>Well done!</strong> <?php echo htmlentities($msg); ?>
											 </div><?php } 
											else if($error){?>
												<div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">
                                                <div class="form-group">

                                                     <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">School Year</label>
                                                        <div class="col-sm-5">
														<select name="sy" class="form-control" id="default" required="required">
														<option selected="selected" value="">-- Select SY -- </option>
														<?php 
														$sys          = "SELECT * FROM tbl_sy_sem WHERE isStatus=1";
														$sysres	 	  = mysqli_query($conn, $sys);
														while($rowss  = $sysres->fetch_assoc()) {?>
														<option value="<?php echo $rowss['school_year'];?>"><?php echo $rowss['school_year'];?></option>
														<?php } ?>
													</select>
                                                        </div>
                                                    </div>


                                                     <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Select Semester</label>
                                                        <div class="col-sm-5">
                                                            <select name="semester" class="form-control" id="default" required="required">
                                                                <option selected="selected" value="">- Select SY -</option>
                                                                <option value="First">First</option>
                                                                <option value="Second">Second</option>
                                                                <option value="Summer">Summer</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Year Level</label>
                                                        <div class="col-sm-5">
                                                       <select name="year_level" class="form-control" id="default" required="required">
														<option selected="selected" value="">-- Select Year Level -- </option>
														<option value="1">First</option>
														<option value="2">Second</option>
														<option value="3">Third</option>
														<option value="4">Fourth</option>
													</select>
                                                        </div>
                                                    </div>

                                                  
                                           
												<?php
												$sqlsy            = "SELECT * FROM tbl_grades WHERE grade=0 and student_id='$student_id' group by student_id";
												$resultsy	      = mysqli_query($conn, $sqlsy);
												$rowsy    	      = mysqli_num_rows($resultsy);
												$row22    		  = mysqli_fetch_assoc($resultsy);
												
												if($rowsy ==1){
													?>
													<div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-20">
                                                            COMPLETE YOUR GRADES!
                                                        </div>
                                                    </div>
												<?php 	
												} else {
													if($balance !=0){ ?>
														<div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-20">
                                                            COMPLETE YOUR BALANCE FEE!
                                                        </div>
                                                    </div>
													<?php } else { ?>
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-20">
                                                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                        </div>
                                                    </div>
												<?php } 
												}?>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
								
								                                 <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Subjects for <?php echo $_GET['semester'];?> Semester</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
											<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Course</th>
                                                            <th>Subject Code</th>
                                                            <th>Descriptive Title</th>
                                                            <th>Credit Units</th>
                                                            <th>Lec Hours</th>
                                                            <th>Lab Hours</th>
                                                            <th>Hrs/Week</th>
                                                             <th>Pre-requisites</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<tr>
													<?php 
													$sem  = $_GET['semester'];
													$yr  = $_GET['year_level'];
													if($sem == 'Second'){
														$ss = 'First';
													}
													if($sem == 'First'){
														$ss = 'Second';
													}
													// CHECK FAIL SUBJECTS //
													 $getpre  = "SELECT a.* , b.* , c.* , d.* FROM tbl_grades a
																		LEFT JOIN tbl_student b on a.student_id = b.student_id
																		LEFT JOIN tbl_classes c on a.class_id = c.class_id
																		LEFT JOIN tbl_subjects d on d.subject_id = c.subject_id
																		where a.student_id='$student_id' and a.year_level='$year_levels' and a.semester='$ss' and a.school_year='$sy1'  and a.remarks='Fail'";
													$getpreres   = mysqli_query($conn, $getpre);
													while($res = $getpreres->fetch_assoc()) {		
															$subject_code[] = $res['descriptive_title'];
													}
													$json =  json_encode($subject_code, JSON_UNESCAPED_SLASHES);
													$backsubjects =  trim($json, '[]');
													
													if(empty($subject_code)){
													 $sql = "Select a.* ,b.* from tbl_subjects a LEFT JOIN tbl_courses b on a.course_id = b.course_id 
													where a.semester='$sem' and a.year_level='$yr' and a.course_id='$courseid'
													order by a.subject_code";
													}else {
													 $sql = "Select a.* ,b.* from tbl_subjects a LEFT JOIN tbl_courses b on a.course_id = b.course_id 
													where a.semester='$sem' and a.year_level='$yr' and a.course_id='$courseid'  and  a.pre_requisite NOT IN ($backsubjects)
													order by a.subject_code ";
													}
													$query = $dbh->prepare($sql);
													$query->execute();
													$results=$query->fetchAll(PDO::FETCH_OBJ);
													$cnt=1;
													if($query->rowCount() > 0)
													{
													foreach($results as $result)
													{   ?>
													<tr>
													<td><?php echo htmlentities($cnt);?></td>
													<td><?php echo htmlentities($result->course_name);?></td>
													<td><?php echo htmlentities($result->subject_code);?></td>
													<td><?php echo htmlentities($result->descriptive_title);?></td>
													<td><?php echo htmlentities($result->units);?></td>
													<td><?php echo htmlentities($result->lec_hrs);?></td>
													<td><?php echo htmlentities($result->lab_hrs);?></td>
													<td><?php echo htmlentities($result->hrs_week);?></td>
													<td><?php echo htmlentities($result->pre_requisite);?></td>
													
													</tr>
													<?php $cnt=$cnt+1;}} ?>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });

            var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
};
        </script>
    </body>
</html>
<?PHP } ?>
