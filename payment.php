<?php
session_start();
error_reporting(0);
include('includes/dbconnect.php');
if(strlen($_SESSION['alogin'])=="")
    {
    header("Location: index.php");
    }
    else{
    $billing_id = $_SESSION['billing_id'];
    $billinglastname = $_SESSION['lastname'];
    $billingrfirstname = $_SESSION['firstname'];

	   if(isset($_POST['pay_btn'])){
		  $stud_id = $_POST['student_id'];
		  $amount = $_POST['amount_paid'];
		  $or_num = $_POST['or_num'];
		  $remarks = $_POST['remarks'];
		  mysqli_query($conn, "INSERT INTO tbl_payment_transaction (student_id,or_number,paid_amount,transaction_remark) VALUES ('$stud_id','$or_num','$amount','$remarks')") or die(mysqli_error($conn));
		  mysqli_query($conn, "UPDATE tbl_student set balance=balance-'$amount' where student_id='$stud_id'") or die(mysqli_error($conn));
		  echo "<script>alert('transation made successfully $amount'); wundow.location.href='payment.php';</script> ";
	   }


        ?>
<!DOCTYPE html>
<html lang="en">
    <head>
       <link rel="stylesheet" href="css/payment.js" >
         <link rel="stylesheet" href="css/payment.css" >
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information | Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/toastr/toastr.min.css" media="screen" >
        <link rel="stylesheet" href="css/icheck/skins/line/blue.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/red.css" >
        <link rel="stylesheet" href="css/icheck/skins/line/green.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>

    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
              <?php include('includes/topbar-billing.php');?>
            <div class="content-wrapper">
                <div class="content-container">

                    <?php include('includes/leftbar-billing.php');?>

           

<div class="container">


  <div class="box2">
    List of Paying Students
    <hr style="padding-bottom: 0px;">
    <div class="wrap">
  <button style="background-color: #fff; border: 1px solid #fff; margin: 0px;padding: 0px; position: absolute; right: 81px"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true" style="margin-right: 3px"></i></button>
<button style="background-color: #fff; border: 1px solid #fff; margin: 0px;padding: 0px; position: absolute; right: 58px"><i class="fa fa-file-word-o fa-lg" aria-hidden="true" style="margin-right: 5px"></i></button>
<button style="background-color: #fff; border: 1px solid #fff; margin: 0px;padding: 0px; position: absolute; right: 42px"><i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i>
</button>

<form class="form-inline" role="form" id="searchform">
  <div class="form-group"><b>Student ID *</b>
    <input type="text" name="" placeholder="Search . . .">
  </div>
  <div class="button" style="display: inline-block; padding-left: 20px; margin-bottom: 15px;">
   <button type="submit" name="Search" class="btn btn-primary" style=" border-radius: 18px; width: 80px;  font-size: 12px; padding-left: 10px; text-align: center;"><span class="fa fa-search"> </span>Search</button>
   </div>
</form> 
<!-- <div class="searching">
   <div class="search">
      <input type="text" class="searchTerm" placeholder="Search. . . ">
     <i class="fa-file-pdf"></i>
   </div>
  
</div> -->

   <div class="table">
   <table>
  <tr>                                   
    <th>Studen ID</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Course</th>
    <th>Fees</th>
    <th>Balance</th>
    <th>Date Approved</th>
    <th>Action</th>
    <th>COR</th>
    <th>History</th>
   </tr>

   <?php
    include('includes/dbconnect.php');
    $i = 1;
    $student_query = mysqli_query($conn, "	SELECT a.* , b.* FROM tbl_student a
											LEFT JOIN tbl_courses b on a.course_id = b.course_id
											WHERE a.status='1' and a.pay_status=1 and a.isPaid=0 and a.balance!=0 ") or die(mysqli_error($conn));
    while ($student_row = mysqli_fetch_array($student_query)) {
    
    $student_id 	= $student_row['student_id'];
    $balance 		= $student_row['balance'];

   
     
   ?>
  <tr>
     <td><?php echo $student_row['student_id']; ?></td>
     <td><?php echo $student_row['firstname']; ?></td>
     <td><?php echo $student_row['lastname']; ?></td>
     <td><?php echo $student_row['course_code']; ?></td>
     <td> <?php echo number_format($student_row['total_school_fee'], 2); ?><td><?php echo number_format($balance,2); ?></td>
     <td><?php echo date('d-m-Y', strtotime($student_row['date_approved'])); ?></td>
     <td> <a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add<?php echo $student_row['student_id']; ?>"> Take Fees </a></td>
     <td> <a href="report/cor.php?student_id=<?php echo $student_id ?>" class="btn btn-primary btn-xs" id="button">Print</a> </td>
     <td> <a href="transaction_history.php?student_id=<?php echo $student_id ?>&payable=<?php echo $student_row['total_school_fee']; ?>&balance=<?php echo $student_row['balance']; ?>" class="btn btn-primary btn-xs" id="button">View</a></td>
   </tr>

   
		<div class="modal fade" id="add<?php echo $student_row['student_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header alert-fill-primary">

				<h5 class="modal-title" id="exampleModalLabel">
				Take Fee  
				</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;">
					<span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <style type="text/css">.form-group input{
			
				border-top: 0px;
				border-bottom: 2px;
			  }
					  input:focus {
				   border-bottom: 1px solid black;
				  }
			  .form-group {
				padding-bottom: 34px;
			  }
			</style>
			  <form method="POST" >
			  
			  <input type="hidden" name="student_id" value="<?php echo $student_row['student_id']; ?>">
			  <div class="modal-body">
				<p>NAME :  <?php echo $student_row['lastname'] . ' , '. $student_row['firstname']; ?></p>
				<p>BALANCE :  <?php echo number_format($student_row['balance'],2); ?></p>
				<br>
				<div class="form-group">
				<input type="text" name="or_num" placeholder="Enter OR number" style="width:90%" class="form-control" required>
				</div>
			
				
				<div class="form-group">
				<input type="number" name="amount_paid" placeholder="Enter Amount Paid" style="width:90%" class="form-control" required>
				</div>

				<div class="form-group">
				<input type="text" name="remarks" placeholder="Enter Remarks" style="width:90%" class="form-control">
				</div>

			  </div>
			  <div class="modal-header">
				  <button class="btn btn-danger" data-dismiss="modal" aria-label="Close" >Cancel</button>
				  <button class="btn btn-success" type="submit" name="pay_btn"> Save </button>
			  </div>
			  </div>
			  </form>
			</div>
		  </div>

   <?php $i++; } ?>
   
</table>
</div>
</div>

  </div>
</div>

                          
      <script>
        document.getElementById('button').addEventListener('click', function() {
          var student_id = $(this).attr('data-id');
          var fill_student_id = document.getElementById('student_id');

          $(fill_student_id).val(student_id);
          
  document.querySelector('.bg-modal').style.display = 'flex';
});

         document.querySelector('.close').addEventListener('click', function() {
          document.querySelector('.bg-modal').style.display = 'none';
         });
      </script>

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/waypoint/waypoints.min.js"></script>
        <script src="js/counterUp/jquery.counterup.min.js"></script>
        <script src="js/amcharts/amcharts.js"></script>
        <script src="js/amcharts/serial.js"></script>
        <script src="js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="js/amcharts/themes/light.js"></script>
        <script src="js/toastr/toastr.min.js"></script>
        <script src="js/icheck/icheck.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script src="js/production-chart.js"></script>
        <script src="js/traffic-chart.js"></script>
        <script src="js/task-list.js"></script>
        <script>
            $(function(){

                // Counter for dashboard stats
                $('.counter').counterUp({
                    delay: 10,
                    time: 1000
                });
                /*
                // Welcome notification
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "3000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }*/
                /*toastr["success"]( "Welcome "<?php echo(json_encode($instructorlastname .", ". $instructorfirstname));?>);

            });*/
        </script>
    </body>

    <div class="foot"><footer>

</footer> </div>

<style> .foot{text-align: center; */}</style>
</html>
<?php } ?>
