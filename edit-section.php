<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
{   
header("Location: index.php"); 
}
else{
if(isset($_POST['Update']))
{
$sid		  = intval($_GET['edit']);
$section_name = $_POST['section_name'];
$course_id    = $_POST['course_id']; 
$year_level   = $_POST['year_level']; 
$semester     = $_POST['semester'];
$students     = $_POST['students'];
$school_year  = $_POST['school_year'];

$sql = "UPDATE tbl_section SET section_name=?, course_id=?, year_level=? , semester=? , school_year=? , students =? WHERE section_id =?";
$dbh->prepare($sql)->execute([$section_name, $course_id, $year_level,$semester,$school_year,$students, $sid]);
$msg="Section Info updated successfully";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin Update Section </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Update Section</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage Sections</li>
                                        <li class="active">Edit Section</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
										<?php if($msg){?>
										<div class="alert alert-success left-icon-alert" role="alert">
										 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
										 </div><?php }  ?>
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">




<!-- =+================================================================================================================================================================================================================================================================
                                                        !!!SAMPLE UI ONLY!!!!
=+================================================================================================================================================================================================================================================================ -->
                <form class="form-horizontal" method="post">
				<?php
				$sid=intval($_GET['edit']);
				$sql = "SELECT * from tbl_section where section_id =:sid";
				$query = $dbh->prepare($sql);
				$query->bindParam(':sid',$sid,PDO::PARAM_STR);
				$query->execute();
				$results=$query->fetchAll(PDO::FETCH_OBJ);
				$cnt=1;
				if($query->rowCount() > 0)
				{
				foreach($results as $result)
				{
				?>   
                    <div class="form-group">
                    <div class="form-group">
						<label for="default" class="col-sm-2 control-label">Course</label>
						<div class="col-sm-5">
						<select id="CourseNames" name="course_id" class="form-control" required>
						<option value=""> -- Select Course -- </option>
						<?php $sql = "SELECT * from tbl_courses order by course_code";
						$query1 = $dbh->prepare($sql);
						$query1->execute();
						$results1=$query1->fetchAll(PDO::FETCH_OBJ);
						if($query1->rowCount() > 0)
						{
						foreach($results1 as $result1)
						{ 
						if($result->course_id == $result1->course_id){
						?>
						<option value="<?php echo htmlentities($result1->course_id); ?>" selected><?php echo htmlentities($result1->course_name); ?></option>
						<?php } else {  ?>
						<option value="<?php echo htmlentities($result1->course_id); ?>"><?php echo htmlentities($result1->course_name); ?></option>
						<?php }}}?>
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="default" class="col-sm-2 control-label">Year Level</label>
						<div class="col-sm-5">
							<select name="year_level" class="form-control" id="default" required="required">
								<option selected="selected" value="<?php echo htmlentities($result->year_level);?>"><?php echo htmlentities($result->year_level);?></option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
							</select>
						</div>
					</div>
					<div class="form-group">
					<label for="default" class="col-sm-2 control-label">Section</label>
					<div class="col-sm-5">
						<select name="section_name" class="form-control" id="default" required="required">
							<option selected="selected" value="<?php echo htmlentities($result->section_name);?>"><?php echo htmlentities($result->section_name);?></option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							<option value="D">D</option>
							<option value="E">E</option>
							<option value="F">F</option>
						</select>
					</div>
				</div>
                 
                    <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">School Year</label>
                                 <div class="col-sm-5">
                                    <input type="text" name="school_year" value="<?php echo htmlentities($result->school_year);?>" class="form-control" id="default" placeholder="Semester" required="required" readonly>
                                </div>
                            </div>

                    <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">Semester</label>
                                 <div class="col-sm-5">
                                    <input type="text" name="semester" value="<?php echo htmlentities($result->semester);?>" class="form-control" id="default" placeholder="Semester" required="required" readonly>
                                </div>
                            </div>


                    <div class="form-group">
                         <label for="default" class="col-sm-2 control-label">No. of Students</label>
                                 <div class="col-sm-5">
                                    <input type="text" name="students"  value="<?php echo htmlentities($result->students);?>" readonly="" class="form-control" id="default" placeholder="Semester" readonly>
                                </div>
                            </div>

					<?php } }  ?>

                    <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-5">
                                <button type="submit" name="Update" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>


                                                </div>
                                            </div>

                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
