 
<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
        if(isset($_GET['delete_sure']))
            {
            $sql = "DELETE FROM tbl_subjects where subject_id=:stid";
            $query = $dbh->prepare($sql);
            $query->bindParam(':stid',$_GET['delete_sure'],PDO::PARAM_STR);
            $query->execute();
            
            $msg="Subejct deleted successfully!";
            }

            if(isset($_GET['sy_sem_id'])){
                $sy_sem_id_g = $_GET['sy_sem_id'];
                $sql = "SELECT * FROM tbl_sy_sem WHERE sy_sem_id=:stid";
                $query = $dbh->prepare($sql);
                $query->bindParam(':stid',$sy_sem_id_g,PDO::PARAM_STR);
                $query->execute();  
                $results=$query->fetchAll(PDO::FETCH_OBJ);
                    foreach($results as $result){
                        $SchoolYear = $result->school_year;
                        $Semester = $result->semester;
            
            
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin View S.Y. & SEM</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
<?php include('includes/leftbar.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>View S.Y. and Semester</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li>Manage S.Y. and Semester</li>
            							<li class="active">View S.Y. and Semester</li>
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <!-- <h4><b>View S.Y. and Semester Information</b></h4> -->

                                                    <br>
<p><b>School Year:<?php echo htmlentities($SchoolYear);?></p>
<p>Semester: <?php echo htmlentities($Semester);?></p></b>


                                                </div>
                                            </div>
                                       
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>

                                           <?php if(isset($_GET['delete'])):?>
    <div class="alert alert-danger left-icon-alert" role="alert">
    <strong>Are you sure to delete <?php echo $_GET['delete']; ?>?</strong>
    <a href="manage-subjects.php?delete_sure=<?php echo htmlentities($_GET['delete']);?>"><input class="btn btn-danger" type="button" name="edit" value="YES"></a>
    <a href="manage-subjects.php"><input class="btn btn-default" type="button" name="edit" value="CANCEL"></a>
    </div
    ><?php endif; ?> 
                                            <div class="panel-body p-20">
                                                
                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Courses</th>
                                                            <th>Sections</th>
                                                           <!--  <th>Action</th> -->
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody>
<?php 
$getCourse = "IFNULL((Select course_code from tbl_courses co where co.course_id=c.course_id limit 1), 'N/A') as Course";
$getSectionName = "IFNULL((SELECT CONCAT (co.course_code,'-', c.year_level,' ',c.section_name) from tbl_courses co where c.course_id=co.course_id limit 1), 'N/A') as SectionName";
$sy_sem_id=$_GET['sy_sem_id'];
$sql = "SELECT * FROM tbl_sy_sem where sy_sem_id=:sy_sem_id"; 
$query = $dbh->prepare($sql);
$query->bindParam(':sy_sem_id',$sy_sem_id,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<tr>
        <td><?php echo htmlentities($cnt);?></td>
        <td><?php echo htmlentities($result->Course);?></td>
        <td><?php echo htmlentities($result->Section);?></td>
        
       
        
<!-- <td>
<a href="edit-subject.php?subjectid=<?php echo htmlentities($result->subject_id);?>"><input type="button" name="edit" value="Edit"> </a> 
<a href="manage-subjects.php?delete=<?php echo htmlentities($result->descriptive_title);?>"><input type="button" name="delete" value="Delete"> </a> -->

</td>
</tr>
<?php $cnt=$cnt+1;}} ?>

                                                       
                                                    
                                                    </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
<?php } ?>
