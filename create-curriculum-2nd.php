<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$course_id=$_POST['course_id']; 
$var2 = explode("/", $course_id);
$course_id=$var2[0];

$curr_id=$_POST['curr_id']; 
$var1 = explode("/", $curr_id);
$curr_id=$var1[0];

$subject_id=$_POST['subject_id']; 
$var = explode("/", $subject_id);
$subject_id=$var[0];

$yr_level=$_POST['yr_level'];
$semester=$_POST['semester'];
$effective_sy=$_POST['effective_sy'];
$revised_sy=$_POST['revised_sy'];
$curr_description=$_POST['curr_description']; 

$sql="INSERT INTO  tbl_curr_subjects(curr_id,course_id,subject_id,yr_level,semester,effective_sy,revised_sy) VALUES(:curr_id,:course_id,:subject_id,:yr_level,:semester,:effective_sy,:revised_sy)";
$query = $dbh->prepare($sql);
$query->bindParam(':curr_id',$curr_id,PDO::PARAM_STR);
$query->bindParam(':course_id',$course_id,PDO::PARAM_STR);
$query->bindParam(':subject_id',$subject_id,PDO::PARAM_STR);
$query->bindParam(':yr_level',$yr_level,PDO::PARAM_STR);
$query->bindParam(':semester',$semester,PDO::PARAM_STR);
$query->bindParam(':effective_sy',$effective_sy,PDO::PARAM_STR);
$query->bindParam(':revised_sy',$revised_sy,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Curriculum Created successfully";
header("Location: manage-curriculum.php");
}
else 
{
$error="Something went wrong. Please try again";
}

}





?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
                                               


        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Create Curriculum</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage Curriculum</li>
                                        <li class="active">Create Curriculum</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5><b>Create New Curriculum</b></h5>
                                                </div>
                                          
                                        </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
         <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
    </div>
<?php } 
//GET and SET Curriculum ID For Selection
$curr_id_set=intval($_GET['CurriculumId']);
$sql_curr_id_set = "SELECT * FROM tbl_curriculum where curr_id=:curr_id"; 
$query_curr_id_set = $dbh->prepare($sql_curr_id_set);
$query_curr_id_set->bindParam(':curr_id',$curr_id_set,PDO::PARAM_STR);
$query_curr_id_set->execute();
$results_curr_id_set=$query_curr_id_set->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query_curr_id_set->rowCount() > 0)
{
    foreach($results_curr_id_set as $result)
        {    
         $getCurrName = $result->curr_id . '/' .$result->curr_name;
         $cnt=$cnt+1;
        }
} 
?>

    <form class="form-horizontal" method="post">
        <div class="form-group">
            <label for="default" class="col-sm-2 control-label">Curriculum Name</label> 
                <div class="col-sm-5">
                     <input list="CurriculumNames" value="<?php echo $getCurrName; ?>" name="curr_id" id="default" class="form-control" placeholder="Course" required>
                        <datalist id="CurriculumNames">

<?php 
$sql = "SELECT * from tbl_curriculum order by curr_id";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>

<option value="<?php echo htmlentities($result->curr_id . '/' .$result->curr_name); ?>"></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>


<?php } 
//GET and SET Curriculum ID For Selection
$course_id_set=intval($_GET['CurriculumId']);
$sql_course_id_set = "SELECT * FROM tbl_courses where course_id=:course_id"; 
$query_course_id_set = $dbh->prepare($sql_course_id_set);
$query_course_id_set->bindParam(':course_id',$course_id_set,PDO::PARAM_STR);
$query_course_id_set->execute();
$results_course_id_set=$query_curr_id_set->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query_curr_id_set->rowCount() > 0)
{
    foreach($results_course_id_set as $result)
        {    
         $getCurrCourse = $result->course_id . '/' .$result->course_name;
         $cnt=$cnt+1;
        }
} 
?>


        <div class="form-group">
            <label for="default" class="col-sm-2 control-label">Course</label>
                <div class="col-sm-5">
                     <input list="CourseNames" value="<?php echo $getCurrCourse; ?>" name="course_id" id="default" class="form-control" placeholder="Course" required>
                        <datalist id="CourseNames">

<?php $sql = "SELECT * from tbl_courses order by course_id";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->course_id . '/' .$result->course_code); ?>"></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Effective SY: </label>
                                                        <div class="col-sm-5">
 <input type="text" name="effective_sy" class="form-control" id="default" placeholder="e.g. 2019-2020" required="required">
                                                        </div>
                                                        </div>        

                                                    <div class="form-group">
                                                     <label for="default" class="col-sm-2 control-label">Revised SY: </label>
                                                        <div class="col-sm-5">
 <input type="text" name="revised_sy" class="form-control" id="default" placeholder="e.g. 2019-2020" required="required">
                                                        </div>
                                                        </div>

         <div class="form-group">
                <label for="default" class="col-sm-2 control-label">Add Yr. Level:</label>
                <div class="col-sm-5">
                    <select name="yr_level" class="form-control" id="default" required="required">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        
                    </select>
                </div>
            </div>

                     <div class="form-group">
                            <label for="default" class="col-sm-2 control-label">Add Semester:</label>
                            <div class="col-sm-5">
                                <select name="semester" class="form-control" id="default" required="required">
                                    <option selected="selected" value="First">First</option>
                                    <option value="Second">Second</option>
                                    <option value="Summer">Summer</option>
                                       
                                </select>
                            </div>
                        </div>

                         <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Subjects</label>
                                                        <div class="col-sm-5">
                                                            <input list="descriptive_title" name="subject_id" id="default" class="form-control" placeholder="Subjects" required>
                                                        <datalist id="descriptive_title">
<?php $sql = "SELECT * from tbl_subjects order by subject_id";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->subject_id . '/' .$result->descriptive_title); ?>"></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

                            
        <!--  <?php if(!isset($_GET['show_instructions'])): ?>
             <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-left">
                    <a href="create-curriculum.php?show_instructions=true"><i class="fa fa-plus"></i> <b>Show Subjects</b></a>
                </div>
            </div>
            <?php endif; ?>
            <?php if(isset($_GET['show_instructions'])): ?>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-left">
                    <a href="create-curriculum.php"><i class="fa fa-minus"></i> <b>Hide Subjects</b></a>
                </div>
            </div> -->
            

              <!-- <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Subject Code</th>
                    <th>Descriptive Title</th>
                    <th>Units</th>
                    <th>Pre-requisites</th>
                    <th>Action</th>
                </tr>
            </thead>
            
                                                    <tbody>
<?php $sql = "SELECT * from tbl_subjects order by descriptive_title";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<tr>
        <td><?php echo htmlentities($cnt);?></td>
        <td><?php echo htmlentities($result->subject_code);?></td>
        <td><?php echo htmlentities($result->descriptive_title);?></td>
        <td><?php echo htmlentities($result->units);?></td>
        <td><?php echo htmlentities($result->pre_requisite);?></td>
        
<td>

<input type="checkbox" name="submit"> 

</td>
</tr>

<?php $cnt=$cnt+1;}} ?>
                                                       
                                                    
                                             </tbody>

                                             <br>
                                             <tr>
                                                <td><button type="submit" name="submit" class="btn btn-primary">Create</button>
                                                 </td>
                                            </tr>  
                                        <?php else: ?>
                                                <br> -->

                                            <div class="form-group"> 
                                             <div class="col-sm-offset-2 col-sm-2"> 
                                                <button type="submit" name="submit" class="btn btn-primary">Create</button>
                                             </div>
                                         </div> 
                                         <?php endif; ?> 
                                             </table>
                                            </form>
                                                 
                                                 </div>
                                                   
                                </div>
                    </div>

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>

        
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>

