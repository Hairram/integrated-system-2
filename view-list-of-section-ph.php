
<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC List of Section</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #dd3d36;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
		.succWrap{
			padding: 10px;
			margin: 0 0 20px 0;
			background: #fff;
			border-left: 4px solid #5cb85c;
			-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
			box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
			<?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
			<?php include('includes/leftbar-ph.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"> List of Section</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-proghead.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li>  List of Section</li>
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                          
                                            <div class="panel-body p-20">
											<b> FILTER : </b>
											<?php
												$sql = "SELECT * from tbl_sy_sem";
												$query   = $dbh->prepare($sql);
												$query->execute();
												$results = $query->fetchAll(PDO::FETCH_OBJ);
											?>
											 <form  class="form-inline" method="post">
                                                <div class="form-group">
													<label for="default" class=""> School Year </label>
													<select name="sy" class="form-control" id="default" required="required">
														<option selected="selected" value="">-- Select SY -- </option>
														<?php foreach($results as $row=> $val){ ?>
														<option value="<?php echo $val->school_year;?>"><?php echo $val->school_year;?></option>
														<?php } ?>
													</select>
												</div>
												<div class="form-group">
													<label for="default" class=""> Semester </label>
													<select name="semester" class="form-control" id="default" required="required">
														<option selected="selected" value="">-- Select Semester -- </option>
														<option value="First">First</option>
														<option value="Second">Second</option>
														<option value="Summer">Summer</option>
													</select>
												</div>
												<div class="form-group">
													<label for="default" class=""> Year Level </label>
													<select name="year_level" class="form-control" id="default" required="required">
														<option selected="selected" value="">-- Select Year Level -- </option>
														<option value="1">First</option>
														<option value="2">Second</option>
														<option value="3">Third</option>
														<option value="4">Fourth</option>
													</select>
												</div>
												<div class="form-group">
													<label for="default" class=""> &nbsp;&nbsp; </label>
													<button type="submit" name="filter" class="btn btn-default btn-md" > FILTER </button>
												</div>
											</form>
											<br><br>
                                              <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Section ID</th>
                                                            <th>Course</th>
                                                            <th>Section</th>
                                                            <th>School Year</th>
                                                            <th>Semester</th>
                                                            <th>Students</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
														<?php 
														if(isset($_POST['filter'])){
														$sy          = $_POST['sy'];
														$semester    = $_POST['semester'];
														$year_level  = $_POST['year_level'];
														$sql         = "SELECT a.*,b.* from tbl_section a
																        LEFT JOIN tbl_courses b  on a.course_id = b.course_id
																		where year_level ='$year_level' and semester ='$semester' and 
																		school_year = '$sy'
																		";
														} else {
														$sql = "SELECT a.*,b.* from tbl_section a
																LEFT JOIN tbl_courses b  on a.course_id = b.course_id";
														}
														$query = $dbh->prepare($sql);
														$query->execute();
														$results=$query->fetchAll(PDO::FETCH_OBJ);
														$cnt=1;
														if($query->rowCount() > 0)
														{
														foreach($results as $result)
														{   
														?> 
														<tr>
															<td class="text-center"><?php echo htmlentities($result->section_id);?></td>
															<td class="text-center"><?php echo htmlentities($result->course_code);?></td>
															<td class="text-center"><?php echo htmlentities($result->course_code.'-'.$result->section_name);?></td> 
															<td class="text-center"><?php echo htmlentities($result->school_year);?></td>
															<td class="text-center"><?php echo htmlentities($result->semester);?></td>
															<td class="text-center"><a href="sectiondetails.php?view=<?php echo $result->section_name;?>&year_level=<?php echo htmlentities($result->year_level);?>&semester=<?php echo htmlentities($result->semester);?>"><button class="btn btn-info btn-md"> Classes </button></a></td>
															
														</tr>
														<?php $cnt=$cnt+1;
															}
															} 
														?>
                                                       
                                                    
                                                    </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->
									<div class="form-group">
                                        <div class="col-sm-10 text-right">
                                        <i class="text-danger">Note! Instructors maximum load is 24 units only.</i>
                                        </div>
                                                               
                                      </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>


