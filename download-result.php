<?php
session_start();

require_once('includes/configpdo.php');


require_once("dompdf/autoload.inc.php");

use Dompdf\Dompdf; 

ob_start();
//error_reporting(0);
?>

<html>
<style>
body {
  padding: 4px;
  text-align: center;
}

table {
  width: 100%;
  margin: 10px auto;
  table-layout: auto;
}

.fixed {
  table-layout: fixed;
}

table,
td,
th {
  border-collapse: collapse;
}

th,
td {
  padding: 1px;
  border: solid 1px;
  text-align: center;
}


</style>


<?php 
$totlcount=0;
$rollid=$_SESSION['rollid'];
$classid=$_SESSION['classid'];
$qery = "SELECT   tblstudents.StudentName,tblstudents.RollId,tblstudents.RegDate,tblstudents.StudentId,tblstudents.Status,tblclasses.ClassName,tblclasses.Section,tblclasses.Semester,tblclasses.School_Year from tblstudents join tblclasses on tblclasses.id=tblstudents.ClassId where tblstudents.RollId=? and tblstudents.ClassId=?";
$stmt21 = $mysqli->prepare($qery);
$stmt21->bind_param("ss",$rollid,$classid);
$stmt21->execute();
                 $res1=$stmt21->get_result();
                 $cnt=1;
                   while($result=$res1->fetch_object())
                  {  ?>

                    <h3> OPOL COMMUNITY COLLEGE</h3>
                    <h4> Opol, Misamis Oriental</h4>
                    <h3> OFFICE OF THE REGISTRAR</h3>
                    <h3> SEMESTRAL REPORT CARD</h3>

<p><b>Student Name: </b> <?php echo htmlentities($result->StudentName);?></p>
<p><b>Student ID: </b> <?php echo htmlentities($result->RollId);?>
<p><b>Course and Year: </b> <?php echo htmlentities($result->ClassName);?>(<?php echo htmlentities($result->Section);?>)
<p><b>Semester: </b> <?php echo htmlentities($result->Semester);?></p>
<p><b>School Year: </b> <?php echo htmlentities($result->School_Year);?></p>
<?php }

    ?>
 <table class="table table-inverse" border="1">
                      
                                                <table class="table table-hover table-bordered">
                                                <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Subject Code</th>
                                                            <th>Descriptive Title</th>    
                                                            <th>Final Grade</th>
                                                            <th>Re-Exam</th>
                                                            <th>Units Earned</th>
                                                        </tr>

                                               </thead>
  


                                                  
                                                  <tbody>
<?php                                              
// Code for result
 $query ="select t.StudentName,t.RollId,t.ClassId,t.marks,t.ReExam,SubjectId,SubjectCode,units,tblsubjects.SubjectName from (select sts.StudentName,sts.RollId,sts.ClassId,tr.marks,tr.ReExam,SubjectId from tblstudents as sts join  tblresult as tr on tr.StudentId=sts.StudentId) as t join tblsubjects on tblsubjects.id=t.SubjectId where (t.RollId=? and t.ClassId=?)";
$stmt = $mysqli->prepare($query);
$stmt->bind_param("ss",$rollid,$classid);
$stmt->execute();
                 $res=$stmt->get_result();
                 $cnt=1;
                   while($row=$res->fetch_object())
                  {

    ?>

                                                   <tr>
                                                <td ><?php echo htmlentities($cnt);?></td>
                                                      <td><?php echo htmlentities($row->SubjectCode);?></td>
                                                      <td><?php echo htmlentities($row->SubjectName);?></td>
                                                      <td><?php echo htmlentities($totalmarks=$row->marks);?></td>
                                                      <td><?php echo htmlentities($row->ReExam);?></td>
                                                      <td><?php echo htmlentities($row->units);?></td>
                                                    </tr>
<?php 
$totlcount+=$totalmarks;
$cnt++;}
?>
<!--<tr>
                                                <th scope="row" colspan="2">Total Marks</th>
<td><b><?php echo htmlentities($totlcount); ?></b> out of <b><?php echo htmlentities($outof=($cnt-1)*100); ?></b></td>
                                                        </tr>
<tr>
                                                <th scope="row" colspan="2">Percntage</th>           
                                                            <td><b><?php echo  htmlentities($totlcount*(100)/$outof); ?> %</b></td>
                                                             </tr>-->

                            </tbody>
                        </table>
                         <p><h3>***This Report Card is not valid if it has mark of erasure or alteration of any entry.***</h3></p>
                    </div>
</html>






<?php
$html = ob_get_clean();


$dompdf = new dompdf();

$dompdf->set_option('enable_html5_parser', TRUE);

$dompdf->loadHtml($html); 
 

$dompdf->setPaper('letter', 'portrait'); 
 

$dompdf->render(); 
ob_end_clean();

$dompdf->stream("Report Card");
?>