<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['Update']))
{
$sid=intval($_GET['sysem_id']);
$school_year=$_POST['school_year'];
$semester=$_POST['semester']; 
$sem_def=$_POST['sem_def']; 

if($sem_def == 1){
$sql1 = "UPDATE tbl_sy_sem SET  isStatus=?";
$dbh->prepare($sql1)->execute([0]);
$sql = "UPDATE tbl_sy_sem SET school_year=?, semester=?, isStatus=? WHERE sy_sem_id=?";
$dbh->prepare($sql)->execute([$school_year, $semester, $sem_def, $sid]);
} else {
$sql = "UPDATE tbl_sy_sem SET school_year=?, semester=?, isStatus=? WHERE sy_sem_id=?";
$dbh->prepare($sql)->execute([$school_year, $semester, $sem_def, $sid]);	
}
$msg="S.Y. & Semester Info updated successfully";
}
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
  <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title"><b>Update S.Y. and Semester</b></h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Manage S.Y. and Semester</li>
                                        <li class="active">Update S.Y. and Semester</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <!-- <h5>Update Subject</h5> -->



                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
    <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
        else if($error){?>
            <div class="alert alert-danger left-icon-alert" role="alert">
                <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
            </div>
                                        <?php } ?>
                                        <form class="form-horizontal" method="post">

<?php
$sid=intval($_GET['sysem_id']);
$sql = "SELECT * from tbl_sy_sem where sy_sem_id=:sid";
$query = $dbh->prepare($sql);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{
?>                           


<div class="form-group">
	<label for="default" class="col-sm-4 control-label">School Year</label>
	<div class="col-sm-7">
	 <input type="text" name="school_year" value="<?php echo htmlentities($result->school_year);?>" class="form-control" id="default" placeholder="school_year" required="required">
	</div>
</div>
<div class="form-group">
<label for="default" class="col-sm-4 control-label">Semester</label>
	<div class="col-sm-7">
	 <input type="text" name="semester" class="form-control" value="<?php echo htmlentities($result->semester);?>"  id="default" placeholder="Semester" required="required">
	</div>
</div>
<div class="form-group">
<label for="default" class="col-sm-4 control-label">Set As Default School Year and Semester</label>
	<div class="col-sm-7">
	 <select name="sem_def" class="form-control" required="required">
		 <option value=""> </option>
		 <?php if($result->isStatus == 1){ ?>
			 <option value="1" selected> Yes </option>
			 <option value="0"> No </option>
		 <?php }  if($result->isStatus == 0){ ?>
			 <option value="1" > Yes </option>
			 <option value="0"> No </option>
		 <?php } ?>
	 </select>
	</div>
</div>
<?php }} ?>

                                                    
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<button type="submit" name="Update" class="btn btn-primary">Update</button>
</div>
</div>
</form>

</div>
</div>
</div>
<!-- /.col-md-12 -->
</div>
</div>
</div>
<!-- /.content-container -->
</div>
<!-- /.content-wrapper -->
</div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
