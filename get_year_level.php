<?php include('includes/config.php');?>
<div class="form-group">
	<label for="default" class="col-sm-2 control-label">Year Level</label>
	<div class="col-sm-5">
		<select id="getyearlevel" name="year_level" class="form-control" required>
			<option value=""> -- Select Year Level -- </option>
			<?php 
			$courseid =  $_POST['course_id'];
			$sql1 = "SELECT * from tbl_section where course_id ='$courseid' group by year_level";
			$query1 = $dbh->prepare($sql1);
			$query1->execute();
			$results1=$query1->fetchAll(PDO::FETCH_OBJ);
			foreach($results1 as $result1){   
			?>
			<option value="<?php echo htmlentities($result1->year_level); ?>"><?php echo htmlentities($result1->year_level); ?></option>
			<?php } ?>
		</select>
	</div>
</div>

<script>
var UrlLink = "http://localhost/StudentInformationSystem/"
$('#getyearlevel').on('change', function() {
	var yearlevel = this.value;
	$.ajax({
		type: "POST",
		url:UrlLink+'get_section.php',
		data : {'yearlevel': yearlevel, 'course':<?php echo $courseid;?> },
		success: function(data) {
				$("#section_level").html(data);
			 }
		 });
});
</script>