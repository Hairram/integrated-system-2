 <?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
       $admin_id = $_SESSION['admin_id'];
	   if(isset($_POST['updateinfo'])){
			$admin_ids  = $_POST['admin_id'];
			$lastname   = $_POST['lastname'];
			$firstname  = $_POST['firstname'];
			$middlename = $_POST['middlename'];
			$address    = $_POST['address'];
			$email      = $_POST['email'];
			$dob        = $_POST['dob'];
		
			$sql = "UPDATE tbl_admin SET lastname=?, firstname=?, middlename=? , address=? , email=? , DOB =?  WHERE admin_id =?";

			$dbh->prepare($sql)->execute([$lastname, $firstname, $middlename,$address,$email,$dob,$admin_ids]);

			$msg="Admin Information Updated!";
		}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin| My Profile </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">My Profile</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                
                                        <li class="active">My Profile</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                       <div class="panel-body">
										<?php if($msg){?>
										<div class="alert alert-success left-icon-alert" role="alert">
										 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
										 </div>
										<?php } else if($error){?>
										<div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>

													<form class="form-horizontal" method="post">
													<?php 
													$sql = "SELECT * FROM tbl_admin WHERE admin_id=:admin_id";
													$query = $dbh->prepare($sql);
													$query->bindParam(':admin_id',$admin_id,PDO::PARAM_STR);
													$query->execute();
													$results=$query->fetchAll(PDO::FETCH_OBJ);
													$cnt=1;
													if($query->rowCount() > 0)
													{
													foreach($results as $result)
													{  ?>

													<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Admin ID</label>
													<div class="col-sm-9">
													<input type="text" name="admin_id" class="form-control" id="admin_id" value="<?php echo htmlentities($result->admin_id)?>"  readonly required="required" autocomplete="off">
													</div>
													</div>

													<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Lastname</label>
													<div class="col-sm-9">
													<input type="text" name="lastname" class="form-control" id="fullanme" value="<?php echo htmlentities($result->lastname)?>"  required="required" autocomplete="off">
													</div>
													</div>

													<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Firstname</label>
													<div class="col-sm-9">
													<input type="text" name="firstname" class="form-control" id="fullanme" value="<?php echo htmlentities($result->firstname)?>"  required="required" autocomplete="off">
													</div>
													</div>


													<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Middlename</label>
													<div class="col-sm-9">
													<input type="text" name="middlename" class="form-control" id="fullanme" value="<?php echo htmlentities($result->middlename)?>"  required="required" autocomplete="off">
													</div>
													</div>

													<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Address</label>
													<div class="col-sm-9">
													<input type="text" name="address" class="form-control" id="address" value="<?php echo htmlentities($result->address)?>"  required="required" autocomplete="off">
													</div>
													</div>

													<div class="form-group">
													<label for="default" class="col-sm-2 control-label">Email</label>
													<div class="col-sm-9">
													<input type="email" name="email" class="form-control" id="email" value="<?php echo htmlentities($result->email)?>"  required="required" autocomplete="off">
													</div>
													</div>

													<div class="form-group">
                                                        <label for="date" class="col-sm-2 control-label">DOB</label>
                                                        <div class="col-sm-9">
															<input type="date"  name="dob" class="form-control" value="<?php echo htmlentities($result->DOB)?>" id="date"  >
                                                        </div>
                                                    </div>




												<?php }} ?>   
												<div class="form-group">
                                                        <label for="date" class="col-sm-2 control-label">&nbsp;</label>
                                                       <button type="submit" name="updateinfo" class="btn btn-primary btn-md"> UPDATE </button>		
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
